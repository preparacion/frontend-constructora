import React, { PureComponent } from 'react';
import { Col, Button, Row } from 'reactstrap';
import Switch from "react-switch";
import { Tabs } from 'antd';
import { fetchAsync, urlTemplates, urlNotificationGroups, urlNotificationStudent } from '../../../helpers/Globals/globals';
import Frame, { FrameContextConsumer } from "react-frame-component";
import LoadingComponent from '../../../helpers/LoadingComponent'
import 'antd/lib/tabs/style/index.css';
import { showNotification, showMessageLoading, showMessageSuccess, showMessageError } from '../../../helpers/MessageAndNotificationUtils';
//Declaration of initialState and resetState using when users close the window, this will reset all selection or changes
// Declare to recive the function of messages, calling again will close the notification
const MessageIcon = `${process.env.PUBLIC_URL}/img/MessageIcon.png`;

const { TabPane } = Tabs
let closeLoading

const defaultState = {
    loading: true,
    text: "",
    textSms: "",
    res: "",
    structureMessage: "",
    checkedEmail: true,
    checkedSMS: true,
}

const ResetState = {
    loading: false,
    text: "",
    textSms: "",
    checkedEmail: true,
    checkedSMS: true,
}

class NotificationComponent extends PureComponent {

    constructor(props) {
        super(props);
        this.state = defaultState
    }

    componentDidMount() {
        this.getHtmlFromTemplate()
    }

    /****************************************************************
    *****************************************************************
    ************************Helpers**********************************
    *****************************************************************
    ****************************************************************/

    handleChange = (event) => {
        this.setState({
            text: event.target.value
        });
    }

    handleChangeSms = (event) => {
        this.setState({
            textSms: event.target.value
        });
    }
    handleChangeSwitchEmail = (checked) => {
        if (!checked && !this.state.checkedSMS) {
            this.setState({
                checkedEmail: checked,
                checkedSMS: true
            });
        } else {
            this.setState({ checkedEmail: checked });
        }
    }
    handleChangeSwitchSMS = (checked) => {
        if (!checked && !this.state.checkedEmail) {
            this.setState({
                checkedSMS: checked,
                checkedEmail: true
            });
        } else {
            this.setState({ checkedSMS: checked });
        }
    }

    setStuctureMessage = (data) => {
        this.setState({
            structureMessage: data
        })
    }

    numCaracteres = () => {
        if (this.state.textSms.length >= 160) {
            return (<p className="pull-right contadorCaracteres">Limite de 500 caracteres alcanzado.</p>)
        } else {
            return (<p className="pull-right contadorCaracteres">Caracteres restantes: {160 - this.state.textSms.length} de 160</p>)
        }
    }

    LoadingMessage = () => {
        if (this.props.type && this.props.type===1){
            return showMessageLoading('Enviando notificación', 0);
        }else{
            return showMessageLoading('Enviando notificaciones', 0);
        }

    };

    SuccessMessage = (messageText) => {
        showMessageSuccess(messageText, 2.5)
    }

    ErrorMessage = (messageText) => {
        showMessageError(messageText, 2.5)
    }

    ErrorNotification(studentList) {
        let messageText = "Error al inscribir " + studentList.length + " alumnos"
        let messageDescription = <div><p>Ocurrio un error al ejecutar la operación</p><p>Error:{studentList[0].error}</p></div>
        showNotification("error", messageText, messageDescription, 0)
    }
    /****************************************************************
    *****************************************************************
    ************************Network Operations***********************
    *****************************************************************
    ****************************************************************/
    getHtmlFromTemplate() {
        fetchAsync(urlTemplates + "d-a934c1e29ac94e4eb91061956d620114/versions/1aaf32ea-1048-491c-80d7-2030e89f2f35", "", "GET", "")
            .then(response => {
                console.log(response.response)

                this.setState({
                    res: response.response.html_content,
                    loading: false
                })
            })
            .catch({

            })
    }


    sendNotification = () => {
        closeLoading = this.LoadingMessage()
        let body = {}
        if (this.state.checkedSMS) {
            body.sms = {
                "message": this.state.textSms
            }
        }
        if (this.state.checkedEmail) {
            body.email = {
                "template": "d-a934c1e29ac94e4eb91061956d620114",
                "templateData": {
                    "message": this.state.text
                }
            }
        }
        if (this.props.type && this.props.type === 1) {
            fetchAsync(urlNotificationStudent + this.props.student._id, JSON.stringify(body), "POST", "")
                .then(response => {
                    if (response.success) {
                        closeLoading()
                        this.SuccessMessage("Notificacion enviada correctamente")
                        this.setState(ResetState)
                    } else {

                    }
                })
                .catch({

                })
        } else {
            fetchAsync(urlNotificationGroups + this.props.grupo._id, JSON.stringify(body), "POST", "")
                .then(response => {
                    if (response.success) {
                        closeLoading()
                        this.SuccessMessage("Notificaciones enviadas correctamente")
                        this.props.onCloseModal()
                    } else {

                    }
                })
                .catch({

                })
        }
    }

    render() {
        return (
            <div>
                {this.state.loading ? <LoadingComponent /> :
                    <Row>
                        <Col xs={12} sm={7}>
                            <Tabs type="card">
                                {!this.state.checkedEmail ? "" :
                                    <TabPane tab="PC" key="1">
                                        <Frame initialContent={this.state.res}>
                                            <FrameContextConsumer>
                                                {
                                                    ({ document, window }) => {
                                                        if (!this.state.structureMessage) {
                                                            let content = document.getElementById("DYNAMIC_ID_CONTENT").innerHTML
                                                            document.getElementById("DYNAMIC_ID_CONTENT").innerHTML = ""
                                                            this.setStuctureMessage(content)
                                                        } else {
                                                            let dummy = this.state.structureMessage
                                                            dummy = dummy.replace("{{name}}", "")
                                                            dummy = dummy.replace("{{message}}", this.state.text)
                                                            document.getElementById("DYNAMIC_ID_CONTENT").innerHTML = dummy
                                                        }
                                                    }
                                                }
                                            </FrameContextConsumer>
                                        </Frame>
                                    </TabPane>
                                }
                                {!this.state.checkedEmail ? "" :
                                    <TabPane tab="Android" key="2">
                                        <div class="deviceContainer androidPhone">
                                            <div class="androidPhone black portrait">
                                                <div class="caseBorder"></div>
                                                <div class="case"></div>
                                                <div class="reflection"></div>
                                                <div class="screen"></div>
                                                <div class="camera"></div>
                                                <div class="speaker"></div>
                                                <div class="homeButtonBorder"></div>
                                                <div class="homeButton"></div>
                                                <div class="content">
                                                    <Frame initialContent={this.state.res}>
                                                        <FrameContextConsumer>
                                                            {
                                                                ({ document, window }) => {
                                                                    if (!this.state.structureMessage) {
                                                                        let content = document.getElementById("DYNAMIC_ID_CONTENT").innerHTML
                                                                        document.getElementById("DYNAMIC_ID_CONTENT").innerHTML = ""
                                                                        this.setStuctureMessage(content)
                                                                    } else {
                                                                        let dummy = this.state.structureMessage
                                                                        dummy = dummy.replace("{{name}}", "")
                                                                        dummy = dummy.replace("{{message}}", this.state.text)
                                                                        document.getElementById("DYNAMIC_ID_CONTENT").innerHTML = dummy
                                                                    }
                                                                }
                                                            }
                                                        </FrameContextConsumer>
                                                    </Frame>
                                                </div>
                                            </div>
                                        </div>
                                    </TabPane>
                                }
                                {!this.state.checkedEmail ? "" :
                                    <TabPane tab="IOS" key="3">
                                        <div class="deviceContainer pointer scmc iphone" >
                                            <div class="iphonex black portrait">
                                                <div class="caseBorder"></div>
                                                <div class="case"></div>
                                                <div class="reflection"></div>
                                                <div className="containerScreen">
                                                    <div class="content centerVH">
                                                        <Frame initialContent={this.state.res}>
                                                            <FrameContextConsumer>
                                                                {
                                                                    ({ document, window }) => {
                                                                        if (!this.state.structureMessage) {
                                                                            let content = document.getElementById("DYNAMIC_ID_CONTENT").innerHTML
                                                                            document.getElementById("DYNAMIC_ID_CONTENT").innerHTML = ""
                                                                            this.setStuctureMessage(content)
                                                                        } else {
                                                                            let dummy = this.state.structureMessage
                                                                            dummy = dummy.replace("{{name}}", "")
                                                                            dummy = dummy.replace("{{message}}", this.state.text)
                                                                            document.getElementById("DYNAMIC_ID_CONTENT").innerHTML = dummy
                                                                        }
                                                                    }
                                                                }
                                                            </FrameContextConsumer>
                                                        </Frame>
                                                    </div>
                                                </div>
                                                <div class="speaker"></div>
                                            </div>
                                        </div>
                                    </TabPane>
                                }

                                {!this.state.checkedSMS ? "" :
                                    <TabPane tab="SMS" key="4">
                                        <div class="deviceContainer androidPhone">
                                            <div class="androidPhone black portrait">
                                                <div class="caseBorder"></div>
                                                <div class="case"></div>
                                                <div class="reflection"></div>
                                                <div class="screen"></div>
                                                <div class="camera"></div>
                                                <div class="speaker"></div>
                                                <div class="homeButtonBorder"></div>
                                                <div class="homeButton"></div>
                                                <div class="content">
                                                    <div id="DIV_2">
                                                        <div id="DIV_3">
                                                            <div id="DIV_4">
                                                                <span id="SPAN_5"></span><span id="SPAN_6">Mensajes</span><span id="SPAN_7"></span>
                                                            </div>
                                                            <div id="DIV_8">
                                                                <div id="DIV_9">
                                                                    <div id="DIV_10">
                                                                        <div id="DIV_11">3122</div>
                                                                        <div id="DIV_12">
                                                                            <p>[PREPARACIONIPN]:{this.state.textSms}</p>
                                                                        </div>
                                                                    </div><img src={MessageIcon} id="IMG_13" alt='' />
                                                                </div><img id="IMG_14" alt='' />
                                                            </div>
                                                            <div id="DIV_15">
                                                                <span id="SPAN_16">Responder</span><span id="SPAN_17">Ignorar</span><span id="SPAN_18">Cancelar</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </TabPane>
                                }

                            </Tabs>
                        </Col>


                        <Col xs={12} sm={5}>
                            <div>
                                <div className="form__form-group">
                                    <span className="form__form-group-label">Envíar Email</span>
                                    <div className="form__form-group-field">
                                        <label>
                                            <Switch onChange={this.handleChangeSwitchEmail} checked={this.state.checkedEmail}
                                                onColor="#86d3ff"
                                                onHandleColor="#2693e6"
                                                handleDiameter={30}
                                                uncheckedIcon={false}
                                                checkedIcon={false}
                                                boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                                                activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                                                height={20}
                                                width={48}
                                                className="react-switch"
                                                id="material-switch"
                                            />
                                        </label>
                                    </div>

                                    <span className="form__form-group-label">Enviar SMS</span>
                                    <div className="form__form-group-field">
                                        <label>
                                            <Switch onChange={this.handleChangeSwitchSMS} checked={this.state.checkedSMS}
                                                onColor="#86d3ff"
                                                onHandleColor="#2693e6"
                                                handleDiameter={30}
                                                uncheckedIcon={false}
                                                checkedIcon={false}
                                                boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                                                activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                                                height={20}
                                                width={48}
                                                className="react-switch"
                                                id="material-switch"
                                            />
                                        </label>
                                    </div>
                                </div>
                            </div>



                            <div className="card-container">
                                <Tabs type="card">
                                    {!this.state.checkedEmail ? "" :
                                        <TabPane tab="Email" key="5">
                                            <div className="cajaText1">
                                                <form action="" className="form_comentarios d-flex justify-content-end flex-wrap" >
                                                    <textarea name="" id="ComponentInputTextMessage"
                                                        placeholder="Mensaje Email"
                                                        value={this.state.text}
                                                        onChange={this.handleChange}
                                                        autofocus="true" />
                                                </form>

                                            </div>
                                        </TabPane>
                                    }
                                    {!this.state.checkedSMS ? "" :
                                        <TabPane tab="SMS" key="6">
                                            <div className="cajaText1">
                                                <form action="" className="form_comentarios d-flex justify-content-end flex-wrap" >
                                                    <textarea name="" id="ComponentInputTextMessage"
                                                        placeholder="Mensaje SMS"
                                                        value={this.state.textSms}
                                                        onChange={this.handleChangeSms}
                                                        autofocus="true" />
                                                    {this.numCaracteres()}
                                                </form>
                                            </div>
                                        </TabPane>
                                    }
                                </Tabs>
                            </div>
                            <Button className="btn" color="success" block onClick={this.sendNotification}>
                                Envíar
                </Button>
                        </Col>
                    </Row>
                }
            </div>
        )
    }
}
export default NotificationComponent