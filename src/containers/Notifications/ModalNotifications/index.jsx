import React, { PureComponent } from 'react';
import { Col, Button, Row } from 'reactstrap';
import Modal from 'react-responsive-modal';
import NotificationComponent from "../Components/NotificationComponent"



class Notification extends PureComponent {

    onCloseModal = () => {
        this.props.closeModal()
    }


    /****************************************************************
    *****************************************************************
    *****************************Render******************************
    *****************************************************************
    ****************************************************************/

    render() {

        const ClassNames = {
            modal: "modal-medium modal-full-height",
        }
        return (
            <Modal
                open={this.props.valueModal}
                onClose={this.closeModal}
                center={true}
                closeOnEsc={false}
                showCloseIcon={false}
                closeOnOverlayClick={false}
                classNames={ClassNames}>
                <Row className="marginTableInfoG">
                    <Col xs={12} sm={12} md={8}>
                        <p className="titleInfoTable">Envío de notificaciones al grupo: <strong>{this.props.grupo.name}</strong></p>
                    </Col>
                    <Col xs={12} sm={12} md={4}>
                        <Button size="sm" className="btn btn-secondary float-right" style={{ marginRight: "15px" }}
                            onClick={() => this.onCloseModal()}>
                            <p>Cerrar</p>
                        </Button>
                    </Col>
                </Row>
                <NotificationComponent
                    grupo={this.props.grupo}
                    onCloseModal={this.onCloseModal}
                />
            </Modal >
        )
    }
}
export default Notification
