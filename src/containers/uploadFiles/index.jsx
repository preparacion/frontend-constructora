import React, { PureComponent } from 'react';
import { Col, Row, Card, CardBody, Button } from 'reactstrap';
//import { fetchAsync, urlConcepts, } from '../../../../../helpers/Globals/globals';
import { uploadData, uploadData2, urlFiles, } from '../../helpers/Globals/globals';
import {
  showNotification, showMessageLoading, showMessageWarning,
  showMessageSuccess, showMessageError
} from '../../helpers/MessageAndNotificationUtils';
import Modal from 'react-responsive-modal';
import { Upload, Progress, Icon, Input } from 'antd';
import "antd/es/upload/style/index.css"
import "antd/es/progress/style/index.css"
import "antd/es/icon/style/index.css"
import "antd/es/input/style/index.css"


let closeLoading = ""
const { TextArea } = Input;
const { Dragger } = Upload;

const startState = {
  name: "",
  uploading: false,
  completeUpload: false,
  previewVisible: false,
  previewImage: '',
  fileList: [],
}

class UploadFileEmployee extends PureComponent {

  constructor(props) {
    super(props);
    this.state = startState;
  }


  componentDidUpdate(prevProps, prevState) {
    if (!this.props.valueModal) {
      this.setState(startState)
    }
  }

  onProgress = (value, index) => {
    let fileList = [...this.state.fileList];
    let item = { ...fileList[index] }
    item.percent = value
    fileList[index] = item
    this.setState({
      fileList: fileList
    })
  }

  onCloseModal = () => {
    if (closeLoading != "") {
      closeLoading()
    }

    this.props.close(false)

    this.setState({
      bloquear: false,
    })
  };

  //crea uan funcion para lamzar el mensaje de loading
  LoadingMessage = () => {
    return showMessageLoading('Subiendo..', 0);
  };

  subirVideos = () => {
    closeLoading = showMessageLoading("Subiendo contenido", 0)
    let promisesList = this.state.fileList.map((file, index) => {
      let formData = new FormData();
      if (file.name.length < 1) {
        showMessageWarning("Debees de ingresar un titulo", 3)
        closeLoading()
        return ""
      } else {
        formData.append("name", file.name)
      }
      formData.append("file", file.file);
      if (this.props.idContratista) {
        formData.append("contratista", this.props.idContratista);
      } else {
        formData.append("employee", this.props.idEmployee);
      }

      return uploadData2(urlFiles, formData, "POST", this.onProgress, index)
        .then(result => { return JSON.parse(result) })
        .then(result => {
          closeLoading()
          showMessageSuccess("Contenido subido correctamente , presiona Terminar para continuar", 3)
          let fileList1 = [...this.state.fileList];
          let item = { ...fileList1[index] }
          item.status = "success"
          item.percent = 100
          fileList1[index] = item
          this.setState({
            fileList: fileList1
          })

          if (this.props.isContratist) {
            this.props.getActualContratista(result.data)
          } else {
            this.props.getRowInfo(result.data)
          }

        })
        .catch(
          error => {
            closeLoading()
            showMessageError("Ocurrio un error al subir el contenido", 3)
            console.log("entro al catch")
            console.log(error)
            let fileList = [...this.state.fileList];
            let item = { ...fileList[index] }
            item.status = "exception"
            fileList[index] = item
            this.setState({
              fileList: fileList
            })
          }
        )
    })
    Promise.all(promisesList).then(results => {
      console.log("en promise all", results)
      if (results.length > 0) {
        this.setState({
          uploading: false,
          completeUpload: true
        })
      }
    })
  }

  drawVideoPreview = (list) => {
    return (
      <div class="ant-upload-list ant-upload-list-picture">
        {list.map(item => {
          return (
            <div class={`ant-upload-list-item ant-upload-list-item-${item.status}`}>
              <div class="ant-upload-list-item-info">
                <Row>
                  <Col xs={3}>

                  </Col>
                  <Col xs={9}>
                    <Input
                      prefix={<Icon type="play-square" />}
                      placeholder="Titulo"
                      value={item.name}
                      onChange={(event) => this.onChangeTitle(event, item)}
                      disabled={item.status == "uploading"} />
                  </Col>
                </Row>
                <Row>
                  <Col xs={12}>
                    {item.status == "uploading" || item.status == "success" || item.status == "exception" ?
                      <Progress percent={item.percent ? item.percent : 0} status={item.status == "uploading" ? "active" : item.status} />
                      : ""
                    }
                  </Col>
                </Row>
              </div>
              <i aria-label="icon: close" title="Remove file" tabindex="-1" class="anticon anticon-close" onClick={() => this.handleRemoveElement(item)}><svg viewBox="64 64 896 896" focusable="false" class="" data-icon="close" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M563.8 512l262.5-312.9c4.4-5.2.7-13.1-6.1-13.1h-79.8c-4.7 0-9.2 2.1-12.3 5.7L511.6 449.8 295.1 191.7c-3-3.6-7.5-5.7-12.3-5.7H203c-6.8 0-10.5 7.9-6.1 13.1L459.4 512 196.9 824.9A7.95 7.95 0 0 0 203 838h79.8c4.7 0 9.2-2.1 12.3-5.7l216.5-258.1 216.5 258.1c3 3.6 7.5 5.7 12.3 5.7h79.8c6.8 0 10.5-7.9 6.1-13.1L563.8 512z"></path></svg>
              </i>
            </div>
          )
        })}
      </div>
    )
  }


  onChangeTitle = (e, item) => {
    let newName = e.target.value
    let theitem = item
    this.setState(prevState => {
      let fileList = [...prevState.fileList];
      let index = fileList.findIndex(el => el.uid === theitem.uid);
      let item = { ...fileList[index] }
      item.name = newName
      fileList[index] = item
      return { fileList };
    })
  }

  onChangeDescription = (e, item) => {
    let newName = e.target.value
    let theitem = item
    this.setState(prevState => {
      let fileList = [...prevState.fileList];
      let index = fileList.findIndex(el => el.uid === theitem.uid);
      let item = { ...fileList[index] }
      item.description = newName
      fileList[index] = item
      return { fileList };
    })
  }


  handleChange = ({ fileList }) => {
    console.log("enter handleChange")
    this.setState(state => {
      let data = state.fileList
      if (data.length < 1) {
        data.push(fileList[0])
      }
      return {
        fileList: data,
      };
    });
  }

  handleRemoveElement = (file) => {
    this.setState(state => {
      const index = state.fileList.indexOf(file);
      const newFileList = state.fileList.slice();
      newFileList.splice(index, 1);
      return {
        fileList: newFileList,
      };
    });
  }

  handleUpload = () => {
    if (this.state.completeUpload) {
      this.onCloseModal()
    } else {
      let prevfileList = []
      for (let i = 0; i < this.state.fileList.length; i++) {
        prevfileList.push(this.state.fileList[i])
        prevfileList[i].status = 'uploading';
        // prevfileList[i].percent = 50;
      }
      this.setState({
        fileList: prevfileList,
        uploading: true,
      })
      this.subirVideos()
    }
  }


  render() {
    console.log("RENDERUploadFileEmployee")
    console.log(this.props)
    console.log("RENDERUploadFileEmployee")
    const { uploading, fileList, completeUpload } = this.state;
    const props = {
      beforeUpload: file => {
        this.setState(state => {
          let fileList = [...state.fileList]
          let prov = {
            name: file.name,
            description: "",
            percent: 0,
            uid: file.uid,
            file: file
          }
          fileList.push(prov)
          return { fileList }
          // fileList: [...state.fileList, file],
        });
        return false;
      },
      accept: [".pdf", ".jpeg", ".jpg", ".png"],
      fileList: [],
      multiple: false,
      onChange: this.handleChange,
      disabled: this.state.uploading,
    };

    return (
      <Row >
        <Col sm={12} md={12}>
          <Modal
            open={this.props.valueModal}
            onClose={this.onCloseModal}
            center={true}
            closeOnEsc={false}
            showCloseIcon={false}
            closeOnOverlayClick={false}>
            <Card className="addCursoSize mt-4 margenCursoAdd">
              <Row className="d-flex flex-row">
                <Col sm={12} md={12}>
                  <Button style={{ marginRight: "15px" }}
                    type="button" className="btn-sm float-right"
                    onClick={() => this.onCloseModal()}
                  >
                    Salir
                  </Button>
                </Col>
              </Row>
              <Row>
                <Col sm={12} md={5}>
                  <div className="card__title">
                    <h4 className="bold-text">Agregar Archivo</h4>
                  </div>
                </Col>
              </Row>
              <Dragger {...props}>
                <p className="ant-upload-drag-icon">
                  <Icon type="inbox" />
                </p>
                <p className="ant-upload-text">Da click o arrastra contenido para subir</p>
                <p className="ant-upload-hint">
                  Recuerda que todo el contenido debe verificarse antes de subir para evitar errores en sistema.
                            </p>
              </Dragger>
              {this.drawVideoPreview(this.state.fileList)}

              <Button
                color="success"
                type="button"
                onClick={() => this.handleUpload()}
                disabled={fileList.length === 0 || this.state.uploading}

                loading={uploading}>
                {uploading ? 'Subiendo' : completeUpload ? "Terminar" : 'Subir'}
              </Button>
            </Card>
          </Modal>
        </Col>
      </Row>
    );
  }
}
export default UploadFileEmployee;


