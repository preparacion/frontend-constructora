import React, { Component, PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';
import Layout from '../Layout/index';
import MainWrapper from './MainWrapper';
import NotFound404 from '../DefaultPages/404/index';
import LogIn from '../LogIn/index';
import ForgotForm from '../PasswordRecovery/index';
import LogOut from '../Logout/index';
import PasswordUpdate from '../PasswordUpdate/index';
import Employees from '../employess/index';
import Roles from '../Roles/index';
import Categories from '../Categories/index';
import Concepts from '../Conceptos/index';
import Contratista from '../Contratistas/index';
import Catalogos from '../Catalogos/index';

const Account = (url) => {
  return (
    <PasswordUpdate infoUrl={url} />
  )
}

const EmployessRoute = (open) => {
  return (
    <Employees
      openSide={open}
    />
  );
}

const RolesRoute = (open, close) => {
  return (
    <Roles
      openSide={open}
      closeTab={close}
    />
  )
}

const CategoriesRote = () => {
  return (
    <Categories />
  )
}

const ConceptsRote = () => {
  return (
    <Concepts />
  )
}

const ContratistaRoute = (open) => {
  return (
    <Contratista
      openSide={open}
    />
  );
}

const CatalogosRote = (open, props) => {
  return (
    <Catalogos openSide={open} props1={props} />
  )
}


const wrappedRoutes = (openSide, closeTab, valueTab, contentSideBar, urls) => {

  return (
    <div>
      <Layout valueTab={valueTab} content={contentSideBar} closeTab={closeTab} />
      <div className="container__wrap">
        {/*<Route exact path="/" component={Inscripcion} />*/}
       
        <Route path="/home" render={() => EmployessRoute(openSide)} />
        <Route path="/empleados" render={() => EmployessRoute(openSide)} />
        <Route path="/categorias" render={() => CategoriesRote()} />
        <Route path="/conceptos" render={() => ConceptsRote()} />
        <Route path="/roles" render={() => RolesRoute(openSide, closeTab)} />
        <Route path="/contratistas" render={() => ContratistaRoute(openSide)} />
        <Route path="/catalogos/:idObra/:idContratista" render={(props) => CatalogosRote(openSide, props)} />
        <Route path="/catalogos-obras/:idObra" render={(props) => CatalogosRote(openSide, props)} />
        {/* <Route path="/cursos" component={CursosRoute} />*/}

        <Route path="/account" render={() => Account(urls)} />
        <Route path="/logout" component={LogOut} />
        {/* If route is not found then redirect to 404 error screen */}
        {/* <Redirect from="*" to="/404" /> */}
      </div>
    </div>
  )
}


class Router extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired,
  };

  state = {
    sidebarcomplementCollapse: true,
    contentSideBar: ""
  };

  componentDidUpdate(prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      console.log('Route change!');
      this.setState(prevState => ({
        sidebarcomplementCollapse: true,
      }))
    }
  }

  changeSidebarVisibilityComplement = (content) => {
    console.log("SetState de cambio en el router")
    this.setState({
      sidebarcomplementCollapse: false,
      contentSideBar: content
    });
  };


  closeTab = () => {
    this.setState({
      sidebarcomplementCollapse: true,
      contentSideBar: ""
    });
  };


  render() {

    let url = this.props.location.pathname
    return (
      <MainWrapper>
        <main>
          <Switch>
            {/* <Route exact path="/" component={LogIn} />
          <Route exact path="/log_in" component={LogIn} />
          <Route exact path="/forgot" component={ForgotForm} /> */}
            {/* <Route path="/404" component={NotFound404} /> */}
            <Route path="/" render={() => wrappedRoutes(this.changeSidebarVisibilityComplement, this.closeTab, this.state.sidebarcomplementCollapse, this.state.contentSideBar, url)} />
          </Switch>
        </main>
      </MainWrapper>
    );
  }
}


export default withRouter(props => <Router {...props} />);
