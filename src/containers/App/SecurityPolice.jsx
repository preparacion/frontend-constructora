import { createContext } from 'react'
import { createContextualCan } from '@casl/react'

export const SecurityPolice = createContext()
export const Can = createContextualCan(SecurityPolice.Consumer)