import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import MainWrapper from './MainWrapper';
import LogIn from '../LogIn/index';
import ForgotForm from '../PasswordRecovery/index';
import Router from './Router';

class RouterHandler extends Component {
    constructor(props) {
        super(props);
        this.state = {
            auth: false,
        }
    }

    componentDidMount() {
        if (localStorage.getItem('authStudent') === 'true') {
            this.setState({
                auth: true,
            });
        }
    }

    render() {
        if (localStorage.getItem('authStudent') !== 'true') {
            return (
                <MainWrapper>
                    <main>
                        <Switch>
                            <Route exact path="/" component={LogIn} />
                            <Route exact path="/log_in" component={LogIn}/>
                            <Route exact path="/forgot" component={ForgotForm} />
                        </Switch>
                    </main>
                </MainWrapper>
            )
        } else {
            return (
                    <Router />
            )
        }
    }
} export default RouterHandler
