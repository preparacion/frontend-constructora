import React, { useState, useEffect, Fragment } from 'react';
import { Col, Row, Card } from 'reactstrap';
import { fetchAsync, urlCatalog, urlContratistas, urlObras } from '../../helpers/Globals/globals';
import { showMessageLoading, showMessageWarning, } from '../../helpers/MessageAndNotificationUtils';
import AddConcepToCatalog from './components/AddConcepToCatalog';
import { Header } from './components/Header';
import TableEstimaciones from './components/Table';
import ModalConfirmation from "../Modal/ModalConfirmation";
import EmptyComponent from "../../helpers/Empty/EmptyComponent";
import Contratista from './components/Contratista';
import moment from 'moment';
import 'antd/dist/antd.css';
import { Pagination } from 'antd';
import {
  creaImporte, creaImporteEstimacionActual,
  creaCantidadAcumulado, creaImporteAcumulado, createPorcentaje
} from './components/UpdateTable';
import LoadingComponent from "../../helpers/LoadingComponent";


let closeLoading = "";

function Catalogos(props) {

  //creamos los estado y effect
  const [allCatalogos, setAllCatalogos] = useState([])
  const [catalogos, setCatalogos] = useState({})
  const [addConceptModal, setAddConceptModal] = useState(false)
  const [eliminarModal, setEliminarModal] = useState(false)
  const [idEliminar, setIdEliminar] = useState(false)
  const [contratista, setContratista] = useState({})
  const [actualPage, setActualPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [actualObra, setActualObra] = useState(1);
  //estados para generar los datos en la tabla
  const [copyActualCatalog, setCopyActualCatalog] = useState({})
  //const [newEdit, setNewEdit] = useState({})
  const [update, setUpdate] = useState(false)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    //validamos si es por obra o por contratista
    if (props.props1.match.path === "/catalogos-obras/:idObra") {
      getCatalogsObra()
      getObraById()
    } else {
      getCatalogs()
      getContratisa()
      getObraById()
    }

  }, [])

  //crea un catalogo para poder añadir conceptos
  const createCatalog = (bodyConcep) => {

    let body = new Object()
    body = {
      contratista: props.props1.match.params.idContratista,
      obra: props.props1.match.params.idObra,
    }

    fetchAsync(urlCatalog, JSON.stringify(body), "POST", "")
      .then(
        (data) => {
          console.log(data)
          //getCatalogs()
          addConcept(bodyConcep, data._id)
        })
      .catch(
        (reason) => {
          console.log(reason)
        });
  }

  //const getCatalogs = (contratistaData, obraId) => {
  const getCatalogs = () => {
    setLoading(false)
    //return fetchAsync(urlCatalog +  + "/" + props.props1.match.params.idObra, "", "GET", "")
    return fetchAsync(urlCatalog + props.props1.match.params.idObra + "/" + props.props1.match.params.idContratista, "", "GET", "")
      .then(
        (result) => {

          if (result.length > 0) {
            //this.state.groupInfo.schedules.map(a => ({ ...a }))
            let datos = result.map(a => ({ ...a }))
            datos.reverse()//invetimos el array paara la paginacion
            //let actualCat = datos[result.length - 1]
            let actualCat = datos[0]//obtenemos el ultimo catalogo
            setActualPage(1)//Setea la paginacion en el numero 1
            setCatalogos(actualCat)//guarda el valor del ultimo catalogo 
            setAllCatalogos(datos)//guarda el array de lso catalogos
            setTotalPages(result.length * 10)//setea el numero de paginas

            //agregamos una copia d elos datos que ser la que se va a editar
            setCopyActualCatalog(actualCat)

          }
          setLoading(true)
        }).catch((reason) => {
          console.log(reason.message)
        });
  }

  //////Trae los catalogos por OBRA////////
  const getCatalogsObra = () => {
    setLoading(false)
    //return fetchAsync(urlCatalog +  + "/" + props.props1.match.params.idObra, "", "GET", "")
    return fetchAsync(urlCatalog + "obra/" + props.props1.match.params.idObra, "", "GET", "")
      .then(
        (result) => {

          if (result.length > 0) {
            //this.state.groupInfo.schedules.map(a => ({ ...a }))
            let datos = result.map(a => ({ ...a }))
            datos.reverse()//invetimos el array paara la paginacion
            //let actualCat = datos[result.length - 1]
            let actualCat = datos[0]//obtenemos el ultimo catalogo
            setActualPage(1)//Setea la paginacion en el numero 1
            setCatalogos(actualCat)//guarda el valor del ultimo catalogo 
            setAllCatalogos(datos)//guarda el array de lso catalogos
            setTotalPages(result.length * 10)//setea el numero de paginas

            //agregamos una copia d elos datos que ser la que se va a editar
            setCopyActualCatalog(actualCat)

          }
          setLoading(true)
        }).catch((reason) => {
          console.log(reason.message)
        });
  }

  //agregar conceptos al catalogo
  const addConcept = (body, catalogId = "") => {
    closeLoading = LoadingMessage()
    if (catalogId != "") {
      fetchAsync(urlCatalog + "addConcept/" + catalogId, JSON.stringify(body), "PUT", "")
        .then(
          (data) => {
            getCatalogs()
            setAddConceptModal(false)
            closeLoading();
          }).catch((reason) => {
            console.log(reason)
          });
    } else {
      fetchAsync(urlCatalog + "addConcept/" + catalogos._id, JSON.stringify(body), "PUT", "")
        .then(
          (data) => {
            getCatalogs()
            setAddConceptModal(false)
            closeLoading();
          }).catch((reason) => {
            console.log(reason)
          });
    }
  }

  //editar conceptos en el catalogo actual
  const editConcept = (row) => {
    closeLoading = LoadingMessageEditCell()
    let body = new Object()

    /*body = {
      concept: {
        cantidad: row.cantidad.toString(),
        importe: row.importe.toString(),
        //cantidadAcumulado: row.cantidadAcumulado,cantidad_Acumulado
        cantidadAcumulado: cantidad_Acumulado(row.cantidad, row.cantidadEstimacionActual),
        //importeAcumulado: row.importeAcumulado,importe_Acumulado
        importeAcumulado: importe_Acumulado(row.importe, importe_EstimacionActual(row.cantidadEstimacionActual, row.concepto.pu)),
        porcentaje: porcentaje_(row.cantidad, cantidad_Acumulado(row.cantidad, row.cantidadEstimacionActual)),
        cantidadEstimacionActual: row.cantidadEstimacionActual,
        //importeEstimacionActual: row.importeEstimacionActual,
        importeEstimacionActual: importe_EstimacionActual(row.cantidadEstimacionActual, row.concepto.pu),
      }
    }*/

    body = {
      concept: {
        cantidad: row.cantidad,
      }
    }
    console.log(body)
    fetchAsync(urlCatalog + "editConcept/" + catalogos._id + "/" + row._id, JSON.stringify(body), "PUT", "")
      .then(
        (data) => {
          getCatalogs()
          closeLoading();
        }).catch(
          (reason) => {
            console.log(reason)
          });
  }

  //eliminar Concepto de un catalogo
  const deleteConcept = (idConcepto) => {
    closeLoading = LoadingMessageDelete()
    fetchAsync(urlCatalog + "deleteConcept/" + catalogos._id + "/" + idConcepto, "", "DELETE", "")
      .then(
        (data) => {
          getCatalogs()
          closeLoading();
        })
      .catch(
        (reason) => {
          console.log(reason)
        });
  }

  //traemos los datos del contratista
  const getContratisa = () => {

    //return fetchAsync(urlCatalog +  + "/" + props.props1.match.params.idObra, "", "GET", "")
    return fetchAsync(urlContratistas + props.props1.match.params.idContratista, "", "GET", "")
      .then(
        (result) => {
          //console.log(result)
          setContratista(result)
        }).catch((reason) => {
          console.log(reason.message)
        });
  }

  //traemos los datos de la obra
  const getObraById = () => {

    return fetchAsync(urlObras + props.props1.match.params.idObra, "", "GET", "")
      .then(
        (result) => {
          console.log(result)
          setActualObra(result)
        }).catch((reason) => {
          console.log(reason.message)
        });
  }

  //crea un catalogo para poder añadir conceptos
  const createCatalogConcepts = () => {
    let body = new Object()

    body = {
      contratista: props.props1.match.params.idContratista,
      obra: props.props1.match.params.idObra,
      conceptos: catalogos.conceptos,
    }

    console.log(body)

    fetchAsync(urlCatalog, JSON.stringify(body), "POST", "")
      .then(
        (data) => {
          console.log(data)
          getCatalogs()
        })
      .catch(
        (reason) => {
          console.log(reason)
        });
  }

  //valida si existe un catalogo de loc onrario primero se crea uno y se agrega el row
  const addConceptOrcreateCatalog = (body) => {
    if (Object.keys(catalogos).length != 0) {
      addConcept(body)
      //console.log("AgregarConcepto")
      return null

    } else {
      createCatalog(body)
      //console.log("crearCatalogo")
      return null
    }
  }

  //abre el modal mensajes y setea el id a eliminar
  const onClickTable = (row) => {
    //cambiamos el state para abrir el modal del mesnaje
    setEliminarModal(true)

    //asignamos el valor id a borrar al estado
    setIdEliminar(row._id)
  }

  //asigna el valor para la tabla al pasara de pagina
  const nextPage = (number) => {
    //clocarlo en el state
    //setActualPage(number);
    setCatalogos(allCatalogos[number - 1])
  }

  //MENSAJES///
  //crea uan funcion para lamzar el mensaje de loading
  const LoadingMessage = () => {
    return showMessageLoading('Guardando..', 0);
  };

  const LoadingMessageDelete = () => {
    return showMessageLoading('Eliminando..', 0);
  };

  const LoadingMessageEditCell = () => {
    return showMessageLoading('Calculando..', 0);
  };

  //////Nuevo update Table/////////
  const getRowDataTable = (column, row) => {

    //let conceptoEditar = copyActualCatalog.conceptos.find(value => value._id === row._id)
    console.log("UpdateTable")
    console.log(column)
    console.log(row)
    console.log(copyActualCatalog)
    console.log(catalogos)
    console.log("UpdateTable")

    const cloneConceptos = [...catalogos.conceptos]

    //cuando se edita la cantidad presupuesto
    if (column.dataField === "cantidad") {
      cloneConceptos.map((value) => {
        if (value._id === row._id) {

          value.cantidad = row.cantidad
          value.importe = creaImporte(row.cantidad, row.concepto.pu)
          return null
        }
      })

      catalogos.conceptos = cloneConceptos
      const newDataTable = catalogos
      setUpdate((update) ? false : true)
      setCatalogos(newDataTable)

    } else if (column.dataField === "cantidadEstimacionActual") {
      cloneConceptos.map((value) => {
        if (value._id === row._id) {

          value.cantidadEstimacionActual = row.cantidadEstimacionActual
          value.importeEstimacionActual = creaImporteEstimacionActual(row.cantidadEstimacionActual, row.concepto.pu)
          //sacamos los Acumulados
          value.cantidadAcumulado = creaCantidadAcumulado(row.cantidadAcumuladoAnterior, row.cantidadEstimacionActual)
          value.importeAcumulado = creaImporteAcumulado(creaCantidadAcumulado(row.cantidadAcumuladoAnterior, row.cantidadEstimacionActual), row.concepto.pu)
          //sacamos el porcentaje
          value.porcentaje = createPorcentaje(creaCantidadAcumulado(row.cantidadAcumuladoAnterior, row.cantidadEstimacionActual), row.cantidad)
          return null
        }
      })

      catalogos.conceptos = cloneConceptos
      const newDataTable = catalogos
      setUpdate((update) ? false : true)
      setCatalogos(newDataTable)

    }

    console.log("+++++++")
    console.log(cloneConceptos)
    console.log(catalogos.conceptos.length)
    console.log("+++++++")

  }

  return (
    <Fragment>
      <Card className="">

        <Row className="estimacionesHeaderPag">
          <Col xs="12">

            <Header
              titulo="Estimaciones"
              setAddConceptModal={setAddConceptModal}
              createCatalogConcepts={createCatalogConcepts}
              catalogos={catalogos}
            />
            <Pagination
              defaultCurrent={actualPage}
              total={totalPages}
              onChange={(current) => { nextPage(current) }}
            />
          </Col>
        </Row>

        <Row className="estimacionesComp">
          <Col >
            <Contratista info={contratista} obra={actualObra} />
          </Col>
        </Row>

        <Row>
          {loading === true ?
            Object.keys(catalogos).length != 0 && catalogos.conceptos.length > 0 ?
              <div className="scrollEstimaciones3">
                <TableEstimaciones
                  dataTable={catalogos ? catalogos.conceptos : null}
                  //this.state.groupInfo.schedules.map(a => ({ ...a }))
                  editConcept={editConcept}
                  onClickTable={onClickTable}
                  getRowDataTable={getRowDataTable}
                />
              </div>
              :
              <Col sm="12" md={{ size: 6, offset: 3 }}>
                <EmptyComponent message="Aún no hay filas creadas" />
              </Col>
            : <LoadingComponent />}
        </Row>

      </Card>

      {(addConceptModal) ? <AddConcepToCatalog
        valueModal={addConceptModal}
        close={setAddConceptModal}
        idContratista={props.props1.match.params.idObra}
        obraId={props.props1.match.params.idObra}
        addConcept={addConcept}
        addConceptOrcreateCatalog={addConceptOrcreateCatalog}
      /> : null}

      <ModalConfirmation
        closeModal={setEliminarModal}
        valueModal={eliminarModal}
        callback={deleteConcept}
        value={idEliminar}
        titulo="Confirmación : ¿estas seguro que deseas eliminar este registro?"
      //closeParent={this.onCloseModal.bind(this)}
      />
    </Fragment>
  )
}

export default Catalogos