import React, { Fragment } from 'react';
import 'antd/dist/antd.css';
import { Select } from 'antd';
import {
  Col, Row, Card, CardBody, Form, FormGroup, Label, Input
} from 'reactstrap';
import {showDate} from '../../../helpers/Globals/funciones';


const ShowListCatalogs = ({ allCatalogos, setCatalogos }) => {



  //muestra la lista de conceptos en el select
  const showCatalogs = () => {
    //const date = new Date()

    return allCatalogos.map((value, index) => {
      // eslint-disable-next-line react/jsx-no-undef
      return <Option key={index}
        value={value._id}
      //title={value.concepto}value.createdAt
      >{showDate(value.createdAt)}</Option>
    })
  }
  
  const handleChangeSelect = (value) => {
    let tableInfo = allCatalogos.find(x => x._id === value)
    console.log("NUevoInformacio")
    console.log(tableInfo)
    console.log("NUevoInformacio")
    setCatalogos(tableInfo)

  }

  return (

    <Fragment>
      <span className="form__form-group-label">Selecionar Estimaciones anteriores</span>
      <Col sm={12} md={12} className="marginSelectAdd mb-2">
        <Select
          name="concepto"
          //value={datosForm.concepto}
          showSearch
          style={{ width: 450 }}
          placeholder="Seleccionar estimación"
          onChange={handleChangeSelect}
        >
          {
            showCatalogs()
          }
        </Select>
      </Col>
    </Fragment>
  )
}
export default ShowListCatalogs