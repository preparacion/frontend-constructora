import React from 'react';
import PropTypes from 'prop-types';
import {
  Col, Row, Card, CardBody, Button,
  ButtonToolbar, Container, ModalHeader, CardTitle
} from 'reactstrap';
import { Link } from "react-router-dom";
import 'antd/dist/antd.css';
import { Divider } from 'antd';
import { Popconfirm, message, } from 'antd';

const text = '¿Seguro que deseas guardar la estimación?';

function confirm() {
  message.info('Clicked on Yes.');
}

export const Header = ({ titulo, setAddConceptModal, createCatalogConcepts, catalogos }) => (
  <Row>

    <Col xs="12" sm="12" md="6">
      <Divider orientation="left"> <p className="titleEstimacion">{titulo}</p></Divider>
    </Col>
    <Col xs="12" sm="12" md="6" className="buttonSalir">

      <ButtonToolbar className="float-right">
        <Popconfirm placement="bottom" title={text} onConfirm={() => createCatalogConcepts(true)} okText="Si" cancelText="No">
          <Button color="success" size="sm"
            //onClick={() => createCatalogConcepts(true)}
            //disabled={Object.keys(catalogos).length === 0 || catalogos.conceptos.length === 0 ? "disabled" : ""}
          >
            <p>Guardar estimacion</p>
          </Button>
        </Popconfirm>
        <Button color="success" size="sm"
          onClick={() => setAddConceptModal(true)}
          //disabled={Object.keys(catalogos).length === 0 || catalogos.conceptos.length === 0 ? "disabled" : ""}
        >
          <p>Agregar fila</p>
        </Button>

      <Link to="/contratistas">
        <Button className="btn float-right icon" size="sm" color="success"
        >
          <p>Regresar</p>
        </Button>
      </Link>

      </ButtonToolbar>

    </Col>
  </Row >
)

Header.propTypes = {
  titulo: PropTypes.string.isRequired
}


export const HeaderAdd = ({ titulo, onCloseModal, crearFila }) => (
  <Row>

    <Col xs="12" sm="12" md="6">
      <h3>{titulo}</h3>
    </Col>
    <Col xs="12" sm="12" md="6" className="">

      <ButtonToolbar className="float-right">
        <Button color="success" size="sm"
          onClick={() => crearFila()}
        //disabled={this.state.bloquear === true ? "disabled" : ""}
        >
          <p>Guardar</p>
        </Button>
        <Button size="sm"
          onClick={() => onCloseModal()}>
          <p>Salir</p>
        </Button>
      </ButtonToolbar>

    </Col>
  </Row>
)

HeaderAdd.propTypes = {
  titulo: PropTypes.string.isRequired
}

