import React from 'react';

export const creaImporte = (cantidad, pu) => {
  let _cantidad = parseFloat(cantidad.replace(/,/g, ''))
  let _precioUnitario = parseFloat(pu)
  let total = (_cantidad * _precioUnitario).toFixed(2);
  //return total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  return total
}

export const creaImporteEstimacionActual = (cantidadEstimacionActual, pu) => {
  let _cantidadEstimacionActual = parseFloat(cantidadEstimacionActual.replace(/,/g, ''))
  let _precioUnitario = parseFloat(pu)
  let total = (_cantidadEstimacionActual * _precioUnitario).toFixed(2);
  //return total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  return total
}

export const creaCantidadAcumulado = (cantidadAcumuladoAnterior, cantidadEstimacionActual) => {
  let _cantidadAcumuladoAnterior = parseFloat(cantidadAcumuladoAnterior)
  let _cantidadEstimacionActual = parseFloat(cantidadEstimacionActual.replace(/,/g, ''))
  let total = (_cantidadAcumuladoAnterior + _cantidadEstimacionActual).toFixed(2);
  //return total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  return total
}

export const creaImporteAcumulado = (cantidadAcumulado, pu) => {

  let _cantidadAcumulado = parseFloat(cantidadAcumulado.replace(/,/g, ''))
  let _pu = parseFloat(pu)
  let total = (_cantidadAcumulado * _pu).toFixed(2);
  //return total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  return total
}

export const createPorcentaje = (cantidadAcumulado, cantidad) => {
  let _cantidadAcumulado = parseFloat(cantidadAcumulado)
  let _cantidad = parseFloat(cantidad)
  let total = ((_cantidadAcumulado / _cantidad) * 100).toFixed(0);
  //if(total)
  return total
}