import React, { useState, useEffect, Fragment } from 'react';
import {
  Col, Row, Card, CardBody, Form, FormGroup, Label, Input
} from 'reactstrap';
import { showMessageWarning, } from '../../../helpers/MessageAndNotificationUtils';
import {
  fetchAsync, urlConcepts,
} from '../../../helpers/Globals/globals';
import Modal from 'react-responsive-modal';
import { HeaderAdd } from './Header';
import 'antd/dist/antd.css';
import { Select } from 'antd';

function AddConcepToCatalog({ valueModal, close, addConcept, addConceptOrcreateCatalog }) {

  const stateInicial = {
    cantidad: "",
    costoUnitario: "",
    concepto: "",
    descripcion: "",
  }

  //creamos sus estados
  const [datosForm, setDatosForm] = useState(stateInicial)
  const [conceptos, setConceptos] = useState([])

  useEffect(() => {
    getConcepts()
  }, [])

  //metodo que se trae los conceptos
  const getConcepts = () => {
    return fetchAsync(urlConcepts, "", "GET", "")
      .then(
        (result) => {
          setConceptos(result)
        })
      .catch(
        (reason) => {
          console.log(reason.message)
        });
  }

  //guarda los datos
  const crearFila = () => {

    if (datosForm.cantidad == null || datosForm.cantidad == "") {
      showMessageWarning("Atención! Debes agregar una cantidad", 2)
    } else if (datosForm.concepto == null || datosForm.concepto == "") {
      showMessageWarning("Atención! Debes seleccionar un concepto", 2)
    } else {

      let body = {
        concept: {
          concepto: datosForm.concepto,
          cantidad: datosForm.cantidad,
          cu: datosForm.costoUnitario,
        }
      }
      //console.log(body)
      //addConcept(body)
      addConceptOrcreateCatalog(body)

      //reiniciar los estados
      setDatosForm(stateInicial)
    }
  }

  //FUNCIONES//
  //maneja el onChange de los inputs
  const handleChange = e => {
    setDatosForm({
      ...datosForm, //recordar copiar el objeto actual para no eliminar la sotras propiedades
      [e.target.name]: e.target.value
    })
  }

  //funcion que cierra el modal
  const onCloseModal = () => {
    close(false)
  }

  //muestra la lista de conceptos en el select
  const showConceptos = () => {
    return conceptos.map((value, index) => {
      // eslint-disable-next-line react/jsx-no-undef
      return <Option key={index}
        value={value.clave + "_" + value._id + "_" + value.pu}
        title={value.concepto}
      >{value.clave+" "+value.concepto}</Option>
    })
  }

  //setea el valor del select 
  const handleChangeSelect = (value, e) => {
    setDatosForm({
      ...datosForm, //recordar copiar el objeto actual para no eliminar la sotras propiedades
      concepto: value.split("_")[1],
      costoUnitario: value.split("_")[2],
      descripcion: e.props.title
    })
  }

  return (
    <Modal
      open={valueModal}
      onClose={onCloseModal}
      center={true}
      closeOnEsc={false}
      showCloseIcon={false}
      closeOnOverlayClick={false}
    >
      <Card className="formConceptCatalog">
        <Row>
          <Col xs="12">
            <HeaderAdd
              titulo="Agregar concepto"
              onCloseModal={onCloseModal}
              crearFila={crearFila}
            />
          </Col>
        </Row>


        <span className="form__form-group-label">Ver claves</span>
        <Col sm={12} md={12} className="marginSelectAdd mb-2">
          <Select
            name="concepto"
            //value={datosForm.concepto}
            showSearch
            style={{ width: 450 }}
            placeholder="Seleccionar una clave"
            onChange={handleChangeSelect}
          >
            {
              showConceptos()
            }
          </Select>
        </Col>

        <Form >
          <FormGroup>
            <Label htmlFor="cantidad">Cantidad</Label>
            <Input
              type="number"
              name="cantidad"
              onChange={handleChange}
              value={datosForm.cantidad}
            />
          </FormGroup>
          <FormGroup>
            <Label htmlFor="costoUnitario">Precio unitario</Label>
            <Input
              disabled
              type="number"
              name="costoUnitario"
              //onChange={handleChange}
              value={datosForm.costoUnitario}
            />
          </FormGroup>
          <FormGroup>
            <Label htmlFor="costoUnitario">Concepto</Label>
            <Input
              disabled
              type="text"
              name="descripcion"
              //onChange={handleChange}
              value={datosForm.descripcion}
            />
          </FormGroup>

        </Form>
      </Card>

    </Modal>
  )
}
export default AddConcepToCatalog