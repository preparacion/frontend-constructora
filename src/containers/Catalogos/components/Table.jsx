import React, { PureComponent, Component } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import cellEditFactory from 'react-bootstrap-table2-editor';

import { Col, Button, Row } from 'reactstrap';
import { COLOR_SORT_TABLES } from '../../../helpers/Tables/constants';
import SortTable from '../../../helpers/Tables/sortTable';
import { month } from '../../../helpers/Fichas/constants';
import LoadingComponent from "../../../helpers/LoadingComponent";
import Can from '../../../helpers/Secure/Can';

class TableEstimaciones extends PureComponent {


  eliminarIconAlumno = (cell, row) => {
    if (cell) {
      return (
        <div>
          <Can do="put" on="/users/">
            <div className="sameLineIcons colorIconDelete"
              onClick={() => {
                this.props.onClickTable(row)
              }}
            >
              <i className="material-icons">delete</i>
            </div>
          </Can>
        </div>
      )
    } else {
      return " "
    }
  }

  //Funcion que enumera las filas de la lista
  generaNum = (cell, row, rowIndex) => {
    if (row) {
      return (rowIndex + 1)
    } else {
      return ""
    }
  }

  importe = (cell, row) => {
    return cell.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  precioUnidad = (cell, row) => {
    return cell.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  cantidad = (cell, row) => {
    return cell.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  showName = (cell, row) => {
    return `${cell.clave} ${cell.concepto}`
  }

  porcentaje = (col, row) => {
    return <span>{col} %</span>
  }

  importeEstimacionActual = (cell, row) => {
    return cell.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  render() {
    //this.addCantidadPu()
    console.log("TableCatalogosContratista")
    console.log(this.props.dataTable)
    console.log("TableCatalogosContratista")
    const cloneData = this.props.dataTable != undefined ? this.props.dataTable.map(a => ({ ...a })) : []
    const headerSortingStyle = { backgroundColor: COLOR_SORT_TABLES };
    let { SearchBar } = Search;

    const columns = [
      {
        dataField: '',
        text: '#',
        formatter: this.generaNum,
        headerStyle: (colum, colIndex) => {
          return { width: '30px', textAlign: 'center', color: "black" };
        },
        //headerSortingStyle,
        align: 'center',
        editable: false,
        style: {
          textAlign: "center",
          verticalAlign: 'middle',
        },
        footer: ''
      },
      {////////CONCEPTO////////////////////
        dataField: 'concepto',
        text: 'Concepto',
        headerStyle: (colum, colIndex) => {
          return { textAlign: 'center', color: "black" };
        },
        headerAlign: 'center',
        align: 'laft',
        editable: false,
        formatter: this.showName,
        style: {
          textAlign: "center",
          verticalAlign: 'middle',
        },
        footer: ''
      },
      {//////////UNIDAD//////////////////////////
        dataField: 'concepto.unidad',
        text: 'U',
        sort: true,
        sortCaret: (order, column) => {
          return <SortTable order={order} colum={column} />
        },
        headerSortingStyle,
        headerAlign: 'center',
        align: 'center',
        editable: false,
        headerStyle: (colum, colIndex) => {
          return { textAlign: 'center', color: "black" };
        },
        style: {
          textAlign: "center",
          verticalAlign: 'middle',
        },
        footer: ''
      },
      { ///PRESUPUESTO CANTIDAD////////////////
        dataField: 'cantidad',
        text: 'Presupuesto / Cantidad',
        sort: true,
        sortCaret: (order, column) => {
          return <SortTable order={order} colum={column} />
        },
        headerSortingStyle,
        headerAlign: 'center',
        align: 'center',
        formatter: this.cantidad,
        editCellStyle: {
          backgroundColor: '#BDC3C7'
        },
        validator: (newValue, row, column) => {
          if (isNaN(newValue)) {
            return {
              valid: false,
              message: 'solo puedes ingresar numeros'
            };
          }
          if (newValue <= 0) {
            return {
              valid: false,
              message: 'la cantidad no debe ser menor o igual a 0'
            };
          }
          return true;
        },
        headerStyle: (colum, colIndex) => {
          return { textAlign: 'center', color: "black" };
        },
        style: {
          textAlign: "center",
          verticalAlign: 'middle',
        },
        footer: ''
        //footer: columnData => columnData.reduce((acc, item) => acc + item),
      },
      {////////////////PRECIO UNITARIO///////////////////////
        dataField: 'concepto.pu',
        text: 'P.U',
        sort: true,
        sortCaret: (order, column) => {
          return <SortTable order={order} colum={column} />
        },
        headerSortingStyle,
        headerAlign: 'center',
        align: 'center',
        formatter: this.precioUnidad,
        editCellStyle: {
          backgroundColor: '#BDC3C7'
        },
        editable: false,
        headerStyle: (colum, colIndex) => {
          return { textAlign: 'center', color: "black" };
        },
        style: {
          textAlign: "center",
          verticalAlign: 'middle',
        },
        //footer: columnData => columnData.reduce((acc, item) => acc + item),
        footer: 'SUMA'
      },
      {/////////IMPORTE///////////////////////
        dataField: 'importe',
        text: 'Importe',
        //sort: true,
        //sortCaret: (order, column) => {
        //return <SortTable order={order} colum={column} />
        //},
        //headerSortingStyle,
        headerAlign: 'center',
        align: 'right',
        formatter: this.importe,
        editable: false,
        headerStyle: (colum, colIndex) => {
          return { textAlign: 'center', color: "black" };
        },
        style: {
          textAlign: "center",
          verticalAlign: 'middle',
        },
        footer: columnData => columnData.reduce((acc, item) => parseFloat(acc) + parseFloat(item)).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),
        footerAlign: (column, colIndex) => 'right',
        footerStyle: {
          backgroundColor: '#F7DC6F'
        }
      },
      //vacias
      /////////////////////////////
      ////////////////////////////
      {
        dataField: 'cantidadAcumuladoAnterior',
        text: 'Cant.',
        sort: true,
        sortCaret: (order, column) => {
          return <SortTable order={order} colum={column} />
        },
        headerSortingStyle,
        headerAlign: 'center',
        align: 'center',
        //formatter: this.cantidad,
        editCellStyle: {
          backgroundColor: '#BDC3C7'
        },
        editable: false,
        headerStyle: (colum, colIndex) => {
          return { textAlign: 'center', color: "black" };
        },
        style: {
          textAlign: "center",
          verticalAlign: 'middle',
        },
        footer: ''
      },
      {
        dataField: 'importeAcumuladoAnterior',
        text: 'Importe',
        //sort: true,
        //sortCaret: (order, column) => {
        //return <SortTable order={order} colum={column} />
        //},
        //headerSortingStyle,
        headerAlign: 'center',
        align: 'right',
        //formatter: this.importe,
        editable: false,
        headerStyle: (colum, colIndex) => {
          return { textAlign: 'center', color: "black" };
        },
        style: {
          textAlign: "center",
          verticalAlign: 'middle',
        },
        footer: columnData => columnData.reduce((acc, item) => parseFloat(acc) + parseFloat(item)).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),
        footerAlign: (column, colIndex) => 'right',
        footerStyle: {
          backgroundColor: '#F7DC6F'
        }
      },
      /////////////////////////////////////
      //ENMEDIO EDITABLE//////////////
      {
        dataField: 'cantidadEstimacionActual',
        text: 'Agregar / Cantidad.',
        style: {
          textAlign: "center",
          verticalAlign: 'middle',
        },
        //formatter: this.eliminarIconAlumno,
        headerAlign: 'center',
        align: 'center',
        headerStyle: (colum, colIndex) => {
          return { textAlign: 'center', color: "black" };
        },
        footer: ''
      },
      {/////////IMPORTE BASE/////////
        headerAlign: 'center',
        dataField: 'importeEstimacionActual',
        text: 'Importe.',
        style: {
          textAlign: "center",
          verticalAlign: 'middle',
          //color: "#F7DC6F",
        },
        align: 'center',
        editable: false,
        headerStyle: (colum, colIndex) => {
          return { textAlign: 'center', color: "black" };
        },
        footer: columnData => columnData.reduce((acc, item) => parseFloat(acc) + parseFloat(item)).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),
        footerAlign: (column, colIndex) => 'right',
        footerStyle: {
          backgroundColor: '#F7DC6F'
        },
        formatter: this.importeEstimacionActual,
      },
      //FIN ENMEDIO EDITABLES////////
      ////////////////////////////////
      {///ACUMULADOS TOTALES CANTIDAD ACUMULADA///////////////
        dataField: 'cantidadAcumulado',
        text: 'Acumulado Actual / Cantidad',
        sort: true,
        sortCaret: (order, column) => {
          return <SortTable order={order} colum={column} />
        },
        headerSortingStyle,
        headerAlign: 'center',
        align: 'center',
        //formatter: this.cantidad,
        editCellStyle: {
          backgroundColor: '#F7DC6F'
        },
        editable: false,
        headerStyle: (colum, colIndex) => {
          return { textAlign: 'center', color: "black" };
        },
        style: {
          textAlign: "center",
          verticalAlign: 'middle',
        },
        footer: ''
      },
      {///IMPORTE ACUMULADO///////////////////////
        headerAlign: 'center',
        dataField: 'importeAcumulado',
        text: 'Acumulado Actual / Importe.',
        style: {
          textAlign: "center",
          verticalAlign: 'middle',
          //color: "#F7DC6F",
        },
        formatter: this.importe,
        align: 'center',
        editable: false,
        headerStyle: (colum, colIndex) => {
          return { textAlign: 'center', color: "black" };
        },
        footer: columnData => columnData.reduce((acc, item) => parseFloat(acc) + parseFloat(item)).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),
        //footerAlign: (column, colIndex) => 'right',
        footerStyle: {
          backgroundColor: '#F7DC6F'
        },
        footerAlign: (column, colIndex) => 'right',
      },
      {///PORCENTAGE///////////////////////
        headerAlign: 'center',
        dataField: 'porcentaje',
        text: '100%.',
        csvExport: false,
        style: {
          textAlign: "center",
          verticalAlign: 'middle',
          //color: "#F44336",
        },
        formatter: this.porcentaje,
        headerAlign: 'center',
        align: 'center',
        editable: false,
        headerStyle: (colum, colIndex) => {
          return { textAlign: 'center', color: "black" };
        },
      },
      /////////////////////////////
      /////////////////////////////
      // terminan las columnas vacias
      {
        headerAlign: 'center',
        dataField: '_id',
        text: 'Acciones',
        csvExport: false,
        style: {
          textAlign: "center",
          verticalAlign: 'middle',
          //color: "#F44336",
        },
        formatter: this.eliminarIconAlumno,
        headerAlign: 'center',
        align: 'center',
        editable: false,
      },
    ];

    let defaultSorted = [{
      dataField: 'name',
      order: 'desc'
    }];


    return (
      <Col xs={12} sm={12} md={12} >
        {this.props.dataTable !== undefined ?

          <div className="modal-body table-responsive1 modalEstimacionSize1">
            <ToolkitProvider
              keyField="_id"
              //key="_id"
              //data={this.props.dataTable}
              data={cloneData}
              columns={columns}
              //search={{ defaultSearch: '' }}
              //defaultSorted={defaultSorted}
              /*exportCSV={{
                fileName: `Lista de personal.csv`,
                exportAll: false,
                noAutoBOM: false,
              }}*/
              striped
            >
              {
                props => (
                  <div >

                    <BootstrapTable
                      className="tableTitlesColor1"
                      striped
                      hover
                      bordered={false}
                      {...props.baseProps}
                      noDataIndication={<LoadingComponent />}
                      expandRow={this.expandRow}
                      //headerClasses="header-classTable"
                      cellEdit={cellEditFactory({
                        mode: 'dbclick',
                        afterSaveCell: (oldValue, newValue, row, column) => {
                          console.log('After Saving Cell!!');
                          console.log(column)
                          console.log(row)
                          console.log('After Saving Cell!!');
                          this.props.getRowDataTable(column, row)
                          //this.props.editConcept(row)
                          /*if (column.dataField === "cantidad") {
                            this.props.editConcept(row)
                          }*/
                        }
                      })}
                    />
                  </div>
                )}
            </ToolkitProvider>
          </div>
          : " "}


      </Col>
    );
  }
}

export default TableEstimaciones;