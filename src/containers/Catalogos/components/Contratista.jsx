import React, { Fragment } from 'react';
import {
  Col, Row, Card, CardBody, Button, ButtonToolbar, Container, CardTitle
} from 'reactstrap';
import { getActualDate } from '../../../helpers/Globals/funciones';

const Contratista = ({ info = "", obra = "" }) => {
  const { name, aPaterno, aMaterno } = info;
  console.log("infoVer")
  console.log(info)
  return (
    <Row>
      <Col className="contratistasTitleTable" xs={6}>
        {Object.keys(info).length > 0 ? <h4>{`Contratista: ${name} ${aPaterno} ${aMaterno}`}</h4> : null}
        <h4>{`Obra: ${obra.name}`}</h4>
      </Col>
      <Col className="contratistasTitleTable" xs={6}>
        <h4>Fecha : {getActualDate()}</h4>
      </Col>
    </Row>
  )

}
export default Contratista