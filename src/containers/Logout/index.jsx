import React, { PureComponent } from 'react';
import {
    Redirect,
} from "react-router-dom";

class LogOut extends PureComponent {

    constructor() {
        super();
        this.state = {
            auth: false
        };
    }
    render() {
        localStorage.setItem('dataLoginStudent', null);
        localStorage.setItem('authStudent', false);
        return (<Redirect to="/" />);
    }
}

export default LogOut;
