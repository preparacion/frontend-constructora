import React, { PureComponent } from 'react';
import EyeIcon from 'mdi-react/EyeIcon';
import { fetchAsync, urlLoginStudent } from '../../../helpers/Globals/globals';
import {
  showNotification, 
} from '../../../helpers/MessageAndNotificationUtils';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import CheckBox from '../../../shared/components/form/CheckBox';
import { serviceToabilities } from "../../../helpers/Secure/utils";
import { ability, forceUpdate } from "../../../helpers/Secure/abilities"
import {
  Redirect,
} from "react-router-dom";


class LogInForm extends PureComponent {

  constructor() {
    super();
    this.state = {
      showPassword: false,
      email: "",
      password: "",
      auth: false,
      showError: false,
      errorMessage: "",
      colorMessage: "green"

    };
  }


  flogin(e) {
    e.preventDefault();
    var dataLogin = {
      email: this.state.email,
      password: this.state.password,
    }
    fetchAsync(urlLoginStudent, JSON.stringify(dataLogin), "POST", "default")
      .then(
        (data) => {
          if (data.success === true) {
            let data1 = data
            data1.allows = serviceToabilities(data.allows)
            localStorage.setItem('dataLoginStudent', JSON.stringify(data1));
            localStorage.setItem('authStudent', true);
            // ability.update(data1.allows)
            forceUpdate(data1.allows)
            this.setState({
              auth: true,
            });
          } else {
            let messageText = "Error: Contraseña o correo inválidos."
            let messageDescription = ""
            showNotification("error", messageText, messageDescription, 0)
            /*this.setState({
              showError: true,
              errorMessage: "Error: Contraseña o correo inválidos",
              colorMessage: 'red',
            });*/
          }
        }
      )
      .catch(
        (reason) => {
          console.log(reason.message)
          this.showError = true;
          this.errorMessage = reason.message;
          this.colorMessage = "red";

        }
      );
  }


  showPassword = (e) => {
    e.preventDefault();
    this.setState({
      showPassword: !this.state.showPassword,
    });
  };

  render() {
    if (this.state.auth === true) {
      return (<Redirect to="/" />);
    } else {
      return (
        <form className="form">
          <div className="form__form-group">

            <span className="form__form-group-label">Tú correo electrónico</span>
            <div className="form__form-group-field">

              <input
                name="email"
                type="email"
                onChange={(event) => {
                  this.setState({
                    email: event.target.value
                  });
                }}
                value={this.state.email}
                placeholder="heg@mail.com"
              />
            </div>
          </div>
          <div className="form__form-group">
            <span className="form__form-group-label">Tú contraseña</span>
            <div className="form__form-group-field">
              <input
                name="password"
                value={this.state.password}
                onChange={(event) => {
                  this.setState({
                    password: event.target.value
                  });
                }}
                type={this.state.showPassword ? 'text' : 'password'}
                placeholder="Contraseña"
              />
              <button
                className={`form__form-group-button${this.state.showPassword ? ' active' : ''}`}
                onClick={e => this.showPassword(e)}
              ><EyeIcon />
              </button>
            </div>
            <div className="account__forgot-password">
              <a><Link to="/forgot">¿Olvidaste tu contraseña?</Link></a>
            </div>
          </div>
          <div className="form__form-group">
            <div className="form__form-group-field">
              <CheckBox name="remember_me" label="Recordarme" onChange={() => { }} />
            </div>
          </div>
          <div className="account__btns">
            <Button onClick={(e) => this.flogin(e)} className="btn btn-primary account__btn"><a style={{ color: "black" }}>Entrar</a></Button>
          </div>
        </form>
      );
    }
  }
}

export default LogInForm;
