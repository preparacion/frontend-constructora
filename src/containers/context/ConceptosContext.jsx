import React, { Component } from 'react';
import { fetchAsync, urlConcepts } from '../../helpers/Globals/globals';


//creamos el context
const ConceptosContext = React.createContext()
//creamos el consumer y lo exportamos
export const ConceptosConsumer = ConceptosContext.Consumer

class ConceptosProvider extends Component {

  state = {
    conceptos: []
  }

  componentDidMount() {
    this.getConcepts()
  }
  

  getConcepts = () => {
    console.log("DesdeConceptosProvider")
    return fetchAsync(urlConcepts, "", "GET", "")
      .then(
        (result) => {

          this.setState({
            conceptos: result
          });

        })
      .catch(
        (reason) => {
          console.log(reason.message)
        });
  }

  //pasamos los valores que tendra este context 
  //e indicamos que tendra hijos  {this.props.children}

  render() {
    return (

      <ConceptosContext.Provider
        value={{
          conceptos: this.state.conceptos,
          getConcepts: this.getConcepts
        }}
      >
        {this.props.children}
      </ConceptosContext.Provider>

    )
  }
}

export default ConceptosProvider