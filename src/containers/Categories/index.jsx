import React, { Component } from 'react';
import { Col, Container, Row, Button, } from 'reactstrap';
import { fetchAsync, urlEmployees, urlCategories } from '../../helpers/Globals/globals';
import TableCategories from './components/table';
import AddIcon from 'mdi-react/AddIcon';
import ModalConfirmation from '../Modal/ModalConfirmation'
import AddCategory from './components/addCategory';
import EditCategory from './components/editCategory';
import Can from "../../helpers/Secure/Can";

class Categories extends Component {

  constructor(props) {
    super(props)
    this.state = {
      addModal: false,

      categories: [],
      actualcategorie: '',
      eliminar: false,
      idDelete: "",
      editModal: false,
      editInfo: [],

    }
  }

  componentDidMount() {
    this.getCategories()
  }


  openAddModal = (value) => {
    this.setState({
      addModal: value,
    })
  }

  //abre el modal para eliminar el empleado
  openEliminar = (value, idDelete) => {
    this.setState({
      eliminar: value,
      idDelete
    })
  }

  //abre el modal para eliminar el empleado
  openEdit = (value, editInfo) => {
    this.setState({
      editModal: value,
      editInfo
    })
  }

  //RED OPERATIONS///
  //fetch para obtener las categorias
  getCategories = () => {

    return fetchAsync(urlCategories, "", "GET", "")
      .then(
        (result) => {

          this.setState({
            categories: result
          });

        })
      .catch(
        (reason) => {
          console.log(reason.message)
        });
  }

  //funcion para eliminar una categoria
  deleteCategorie = (id) => {
    fetchAsync(urlCategories + id, "", "DELETE", "")
      .then(
        (data) => {
          this.getCategories()
        })
      .catch(
        (reason) => {
          console.log(reason.message)

        }
      );
  }



  render() {
    //console.log("RenderCategories")
    //console.log(this.state.editInfo)
    //console.log("RenderCategories")
    return (
      <Container className="dashboard">
        <Row>
          <Col sm={12} md={12} className="mt-2">

            <Row>
              <Col xs={12} sm={12} md={7} className="mt-2 titlesComponents">
                <div className="typography--inline">
                  <p className="subhead">Gestión de usuarios |</p>
                  <p style={{ color: "#3057af", marginLeft: "5px" }}>Categorías</p>
                </div>
              </Col>
              <Can do="post" on="/users/">
                <Col xs={12} sm={12} md={5} className="mt-2">
                  <Button className="btn float-right icon" size="sm" color="success"
                    style={{ marginRight: "15px" }}
                    onClick={() => this.openAddModal(true)}
                  >
                    <p><AddIcon />Añadir Categoría</p>
                  </Button>
                </Col>
              </Can>
            </Row>

            <Row>
              <Col sm={12} >
                <TableCategories
                  categories={this.state.categories}
                  openEliminarModal={this.openEliminar}
                  openEditModal={this.openEdit}
                //getRowInfo={this.getRowInfo.bind(this)}
                //categories={this.state.categories}
                />
              </Col>
              <Col sm={4}>
              </Col>
            </Row>


          </Col>
        </Row>

        <AddCategory
          openAddModal={this.openAddModal}
          valueAddModal={this.state.addModal}
          getCategories={this.getCategories}
        />

        <ModalConfirmation
          closeModal={this.openEliminar}
          valueModal={this.state.eliminar}
          callback={this.deleteCategorie}
          value={this.state.idDelete}
          titulo="Confirmación : ¿estas seguro que deseas eliminar este registro?"
        //closeParent={this.onCloseModal.bind(this)}
        />

        {(this.state.editModal) ? <EditCategory
          openEditModal={this.openEdit}
          valueModal={this.state.editModal}
          infoCategory={this.state.editInfo}
          getCategories={this.getCategories}
        /> : null}


      </Container>
    )
  }

}
export default Categories