import React, { PureComponent } from 'react';
import { Col, Container, Row, Card, CardBody } from 'reactstrap';
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import SortTable from '../../../helpers/Tables/sortTable';
import {COLOR_SORT_TABLES} from '../../../helpers/Tables/constants';
import LoadingComponent from "../../../helpers/LoadingComponent";
import Can from '../../../helpers/Secure/Can';

class TableSede extends PureComponent {

    //genera los iconos en la columna acciones
    eliminarIconAlumno = (cell, row) => {
        if (cell) {
            return (

                <div>
                    <Can do="put" on="/users/">
                        <div className="sameLineIcons mr-4 colorIconPencil"
                            onClick={() => {
                                console.log(cell)
                                this.props.openEditModal(true, row)

                            }}
                        >
                            <i className="material-icons ">edit</i>
                        </div>
                    </Can>

                    <Can do="put" on="/users/">
                        <div className="sameLineIcons colorIconDelete"
                            onClick={() => {
                                console.log(cell)
                                this.props.openEliminarModal(true, cell)

                            }}
                        >
                            <i className="material-icons">delete</i>

                        </div>
                    </Can>
                </div>
            )
        } else {
            return " "
        }
    }

    render() {

        const headerSortingStyle = { backgroundColor: COLOR_SORT_TABLES };
        const { SearchBar } = Search;
        const columns = [
            {
                dataField: 'name',
                text: 'Nombre',
                sort: true,
                headerAlign: 'center',
                align: 'center',
                sortCaret: (order, column) => {
                    return <SortTable order={order} colum={column} />
                },
                headerSortingStyle,

            },
            {
                dataField: 'monto',
                text: 'Sueldo',
                sort: true,
                headerAlign: 'center',
                align: 'center',
                sortCaret: (order, column) => {
                    return <SortTable order={order} colum={column} />
                },
                headerSortingStyle,
            },
            {
                dataField: 'sdi',
                text: 'SDI',
                sort: true,
                headerAlign: 'center',
                align: 'center',
                sortCaret: (order, column) => {
                    return <SortTable order={order} colum={column} />
                },
                headerSortingStyle,
            },
            {
                headerAlign: 'center',
                dataField: '_id',
                text: 'Acciones',
                csvExport: false,
                style: {
                    textAlign: "center",
                    verticalAlign: 'middle',
                    //color: "#F44336",
                },
                formatter: this.eliminarIconAlumno,
            },
        ];

        return (
            <Container >
                <Row>
                    <Col sm={12} md={12}>
                        <Card>
                            <CardBody>
                                <ToolkitProvider
                                    keyField="_id"
                                    data={this.props.categories}
                                    columns={columns}
                                    search
                                >
                                    {
                                        props => (
                                            <div >

                                                <SearchBar {...props.searchProps}
                                                    className="custome-search-field busqueda tableTitleTex"
                                                    style={{
                                                        //color: '#2980B9',
                                                        backgroundColor: "#ecf0f1",
                                                    }}
                                                    placeholder="Buscar una categoría"
                                                />

                                                <hr />
                                                <BootstrapTable
                                                    striped
                                                    hover
                                                    bordered={false}
                                                    //headerClasses="header-class"
                                                    {...props.baseProps}
                                                    noDataIndication={<LoadingComponent />}
                                                    headerClasses="header-classTable"
                                                />
                                            </div>
                                        )
                                    }
                                </ToolkitProvider>
                            </CardBody>
                        </Card>

                    </Col>
                </Row>
            </Container>
        );
    }
}
export default TableSede;
