import React, { Component } from 'react';
import { Col, Row, Card, CardBody, Button, ButtonToolbar, } from 'reactstrap';
import { fetchAsync, urlCategories } from '../../../helpers/Globals/globals';
import {
  showMessageLoading, showMessageWarning,
} from '../../../helpers/MessageAndNotificationUtils';
import Modal from 'react-responsive-modal';
import 'rc-checkbox/assets/index.css';
import 'antd/lib/date-picker/style/index.css';

let closeLoading = ""

class AddCategory extends Component {

  constructor(props) {
    super(props);
    this.state = {

      name: "",
      concept: "",
      sdi: "",
      bloquear: false,
    };

  }

  onCloseModal = () => {
    if (closeLoading != "") {
      closeLoading()
    }

    this.props.openAddModal(false);

    this.setState({
      name: "",
      concept: "",
      sdi: "",
      bloquear: false,
    })
  };

  //crea uan funcion para lamzar el mensaje de loading
  LoadingMessage = () => {
    return showMessageLoading('Registrando..', 0);
  };

  //valida y genera las advertencias necesarias para el formulario
  registerEmployee = () => {

    if (this.state.name == null || this.state.name == "") {
      showMessageWarning("Atención! Debes ingresar un nombre", 2)

    } else if (this.state.concept == null || this.state.concept == "") {
      showMessageWarning("Atención! Debes ingresar una cantidad en concepto", 2)
    } else {

      closeLoading = this.LoadingMessage()
      this.setState({ bloquear: true })//bloquea el boton editar

      // eslint-disable-next-line no-new-object
      let bodyEmploye = new Object()


      bodyEmploye = {
        name: this.state.name,
        monto: this.state.concept,
        sdi: parseFloat(this.state.sdi),
        //sdi: this.state.sdi,
      }

      //console.log(bodyEmploye)

      fetchAsync(urlCategories, JSON.stringify(bodyEmploye), "POST", "")
        .then(
          (data) => {
            closeLoading();
            this.props.getCategories() //callback a index para recargar la tabla
            this.onCloseModal()

          }).then(() => this.setState({ bloquear: false }))//desbloquea el boton editar)
        .catch(
          (reason) => {
            console.log(reason)
          });
    }

  }

  render() {
    //console.log("OpenMOdalEmpleados")
    return (

      <Modal
        open={this.props.valueAddModal}
        onClose={this.onCloseModal}
        center={true}
        closeOnEsc={false}
        showCloseIcon={false}
        closeOnOverlayClick={false}
      >
        <Card className="addCategoryCont">
          <CardBody>

            <Row>

              <Col sm={12} md={5} className="titileCategorie">
                <h3 className="addEditTitle">Crear categoria</h3>
              </Col>

              <Col sm={12} md={7}>
                <ButtonToolbar className="ml-1 float-right">
                  <Button size="sm"
                    onClick={() => this.onCloseModal()}>
                    <p>Salir</p>
                  </Button>
                  <Button color="success" size="sm"
                    onClick={() => this.registerEmployee()}
                    disabled={this.state.bloquear === true ? "disabled" : ""}
                  >
                    <p>Guardar</p>
                  </Button>
                </ButtonToolbar>
              </Col>
            </Row>

            <div className="form__form-group">
              <span className="form__form-group-label">Nombre</span>
              <div className="form__form-group-field">

                <input type="text"
                  className="form-control"
                  id="inpuName" autoComplete="new-password"
                  defaultValue={this.state.name}
                  onChange={(event) => {
                    this.setState({ name: event.target.value })
                    //this.state.name = event.target.value
                  }} />
              </div>
            </div>

            <div className="form__form-group">
              <span className="form__form-group-label">Sueldo</span>
              <div className="form__form-group-field">

                <input type="number" className="form-control" id="inpuConcept" name="concept"
                  defaultValue={this.state.concept}
                  onChange={(event) => {
                    this.setState({ concept: event.target.value })
                  }} />
              </div>
            </div>

            <div className="form__form-group">
              <span className="form__form-group-label">SDI</span>
              <div className="form__form-group-field">

                <input type="text" className="form-control" id="inpuConcept" name="sdi"
                  defaultValue={this.state.sdi}
                  onChange={(event) => {
                    this.setState({ sdi: event.target.value })
                  }} />
              </div>
            </div>


          </CardBody>
        </Card>
      </Modal>

    );
  }
}
export default AddCategory;


