import React, { Component } from 'react';
import { Col, Container, Row, Button, } from 'reactstrap';
import { fetchAsync, urlConcepts } from '../../helpers/Globals/globals';
import { showMessageLoading, showMessageWarning, } from '../../helpers/MessageAndNotificationUtils';
import TableConcepts from './components/table';
import AddIcon from 'mdi-react/AddIcon';
import ModalConfirmation from '../Modal/ModalConfirmation'
import Can from "../../helpers/Secure/Can";
import AddEditConcepts from './components/addEditConcept';
import EmptyComponent from "../../helpers/Empty/EmptyComponent";

let closeLoading = "";

class Concepts extends Component {

  constructor(props) {
    super(props)
    this.state = {
      concepts: [],
      eliminar: false,
      idDelete: "",
      editInfo: {},
      addEdit: false,
      empty: false,
    }
  }

  componentDidMount() {
    this.getConcepts()
  }

  //abre el modal para eliminar el empleado
  openEliminar = (value, idDelete) => {
    this.setState({
      eliminar: value,
      idDelete
    })
  }

  //abre el modal para eliminar el empleado
  openAddEdit = (value, editInfo) => {
    this.setState({
      addEdit: value,
      editInfo
    })
    if (closeLoading != "") {
      closeLoading();
    }
  }

  //RED OPERATIONS///

  //crear un concepto
  createConcept = (body) => {
    closeLoading = this.LoadingMessage()
    fetchAsync(urlConcepts, JSON.stringify(body), "POST", "")
      .then((data) => {
        closeLoading();
        this.getConcepts()
        this.openAddEdit(false)
      }).catch((reason) => {
        console.log(reason)
      });
  }

  //editar Concepto
  editConcept = (body, idConcept) => {
    closeLoading = this.LoadingMessageEdit()
    fetchAsync(urlConcepts + idConcept, JSON.stringify(body), "PUT", "")
      .then((data) => {
        closeLoading();
        this.getConcepts()
        this.openAddEdit(false)
      }).catch((reason) => {
        console.log(reason)
      });
  }

  //fetch para obtener los conceptos
  getConcepts = () => {
    return fetchAsync(urlConcepts, "", "GET", "")
      .then(
        (result) => {
          this.setState({
            concepts: result,
            empty: false
          });
          if (result.length < 1) {
            this.setState({
              empty: true
            })
          }
        }).catch((reason) => {
          console.log(reason.message)
        });
  }

  //funcion para eliminar una categoria
  deleteCategorie = (id) => {
    closeLoading = this.LoadingMessageBorrar()
    fetchAsync(urlConcepts + id, "", "DELETE", "")
      .then(
        (data) => {
          closeLoading();
          this.getConcepts()
        })
      .catch((reason) => {
        console.log(reason.message)
      }
      );
  }

  //MENSAJES///
  //crea uan funcion para lamzar el mensaje de loading
  LoadingMessage = () => {
    return showMessageLoading('Registrando..', 0);
  };

  LoadingMessageEdit = () => {
    return showMessageLoading('Editando..', 0);
  };

  LoadingMessageBorrar = () => {
    return showMessageLoading('Borrando..', 0);
  };

  render() {
    //console.log("RenderCategories")
    //console.log(this.state.editInfo)
    //console.log("RenderCategories")
    return (
      <Container className="dashboard">
        <Row>
          <Col sm={12} md={12} className="mt-2">

            <Row>
              <Col xs={12} sm={12} md={7} className="mt-2 titlesComponents">
                <div className="typography--inline">
                  <p className="subhead">Obras |</p>
                  <p style={{ color: "#3057af", marginLeft: "5px" }}>Conceptos</p>
                </div>
              </Col>
              <Can do="post" on="/users/">
                <Col xs={12} sm={12} md={5} className="mt-2">
                  <Button className="btn float-right icon" size="sm" color="success"
                    style={{ marginRight: "15px" }}
                    onClick={() => this.openAddEdit(true)}
                  >
                    <p><AddIcon />Añadir Concepto</p>
                  </Button>
                </Col>
              </Can>
            </Row>

            <Row>
              <Col sm={12} >
                {(this.state.empty) ? <EmptyComponent message="No hay conceptos agregados" /> : <TableConcepts
                  concepts={this.state.concepts}
                  openEliminarModal={this.openEliminar}
                  //openEditModal={this.openEdit}
                  openAddEdit={this.openAddEdit}
                />}
              </Col>
              <Col sm={4}>
              </Col>
            </Row>
          </Col>
        </Row>

        <ModalConfirmation
          closeModal={this.openEliminar}
          valueModal={this.state.eliminar}
          callback={this.deleteCategorie}
          value={this.state.idDelete}
          titulo="Confirmación : ¿estas seguro que deseas eliminar este registro?"
        //closeParent={this.onCloseModal.bind(this)}
        />

        {
          this.state.addEdit ? <AddEditConcepts
            //parametros para abrir el modal
            openAddModal={this.openAddEdit}
            valueAddModal={this.state.addEdit}
            //parametro para crear un concepto
            createConcept={this.createConcept}
            //parametro para editar un concepto
            editar={this.state.editInfo}
            editConcept={this.editConcept}
          /> : null
        }
      </Container>
    )
  }

}
export default Concepts