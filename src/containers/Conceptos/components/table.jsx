import React, { PureComponent } from 'react';
import { Col, Container, Row, Card, CardBody } from 'reactstrap';
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import SortTable from '../../../helpers/Tables/sortTable';
import LoadingComponent from "../../../helpers/LoadingComponent";
import Can from '../../../helpers/Secure/Can';
import PropTypes from 'prop-types';

class TableConcepts extends PureComponent {

    //genera los iconos en la columna acciones
    eliminarIconAlumno = (cell, row) => {
        if (cell) {
            return (

                <div>
                    <Can do="put" on="/users/">
                        <div className="sameLineIcons mr-4 colorIconPencil"
                            onClick={() => {
                                //console.log(row)
                                this.props.openAddEdit(true, row)

                            }}
                        >
                            <i className="material-icons ">edit</i>
                        </div>
                    </Can>

                    <Can do="put" on="/users/">
                        <div className="sameLineIcons colorIconDelete"
                            onClick={() => {
                                //console.log(cell)
                                this.props.openEliminarModal(true, cell)

                            }}
                        >
                            <i className="material-icons">delete</i>

                        </div>
                    </Can>
                </div>
            )
        } else {
            return " "
        }
    }

    render() {

        const headerSortingStyle = { backgroundColor: '#D6DBDF' };
        const { SearchBar } = Search;
        const columns = [
            {
                dataField: 'clave',
                text: 'Clave',
                sort: true,
                headerAlign: 'center',
                align: 'center',
                sortCaret: (order, column) => {
                    return <SortTable order={order} colum={column} />
                },
                headerSortingStyle,

            },
            {
                dataField: 'concepto',
                text: 'Concepto',
                sort: true,
                headerAlign: 'center',
                align: 'center',
                sortCaret: (order, column) => {
                    return <SortTable order={order} colum={column} />
                },
                headerSortingStyle,
            },
            {
                dataField: 'unidad',
                text: 'Unidad',
                sort: true,
                headerAlign: 'center',
                align: 'center',
                sortCaret: (order, column) => {
                    return <SortTable order={order} colum={column} />
                },
                headerSortingStyle,
            },
            {
                dataField: 'pu',
                text: 'Precio unitario',
                sort: true,
                headerAlign: 'center',
                align: 'center',
                sortCaret: (order, column) => {
                    return <SortTable order={order} colum={column} />
                },
                headerSortingStyle,
            },
            {
                headerAlign: 'center',
                dataField: '_id',
                text: 'Acciones',
                csvExport: false,
                style: {
                    textAlign: "center",
                    verticalAlign: 'middle',
                    //color: "#F44336",
                },
                formatter: this.eliminarIconAlumno,
            },
        ];

        return (
            <Container >
                <Row>
                    <Col sm={12} md={12}>
                        <Card>
                            <CardBody>
                                <ToolkitProvider
                                    keyField="_id"
                                    data={this.props.concepts}
                                    columns={columns}
                                    search
                                >
                                    {
                                        props => (
                                            <div className="scrollB">

                                                <SearchBar {...props.searchProps}
                                                    className="custome-search-field busqueda"
                                                    style={{
                                                        //color: '#2980B9',
                                                        backgroundColor: "#ecf0f1",
                                                    }}
                                                    placeholder="Buscar un concepto"
                                                />
                                                <hr />
                                                <BootstrapTable
                                                    striped
                                                    hover
                                                    bordered={false}
                                                    //headerClasses="header-class"
                                                    {...props.baseProps}
                                                    noDataIndication={<LoadingComponent />}
                                                    headerClasses="header-classTable"
                                                />
                                            </div>
                                        )
                                    }
                                </ToolkitProvider>
                            </CardBody>
                        </Card>

                    </Col>
                </Row>
            </Container>
        );
    }
}
TableConcepts.propTypes = {
    openAddEdit: PropTypes.func.isRequired,
    openEliminarModal: PropTypes.func.isRequired,
    concepts: PropTypes.array.isRequired,
}
export default TableConcepts;
