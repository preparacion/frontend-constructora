import React, { useEffect, useState } from 'react';
import { Col, Row, Card, CardBody, Button, ButtonToolbar, } from 'reactstrap';
import { FAMILIAS, UNIDADES } from '../../../helpers/Fichas/constants';
import { showMessageWarning, } from '../../../helpers/MessageAndNotificationUtils';
import Modal from 'react-responsive-modal';
import PropTypes from 'prop-types';
import { Select } from 'antd';
import 'rc-checkbox/assets/index.css';
import 'antd/dist/antd.css';

const { Option } = Select;

function AddEditConcepts({ openAddModal, valueAddModal,
  createConcept, editar = undefined, editConcept }) {

  //estados
  const stateInicial = {
    familia: "",
    clave: "",
    concepto: "",
    pu: "",
    unidad: "",
  }

  const [concept, setConcept] = useState(stateInicial)
  const [bloquear, setBloquear] = useState(bloquear)

  useEffect(() => {
    //console.log("editarEffect")
    //console.log(editar)
    if (editar != undefined) {

      setConcept({
        familia: editar.familia,
        clave: editar.clave.split('-')[1],
        concepto: editar.concepto,
        pu: editar.pu,
        unidad: editar.unidad,
      })
    }
  }, [])

  //INPUTS//
  const handleChangeInputs = e => {
    setConcept({
      ...concept, //recordar copiar el objeto actual para no eliminar las otras propiedades
      [e.target.name]: e.target.value
    })
  }

  //SELECTS//
  //muestra la lista de familia
  const showFamilias = () => {
    return FAMILIAS.map((value) => {
      return <Option value={value.clave} >{value.clave+"-"+value.descripcion}</Option>
    })
  }

  //maneja el evento del selector de familias
  const handleSelectFam = (value) => {
    setConcept({
      ...concept, //recordar copiar el objeto actual para no eliminar la sotras propiedades
      familia: value
    })
  }

  //muestra la lista de unidades
  const showUnidades = () => {
    return UNIDADES.map((value) => {
      return <Option value={value} >{value}</Option>
    })
  }

  //maneja el evento del selector de unidades
  const handleSelectUnidad = (value) => {
    setConcept({
      ...concept, //recordar copiar el objeto actual para no eliminar la sotras propiedades
      unidad: value
    })
  }

  //cerrar el modal
  const onCloseModal = () => {
    openAddModal(false)
    setConcept(stateInicial)//reiniciar losvalores de los estaods
    setBloquear(false)
  }

  //validar datos y enviar el body
  const hanldleSubmit = () => {
    if (concept.familia == null || concept.familia == "") {
      showMessageWarning("Atención! Debes seleccionar uan familia", 2)
    } else if (concept.clave == null || concept.clave == "") {
      showMessageWarning("Atención! Debes ingresar una clave", 2)
    } else if (concept.pu == null || concept.pu == "") {
      showMessageWarning("Atención! Debes ingresar un precio unitario", 2)
    } else if (concept.concepto == null || concept.concepto == "") {
      showMessageWarning("Atención! Debes ingresar un concepto", 2)
    } else if (concept.unidad == null || concept.unidad == "") {
      showMessageWarning("Atención! Debes seleccionar una unidad", 2)
    }
    else {
      //validamos i se editara o se creare un nuevo registro
      if (editar != undefined) {
        editConcept(concept, editar._id)
        //onCloseModal();//cerrar el modal
        setBloquear(true)
      } else {
        createConcept(concept);
        //onCloseModal();//cerrar el modal
        setBloquear(true)
      }
    }
  }

  return (
    <Modal
      open={valueAddModal}
      onClose={onCloseModal}
      center={true}
      closeOnEsc={false}
      showCloseIcon={false}
      closeOnOverlayClick={false}
    >
      <Card className="addConceptCont">
        <CardBody>

          <Row>
            <Col sm={12} md={5}>
              <h3>Crear concepto</h3>
            </Col>

            <Col sm={12} md={7}>
              <ButtonToolbar className="ml-1 float-right">
                <Button size="sm"
                  onClick={() => onCloseModal()}>
                  <p>Salir</p>
                </Button>
                <Button color="success" size="sm"
                  onClick={hanldleSubmit}
                  disabled={bloquear ? "disabled" : ""}
                >
                  <p>Guardar</p>
                </Button>
              </ButtonToolbar>
            </Col>
          </Row>

          <span className="form__form-group-label">Seleccionar familia</span>
          <Col sm={12} md={12} className="selectAMarginConcept">
            <Select
              size={"large"}
              showSearch
              style={{ width: 450 }}
              placeholder="Asignar una familia"
              onChange={handleSelectFam}
              value={concept.familia}
            >
              {
                showFamilias()
              }
            </Select>
          </Col>

          <div className="form__form-group">
            <span className="form__form-group-label">Clave</span>
            <div className="form__form-group-field">
              <input type="number"
                className="form-control"
                name="clave" autoComplete="new-password"
                value={concept.clave}
                onChange={handleChangeInputs}
              />
            </div>
          </div>

          <div className="form__form-group">
            <span className="form__form-group-label">Concepto</span>
            <div className="form__form-group-field">
              <input type="text" className="form-control" id="concepto" name="concepto"
                value={concept.concepto}
                onChange={handleChangeInputs}
              />
            </div>
          </div>

          <div className="form__form-group">
            <span className="form__form-group-label">Precio unitario</span>
            <div className="form__form-group-field">
              <input type="number" className="form-control" id="pu" name="pu"
                value={concept.pu}
                onChange={handleChangeInputs}
              />
            </div>
          </div>

          <span className="form__form-group-label">Seleccionar unidad</span>
          <Col sm={12} md={12} className="selectAMarginConcept">
            <Select
              size={"large"}
              showSearch
              style={{ width: 450 }}
              placeholder="Asignar unidad"
              onChange={handleSelectUnidad}
              name="unidad"
              value={concept.unidad}
            >
              {
                showUnidades()
              }
            </Select>
          </Col>

        </CardBody>
      </Card>
    </Modal>
  );
}
//openAddModal, valueAddModal,
// createConcept, editar = undefined, editConcept
AddEditConcepts.propTypes = {
  openAddModal: PropTypes.func.isRequired,
  valueAddModal: PropTypes.bool.isRequired,
  createConcept: PropTypes.func.isRequired,
  editConcept: PropTypes.func.isRequired,
  editar: PropTypes.object,
}
export default AddEditConcepts
