import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

const icon = `${process.env.PUBLIC_URL}/img/burger.svg`;

class SidebarButton extends PureComponent {
  static propTypes = {
    changeMobileSidebarVisibility: PropTypes.func.isRequired,
    changeSidebarVisibility: PropTypes.func.isRequired,
    sidebarCollapse:PropTypes.bool,
  };

  render() {
    const { changeMobileSidebarVisibility, changeSidebarVisibility } = this.props;

    return (
      <div>
        <button type="button" className={this.props.sidebarCollapse?"sidebar__button sidebar__button--desktop collapsed":"sidebar__button sidebar__button--desktop"} onClick={changeSidebarVisibility}>
          <img src={icon} alt="" className="sidebar__button-icon" />
          
        </button>
      </div>
    );
  }
}

export default SidebarButton;
