import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SidebarLink from './SidebarLink';
import SidebarCategory from './SidebarCategory';
import Can from "../../../helpers/Secure/Can"
class SidebarContent extends Component {
  constructor() {
    super()
    this.state = {
      showModalInscripcion: false,
    }
  }

  static propTypes = {
    onClick: PropTypes.func.isRequired,
  };

  hideSidebar = () => {
    const { onClick } = this.props;
    onClick();
  };

  show = (value) => {
    this.setState({
      showModalInscripcion: value
    })
  }

  render() {
    return (
      <div className="sidebar__content">
        <SidebarLink title="Inicio" iconMaterial="home" route="/home" onClick={this.hideSidebar} />
        <br />
        <ul className="sidebar__block">
          <SidebarCategory title="Gestión de usuarios" iconMaterial="assignment_ind">
            <Can do="get" on="/users/">
              <SidebarLink title="Empleados" route="/empleados" onClick={this.hideSidebar} />
            </Can>

            <Can do="get" on="/users/">
              <SidebarLink title="Categorías" route="/categorias" onClick={this.hideSidebar} />
            </Can>

           { /*<Can do="get" on="/roles/*">
              <SidebarLink title="Roles" route="/roles" onClick={this.hideSidebar} />
            </Can>*/}
          </SidebarCategory>

          <SidebarCategory title="Obras" iconMaterial="school">

            <Can do="get" on="/users/">
              <SidebarLink title="Conceptos" route="/conceptos" onClick={this.hideSidebar} />
            </Can>

            <Can do="get" on="/users/">
              <SidebarLink title="Contratistas" route="/contratistas" onClick={this.hideSidebar} />
            </Can>
          </SidebarCategory>



          <SidebarCategory title="Cuenta" iconMaterial="person">
           {/* <SidebarLink title="Editar contraseña" route="/account/passwordEdit/:id" onClick={this.hideSidebar} />*/}
          </SidebarCategory>

        </ul>

        <SidebarLink title="Cerrar Sesión" iconMaterial="exit_to_app" iconColor="#fa536c" route="/logout" onClick={this.hideSidebar} />

        {/* <ul className="sidebar__block">   
          <SidebarCategory title="Example Pages" icon="diamond">
            <SidebarLink title="Page one" route="/pages/one" onClick={this.hideSidebar} />
            <SidebarLink title="Page two" route="/pages/two" onClick={this.hideSidebar} />
          </SidebarCategory>
        </ul> */}

      </div>
    );
  }
}

export default SidebarContent;
