import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import TopbarSidebarButton from './TopbarSidebarButton';
import TopbarProfile from './TopbarProfile';

class Topbar extends PureComponent {
  static propTypes = {
    changeMobileSidebarVisibility: PropTypes.func.isRequired,
    changeSidebarVisibility: PropTypes.func.isRequired,
    sidebarCollapse :PropTypes.bool.isRequired,
    sidebarCollapseComplement :PropTypes.bool.isRequired,
  };

  render() {
    const { changeMobileSidebarVisibility, changeSidebarVisibility } = this.props;

    return (
      <div className="topbar">
        <div className="topbar__wrapper">
          <div className="topbar__left">
          
            <div className={this.props.sidebarCollapse?"side_bar_space collapsed":"side_bar_space"}/>


            <TopbarSidebarButton
              changeMobileSidebarVisibility={changeMobileSidebarVisibility}
              changeSidebarVisibility={changeSidebarVisibility}
              sidebarCollapse={this.props.sidebarCollapse}
            />
            <Link className="topbar__logo" to="/dashboard_default" />
          </div>
          <div className="topbar__right">
            <TopbarProfile />
            <div className={this.props.sidebarCollapseComplement?"side_bar_space_complement collapsed":"side_bar_space_complement"}/>

          </div>
        </div>
      </div>
    );
  }
}

export default Topbar;
