import React, { PureComponent } from 'react';
import DownIcon from 'mdi-react/ChevronDownIcon';
import { Collapse } from 'reactstrap';
import TopbarMenuLink from './TopbarMenuLink';

const Ava = `${process.env.PUBLIC_URL}/img/ava.png`;

export default class TopbarProfile extends PureComponent {
  constructor() {
    super();
    this.state = {
      collapse: false,
    };
  }

  toggle = () => {
    this.setState(prevState => ({ collapse: !prevState.collapse }));
  };

  render() {
    const { collapse } = this.state;
    let data = JSON.parse(localStorage.getItem('dataLoginStudent'));
    //console.log("TopbarProfile")
    //console.log(data)
    
    return (
      <div className="topbar__profile">
        <button type="button" className="topbar__avatar" onClick={this.toggle}>
          <img className="topbar__avatar-img" src={Ava} alt="avatar" />
        { <p className="topbar__avatar-name">{data.user.name+" "+data.user.lastName}</p> }
          <DownIcon className="topbar__icon" />
        </button>
        {collapse && <button type="button" className="topbar__back" onClick={this.toggle} />}
        <Collapse isOpen={collapse} className="topbar__menu-wrap">
          <div className="topbar__menu">
            {/*<TopbarMenuLink title="Cambiar contraseña" icon="user" path={`/account/passwordEdit/${data.user._id}`}  />*/}
            <div className="topbar__menu-divider" />
            <TopbarMenuLink title="Cerrar Sesión" icon="exit" path="/logout" />
          </div>
        </Collapse>
      </div>
    );
  }
}
