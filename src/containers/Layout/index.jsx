import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import classNames from 'classnames';
import Topbar from './topbar/Topbar';
import Sidebar from './sidebar/Sidebar';
import SidebarComplement from './sidebarcomplement/SIdeBarComplement';

class Layout extends Component {
  state = {
    sidebarShow: false,
    sidebarCollapse: false,
    sidebarcomplementShow: false,
    sidebarcomplementCollapse: true,
  };

componentWillUpdate(nextPropr,nextState){
  if (this.props.valueTab!==nextPropr.valueTab){
    this.setState({
      sidebarcomplementCollapse:nextPropr.valueTab
    })
  }
}

  changeSidebarVisibility = () => {
    this.setState(prevState => ({ sidebarCollapse: !prevState.sidebarCollapse }));
  };

  changeMobileSidebarVisibility = () => {
    this.setState(prevState => ({ sidebarShow: !prevState.sidebarShow }));
  };

  /* This methods are used to set the complement sideBar Component */

  // changeSidebarVisibilityComplement = () => {
  //   this.setState(prevState => ({ sidebarcomplementCollapse: !prevState.sidebarcomplementCollapse }));
  // };

  changeMobileSidebarVisibilityComplement = () => {
    this.setState(prevState => ({ sidebarcomplementShow: !prevState.sidebarcomplementShow }));
  };


  render() {
    const { sidebarShow, sidebarCollapse ,sidebarcomplementShow,sidebarcomplementCollapse} = this.state;
    const layoutClass = classNames({
      layout: true,
      'layout--collapse': sidebarCollapse,
      'layout--collapse--complement': sidebarcomplementCollapse,
    });

    return (
      <div className={layoutClass}>

      
        <Topbar
          changeMobileSidebarVisibility={this.changeMobileSidebarVisibility}
          sidebarCollapse={sidebarCollapse}
          sidebarCollapseComplement={sidebarcomplementCollapse}
          changeSidebarVisibility={this.changeSidebarVisibility}
        />


        <Sidebar
          sidebarShow={sidebarShow}
          sidebarCollapse={sidebarCollapse}
          changeMobileSidebarVisibility={this.changeMobileSidebarVisibility}
          changeSidebarVisibility={this.changeSidebarVisibility}
        />


        <SidebarComplement
          sidebarcomplementShow={sidebarcomplementShow}
          sidebarcomplementCollapse={sidebarcomplementCollapse}
          changeMobileSidebarVisibility={this.changeMobileSidebarVisibilityComplement}
          changeSidebarVisibility={this.props.closeTab}
          contentSide={this.props.content}
        />


      </div>
    );
  }
}

export default withRouter(Layout);
