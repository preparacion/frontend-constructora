import React from 'react';
import Scrollbar from 'react-smooth-scrollbar';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import SidebarButton from './SidebarButtonComplement';
//import SidebarContent from './SidebarContent';


const SidebarComplement = ({
  changeMobileSidebarVisibility, sidebarcomplementShow, sidebarcomplementCollapse,changeSidebarVisibility,contentSide,
}) => {
  const sidebarClass = classNames({
    sidebarComplement: true,
    'sidebarComplement--show': !sidebarcomplementCollapse,
    'sidebarComplement--collapse': sidebarcomplementCollapse,
  });

  return (
    <div className={sidebarClass}>
      <button type="button" className="sidebarComplement__back" onClick={changeSidebarVisibility} />
      <Scrollbar className="sidebarComplement__scroll scroll" damping={0.8} continuousScrolling={false}>
        <div className="sidebarComplement__wrapper sidebarComplement__wrapper--desktop">
          <SidebarButton
           changeMobileSidebarVisibility={changeMobileSidebarVisibility}
           changeSidebarVisibility={changeSidebarVisibility}
           sidebarCollapse={sidebarcomplementShow}
           />
           <div className="sidebarComplement_container">
           {contentSide}
           </div>
        </div>
      </Scrollbar>
    </div>
  );
};

SidebarComplement.propTypes = {
  sidebarcomplementShow: PropTypes.bool.isRequired,
  sidebarcomplementCollapse: PropTypes.bool.isRequired,
  changeMobileSidebarVisibility: PropTypes.func.isRequired,
};

export default SidebarComplement;
