import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from 'mdi-react/CloseIcon';

const icon = `${process.env.PUBLIC_URL}/img/cross.svg`;

class SidebarButtonComplement extends PureComponent {
  static propTypes = {
    changeMobileSidebarVisibility: PropTypes.func.isRequired,
    changeSidebarVisibility: PropTypes.func.isRequired,
    sidebarCollapse: PropTypes.bool,
  };

  render() {
    const { changeMobileSidebarVisibility, changeSidebarVisibility } = this.props;

    return (
      <div>
        <button type="button" className={this.props.sidebarCollapse ? "sidebarComplement__button sidebarComplement__button--desktop collapsed" : "sidebarComplement__button sidebarComplement__button--desktop"} onClick={changeSidebarVisibility}>
          <img src={icon} alt="" className="sidebarComplement__button-icon" />
         
        </button>
      </div>
    );
  }
}

export default SidebarButtonComplement;
