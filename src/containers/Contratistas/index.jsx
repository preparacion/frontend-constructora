import React, { Component, PureComponent } from 'react';
import { Col, Container, Row, Button, ButtonToolbar } from 'reactstrap';
import { fetchAsync, urlCategories, urlObras, urlContratistas } from '../../helpers/Globals/globals';
//import GruposTabs from './components/tabs';
//import Ficha from './components/ficha';
//import AddGrupo from './components/addGrupo/index';
import AddObra from './Components/addObra';
import AddContratista from './Components/addContratista';
import FormContratista from './Components/addEditContratista/FormContratista';
import CollapseObras from './Components/collapseObras';
import FichaContratista from './Components/Ficha';
import LoadingComponent from "../../helpers/LoadingComponent";
import AddIcon from 'mdi-react/AddIcon';
import Can from '../../helpers/Secure/Can';
import EmptyComponent from "../../helpers/Empty/EmptyComponent";

class Contratista extends PureComponent {

  constructor(props) {
    super(props)
    this.state = {
      addObra: false,
      addContratista: false,
      allObras: [],
      allContratistas: [],
      categories: [],
    }
  }

  componentDidMount() {
    this.getAllObras()
    this.getAllContratistasObras()
    this.getCategories()
  }

  //setea la informacion del grupo seleccionado en la tabla
  getActualContratista = (datos, idObra) => {
    //console.log(datos, idObra)
    this.props.openSide(
      <FichaContratista
        dataContratista={datos}
        getActualContratista={this.getActualContratista}
        getAllContratistasObras={this.getAllContratistasObras}
        eliminarContratista={this.eliminarContratista}
        idObra={idObra}
        editarContratista={this.editarContratista}
        categories={this.state.categories}
      />
    )
  }

  //fetch para obtener las categorias
  getCategories = () => {

    return fetchAsync(urlCategories, "", "GET", "")
      .then(
        (result) => {
          this.setState({
            categories: result
          });
        })
      .catch((reason) => {
        console.log(reason.message)
      });
  }

  openModalAddObra = (valor) => {
    this.setState({
      addObra: valor,
    })
  }

  openAddContratista = (valor) => {
    this.setState({
      addContratista: valor,
    })
  }

  getAllObras = () => {

    return fetchAsync(urlObras, "", "GET", "")
      .then(
        (result) => {
          this.setState({
            allObras: result
          })
        })
      .catch(
        (reason) => {
          console.log(reason.message)
        });
  }


  getAllContratistasObras = () => {

    return fetchAsync(urlContratistas + "contratistasConObras", "", "GET", "")
      .then(
        (result) => {
          this.setState({
            allContratistas: result
          })
        })
      .catch(
        (reason) => {
          console.log(reason.message)
        });
  }

  //funcion para eliminar un contratista
  eliminarContratista = (id) => {
    fetchAsync(urlContratistas + id, "", "DELETE", "")
      .then(
        (data) => {
          this.getAllContratistasObras()
        })
      .catch(
        (reason) => {
          console.log(reason.message)
          //store.showModalDelete
        }
      );
  }

  //Agregar contratista
  agregarContratista = (body) => {
    fetchAsync(urlContratistas, JSON.stringify(body), "POST", "")
      .then(
        (data) => {
          this.openAddContratista(false)
        }).catch(
          (reason) => {
            console.log(reason)
          });
  }

  //Editar contratista
  editarContratista = (body, idContratista) => {
    fetchAsync(urlContratistas + idContratista, JSON.stringify(body), "PUT", "")
      .then(
        (data) => {
          this.getAllContratistasObras()
          this.getActualContratista(data)
        }).catch(
          (reason) => {
            console.log(reason)
          });
  }


  render() {

    return (
      <Container>
        <Row>
          <Col xs={12} sm={12} md={3} className="mt-2 titlesComponents">
            <div className="typography--inline">
              <p className="subhead">Obras |</p>
              <p style={{ color: "#3057af", marginLeft: "5px" }}>Contratistas</p>
            </div>
          </Col>
          <Can do="post" on="/groups/">
            <Col xs={12} sm={12} md={9} className="mt-2 float-right">
              <ButtonToolbar className="float-right">
                <Button className="btn icon" size="sm" color="success"
                  style={{ marginRight: "15px" }}
                  onClick={() => this.openAddContratista(true)}
                >
                  <p><AddIcon />Crear Contratista</p>
                </Button>
                <Button className="btn icon" size="sm" color="success"
                  style={{ marginRight: "15px" }}
                  onClick={() => this.openModalAddObra(true)}
                >
                  <p><AddIcon />Crear obra</p>
                </Button>
              </ButtonToolbar>
            </Col>
          </Can>

        </Row>
        <Row className="mt-3">
          <Col xs={12} sm={12} md={12}>
            {this.state.allObras.length > 0 ?
              this.state.allObras.map((value) => {
                return (
                  <CollapseObras
                    key={value._id}
                    nombre={value.name}
                    address={value.address}
                    id={value._id}
                    dataContratistas={this.state.allContratistas}
                    getAllContratistasObras={this.getAllContratistasObras}
                    getActualContratista={this.getActualContratista}
                    getAllObras={this.getAllObras}
                  />
                )
              }) : <LoadingComponent />
            }

          </Col>
        </Row>

        <AddContratista
        //valueAddModal={this.state.addContratista}
        //close={this.openAddContratista}
        />

        {(this.state.addContratista) ? <FormContratista
          titulo="Agregar"
          valueAddModal={this.state.addContratista}
          openAddModal={this.openAddContratista}
          agregarContratista={this.agregarContratista}
          categories={this.state.categories}
        /> : null}

        <AddObra
          valueAddModal={this.state.addObra}
          close={this.openModalAddObra}
          getAllObras={this.getAllObras}

        />
      </Container>
    )
  }

}
export default Contratista