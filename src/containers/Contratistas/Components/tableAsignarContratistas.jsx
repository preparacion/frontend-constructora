import React, { PureComponent } from 'react';
import { Col, Container, Row, Card, CardBody } from 'reactstrap';
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import SortTable from '../../../helpers/Tables/sortTable';
import LoadingComponent from "../../../helpers/LoadingComponent";
import { COLOR_SORT_TABLES } from '../../../helpers/Tables/constants';
//import paginationFactory from 'react-bootstrap-table2-paginator';

import paginationFactory, { PaginationProvider, PaginationListStandalone, SizePerPageDropdownStandalone } from 'react-bootstrap-table2-paginator';

class TableAsignarContratista extends PureComponent {

  showName = (col, row) => {
    if (row.secondName !== undefined || row.secondName !== "") {
      return row.name + " " + row.secondName
    } else {
      return row.name
    }
  }

  render() {
    console.log("TableAsignarContratistaDatos")
    console.log(this.props.dataTable)
    console.log("TableAsignarContratistaDatos")

    const headerSortingStyle = { backgroundColor: COLOR_SORT_TABLES };
    const { SearchBar } = Search;
    const columns = [

      {
        dataField: 'name',
        text: 'Nombre',
        sort: true,
        headerAlign: 'center',
        align: 'center',
        sortCaret: (order, column) => {
          return <SortTable order={order} colum={column} />
        },
        headerSortingStyle,
        formatter: this.showName
      },
      {
        dataField: 'aPaterno',
        text: 'Apellido P',
        sort: true,
        headerAlign: 'center',
        align: 'center',
        sortCaret: (order, column) => {
          return <SortTable order={order} colum={column} />
        },
        headerSortingStyle,
      },

      {
        dataField: 'aMaterno',
        text: 'Apellido M',
        sort: true,
        headerAlign: 'center',
        align: 'center',
        headerSortingStyle,
      },
      {
        dataField: 'rfc',
        text: 'RFC',
        sort: true,
        headerAlign: 'center',
        align: 'center',
        headerSortingStyle,
      },
      {
        dataField: 'noSocial',
        text: 'No. Seguro',
        sort: true,
        headerAlign: 'center',
        align: 'center',
        headerSortingStyle,
      },
      {
        dataField: 'type',
        text: 'Tipo',
        sort: true,
        headerAlign: 'center',
        align: 'center',
        headerSortingStyle,
      },

    ];

    const rowEvents = {
      onClick: (e, row, rowIndex) => {
        //console.log(`clicked on row with index: ${rowIndex}`);
        //console.log(row);
        this.props.getInfoContratista(row)
      },
    };


    const options = {
      custom: true,
      //paginationSize: 4,
      //pageStartIndex: 1,
      paginationSize: 4,
      pageStartIndex: 0,
      firstPageText: 'Primera',
      prePageText: 'Anterior',
      nextPageText: 'Siguiente',
      lastPageText: 'Ultima',
      nextPageTitle: 'First page',
      prePageTitle: 'Pre page',
      firstPageTitle: 'Next page',
      lastPageTitle: 'Last page',
      showTotal: true,
      //totalSize: this.props.empleados.length
      sizePerPageList: [{
        text: '15', value: 15
      },
      {
        text: '30', value: 30
      },
      {
        text: '50', value: 50
      },
      {
        text: 'Todos', value: this.props.dataTable.length
      }]
    };

    const selectRow = {
      mode: 'radio',
      clickToSelect: true,
      bgColor: "#D6DBDF"
    };


    const contentTable = ({ paginationProps, paginationTableProps }) => (
      <Col sm={12} md={12}>
        <Row>

          <Col xs={12} sm={12} md={6} >
            <div className="float-left">
              <SizePerPageDropdownStandalone
                {...paginationProps}
              />
            </div>

          </Col>
          <Col xs={12} sm={12} md={6} >
            <div className="float-right">
              <PaginationListStandalone {...paginationProps} />
            </div>

          </Col>

        </Row>


        <ToolkitProvider
          keyField="_id"
          columns={columns}
          data={this.props.dataTable}
          search
        >
          {
            toolkitprops => (
              <div className="scrollB">

                <Row>
                  <Col xs={12} sm={12} md={12} className="mt-2 mb-4">
                    <SearchBar {...toolkitprops.searchProps}
                      className="custome-search-field busqueda"
                      style={{
                        //color: '#2980B9',
                        backgroundColor: "#ecf0f1",
                        //width: "450px"
                      }}
                      placeholder="Buscar un contratista"
                      autoComplete="off"
                    />
                  </Col>
                </Row>

                <BootstrapTable
                  keyField="_id"
                  //striped
                  //hover
                  bordered={false}
                  rowEvents={rowEvents}
                  selectRow={selectRow}
                  {...toolkitprops.baseProps}
                  {...paginationTableProps}
                  noDataIndication={<LoadingComponent />}
                  headerClasses="header-classTable"
                />
              </div>
            )
          }
        </ToolkitProvider>
      </Col>
    );

    return (
      <Container >
        <Row>
          <Col sm={12} md={12}>
            <Card>

              <Col xs={12} sm={12} md={12}>

                <PaginationProvider
                  pagination={
                    paginationFactory(options)
                  }
                >
                  {contentTable}
                </PaginationProvider>

              </Col>


            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}
export default TableAsignarContratista;
