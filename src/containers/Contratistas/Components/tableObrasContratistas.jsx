import React, { PureComponent, Component } from 'react';
import { CardBody, Card, Button } from 'reactstrap';
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import SetFechaTable from './../../../helpers/Tables/fechaTables';
import CircleIcon from 'mdi-react/CircleIcon';
import SortTable from '../../../helpers/Tables/sortTable';
import LoadingComponent from "../../../helpers/LoadingComponent";
import AsignarContratistas from './asignarContratista';
import EditObra from './editObra';
import { Link } from "react-router-dom";

class TableObrasContratistas extends Component {

  constructor(props) {
    super(props)
    this.state = {
      openModal: false,
      editObra: false,
    }
  }

  openModal = (valor) => {
    this.setState({
      openModal: valor,
    })
  }

  openEditObra = (valor) => {
    this.setState({
      editObra: valor,
    })
  }

  showName = (col, row) => {
    if (row.secondName !== undefined || row.secondName !== "") {
      return row.name + " " + row.secondName
    } else {
      return row.name
    }
  }

  rowEvents = {
    onClick: (e, row, rowIndex) => {
      //console.log(`clicked on  TableSede row with index: ${rowIndex}`);
      //console.log(row);
      this.props.getActualContratista(row, this.props.idObra)
    }
  };


  fecha = (cell, row) => {
    return SetFechaTable(cell)
  }

  createName = (cell, row) => {
    return row.nombre + " " + row.apellidoP + " " + row.apellidoM
  }

  render() {

    //console.log("+++++++")
    //console.log(this.props.dataTable)

    //const headerSortingStyle = { backgroundColor: '#D6DBDF' };
    //const { SearchBar } = Search;
    const columns = [
      {
        dataField: 'name',
        text: 'Nombre',
        sort: true,
        sortCaret: (order, column) => {
          return <SortTable order={order} colum={column} />
        },
        headerAlign: 'center',
        align: 'center',
        formatter: this.showName,
      },
      {
        dataField: 'groups',
        text: 'No Empleados',
        sort: true,
        sortCaret: (order, column) => {
          return <SortTable order={order} colum={column} />
        },
        headerAlign: 'center',
        align: 'center',
      },
      {
        dataField: 'price',
        text: 'Acumulado anterior',
        sort: true,
        sortCaret: (order, column) => {
          return <SortTable order={order} colum={column} />
        },
        headerAlign: 'center',
        align: 'center',
      },
      {
        dataField: 'creationDate',
        text: 'Acumulado actual',
        sort: true,
        sortCaret: (order, column) => {
          return <SortTable order={order} colum={column} />
        },
        headerAlign: 'center',
        align: 'center',
        //formatter: this.fecha,
      },
      {
        dataField: 'active',
        text: 'Diferencia',
        sort: true,
        sortCaret: (order, column) => {
          return <SortTable order={order} colum={column} />
        },
        headerAlign: 'center',
        align: 'center',
        formatter: this.disponible,
      },
    ];

    const defaultSorted = [{
      dataField: 'creationDate',
      order: 'desc'
    }];

    return (

      <Card>
        <CardBody>
          <ToolkitProvider
            keyField="_id"
            data={this.props.dataTable}
            columns={columns}
            search
          >
            {
              props => (
                <div className="scrollB">

                  <div className="mb-5">
                    <Button className="btn float-right icon" size="sm" color="pdf"
                      style={{ marginRight: "15px" }}
                      href={"http://heg.bluedotmx.com/api/listaDePersonal/" + this.props.idObra}
                    >
                      <p>Descargar PDF</p>
                    </Button>
                    <Button className="btn float-right icon" size="sm" color="success"
                      style={{ marginRight: "15px" }}
                      onClick={() => this.openModal(true)}
                    >
                      <p>Asignar Contratista</p>
                    </Button>

                    <Button className="btn float-right icon" size="sm" color="success"
                      style={{ marginRight: "15px" }}
                      onClick={() => this.openEditObra(true)}
                    >
                      <p>Editar obra</p>
                    </Button>

                    <Link to={{ pathname: "/catalogos-obras/" + this.props.idObra}} className="col-xs-1">
                      <Button className="btn float-right icon mr-3" type="button" color="success" size="sm">
                        <p >Estimaciones</p>
                      </Button>
                    </Link>

                  </div>

                  <BootstrapTable
                    striped
                    hover
                    defaultSorted={defaultSorted}
                    bordered={false}
                    rowEvents={this.rowEvents}
                    //headerClasses="header-class"
                    {...props.baseProps}
                    noDataIndication={<LoadingComponent />}
                    headerClasses="header-classTable"
                  />
                </div>
              )
            }
          </ToolkitProvider>
        </CardBody>

        <AsignarContratistas
          valueModal={this.state.openModal}
          close={this.openModal}
          idObra={this.props.idObra}
          getAllContratistasObras={this.props.getAllContratistasObras}

        />

        <EditObra
          valueModal={this.state.editObra}
          close={this.openEditObra}
          address={this.props.address}
          nombreObra={this.props.nombreObra}
          idObra={this.props.idObra}
          getAllObras={this.props.getAllObras}
        />
      </Card>


    );
  }
}
export default TableObrasContratistas;
