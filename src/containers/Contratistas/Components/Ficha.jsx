import React, { PureComponent } from 'react';
import { Col, Row, Button, ButtonToolbar, Card, ButtonGroup, Container } from 'reactstrap';
import EditContratista from './editContratista';
import ListaPersonal from './listaPersonal/index';
import UploadFileEmployee from '../../uploadFiles/index';
import { Link } from "react-router-dom";
import { showDate } from '../../../helpers/Globals/funciones';
import Can from '../../../helpers/Secure/Can';
import listDocuments from '../../../helpers/Fichas/showDocuments';
import AvatarName from '../../../helpers/Fichas/AvatarName';
import FormContratista from '../Components/addEditContratista/FormContratista';

class FichaContratista extends PureComponent {

    constructor(props) {
        console.log("0) constructor")
        super(props)
        this.state = {
            editContratistaModal: false,
            modalListaPersonal: false,
            grupos: [],
            modalComent: false,
            cotalogModal: false,
            uploadFile: false,

            estimaciones: false,
        }
    }

    openModalEdit = (value) => {
        this.setState({
            editContratistaModal: value,
        })
    }

    openModalComent = (value) => {
        this.setState({
            modalComent: value
        })
    }

    openModalListaP = (value) => {
        this.setState({
            modalListaPersonal: value
        })
    }

    openModalUploadFile = (value) => {
        this.setState({
            uploadFile: value
        })
    }

    listObras = () => {
        return this.props.dataContratista.obras.map((data, index) => {
            return (
                <div key={index}>
                    <hu>
                        <li>
                            {data.name}
                        </li>
                    </hu>
                </div>
            )
        })
    }

    render() {
        console.log("RenderFichaContratista")
        console.log(this.props.dataContratista)
        console.log("RenderFichaContratista")
        let { name, secondName, aPaterno,
            aMaterno, rfc, createdAt, files, type ,noSocial} = this.props.dataContratista

        return (
            <Container >

                <Card className="fichaContratistaSize">

                    <Can do="put" on="/users/">
                        <Row>
                            <Col sm={12} className="marginConButtonUpload">
                                <ButtonGroup>

                                    <Button className="icon  btn-ficha" type="button" color="success" outline
                                        onClick={() => this.openModalUploadFile(true)}
                                    >
                                        <div className="textButonFicha" >
                                            <p ><i className="material-icons">cloud_upload</i>Subir archivos</p>
                                        </div>
                                    </Button>

                                    <Button className="icon  btn-ficha" type="button" color="success" outline
                                        onClick={() => this.openModalEdit(true)}
                                    >
                                        <div className="textButonFicha" >
                                            <p ><i className="material-icons">edit</i>Editar</p>
                                        </div>
                                    </Button>


                                </ButtonGroup>
                            </Col>

                        </Row>
                    </Can>

                    <Row className={"no-gutters"}>

                        <Col className="ficha_name_container my-3">
                            <AvatarName
                                name={name}
                                secondName={secondName}
                                aMaterno={aPaterno}
                                aPaterno={aMaterno}
                            />
                        </Col>
                    </Row>
                    <Can do="put" on="/users/">
                        <Row>
                            <Col xs={5}>
                                <Button className=" square mr-3" type="button" color="success" outline
                                    onClick={() => this.openModalListaP(true)}
                                >
                                    <p >Personal</p>
                                </Button>
                            </Col>
                            <Col xs={7}>
                                <ButtonToolbar className="fichaMarginR float-left">
                                    <ButtonGroup>
                                        <Link to={{ pathname: "/catalogos/" + this.props.idObra + "/" + this.props.dataContratista._id }} className="col-xs-1">
                                            <Button className=" square  float-left" type="button" color="success" outline>
                                                <p >Estimaciones</p>
                                            </Button>
                                        </Link>
                                    </ButtonGroup>
                                </ButtonToolbar>
                            </Col>
                        </Row>
                    </Can>
                    <Row>
                        <Col xs={12} sm={12} md={12} className="ficha_name_container">

                            <div className="mt-2 textTitle">
                                <p className="subTitulosFicha textTitle">Fecha alta</p>
                            </div>
                            <p className="fichaData">{showDate(createdAt)}</p>

                            <div className="mt-2 textTitle">
                                <p className="subTitulosFicha ">RFC</p>
                            </div>
                            <p className="fichaData">{rfc}</p>

                            <div className="mt-2 textTitle">
                                <p className="subTitulosFicha ">IMSS</p>
                            </div>
                            <p className="fichaData">{noSocial}</p>

                            <div className="mt-2 textTitle">
                                <p className="subTitulosFicha ">Tipo</p>
                            </div>
                            <p className="fichaData">{type}</p>
                        </Col>
                    </Row>

                    <Row>
                        <Col className="ml-2">
                            <div className="textTitle">
                                <p className="subTitulosFicha ">Obras asignadas</p>
                            </div>

                            {this.listObras()}
                        </Col>
                    </Row>

                    <Row>
                        <Col className="my-3">
                            <h4>Documentos</h4>
                            {listDocuments(files)}
                        </Col>
                    </Row>



                </Card>

                <EditContratista
                    //valueModal={this.state.editContratistaModal}
                    //close={this.openModalEdit}
                    contratistaData={this.props.dataContratista}
                    getActualContratista={this.props.getActualContratista}
                    getAllContratistasObras={this.props.getAllContratistasObras}
                    eliminarContratista={this.props.eliminarContratista}
                //deleteEmploye={this.props.deleteEmployIndex}
                />

                {(this.state.editContratistaModal) ? <FormContratista
                    titulo="Editar"
                    valueAddModal={this.state.editContratistaModal}
                    openAddModal={this.openModalEdit}
                    editarInfo={this.props.dataContratista}
                    editContratista={this.props.editarContratista}
                    borrarContratista={this.props.eliminarContratista}
                    categories={this.props.categories}
                //idObra={this.props.idObra}
                /> : null}

                <ListaPersonal
                    valueModal={this.state.modalListaPersonal}
                    close={this.openModalListaP}
                    contratistaData={this.props.dataContratista}
                    idObra={this.props.idObra}
                />

                {(this.state.uploadFile) ? <UploadFileEmployee
                    valueModal={this.state.uploadFile}
                    close={this.openModalUploadFile}
                    idContratista={this.props.dataContratista._id}
                    getActualContratista={this.props.getActualContratista}
                    isContratist={true}
                /> : null}

            </Container>
        )
    }

}
export default FichaContratista