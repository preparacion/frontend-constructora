import React, { Component } from 'react';
import { Col, Row, Card, CardBody, Button, ButtonToolbar, } from 'reactstrap';
import { fetchAsync, urlObras, fetchAsyncConst } from '../../../helpers/Globals/globals';
import {
  showMessageLoading, showMessageWarning,
} from '../../../helpers/MessageAndNotificationUtils';
import Modal from 'react-responsive-modal';
import 'rc-checkbox/assets/index.css';
import 'antd/lib/date-picker/style/index.css';

let closeLoading = ""

class AddObra extends Component {

  constructor(props) {
    super(props);
    this.state = {

      name: "",
      adress: "",
      bloquear: false,
    };

  }

  onCloseModal = () => {
    if (closeLoading != "") {
      closeLoading()
    }

    this.props.close(false);

    this.setState({
      name: "",
      adress: "",
      bloquear: false,
    })
  };

  //crea uan funcion para lamzar el mensaje de loading
  LoadingMessage = () => {
    return showMessageLoading('Registrando..', 0);
  };


  //valida y genera las advertencias necesarias para el formulario
  registerEmployee = () => {

    if (this.state.name == null || this.state.name == "") {
      showMessageWarning("Atención! Debes ingresar un nombre", 2)

    }  else {

      closeLoading = this.LoadingMessage()
      this.setState({ bloquear: true })//bloquea el boton editar

      // eslint-disable-next-line no-new-object
      let body = new Object()


      body = {

        name: this.state.name,
        address: this.state.adress

      }

      //console.log(body)


      fetchAsync(urlObras, JSON.stringify(body), "POST", "")
        .then(
          (data) => {
            closeLoading();
            this.props.getAllObras() //callback a index para recargar la tabla
            this.onCloseModal()

            /*if (data.success == true) {
                //alert('Usuario registrado con éxito!')
                this.props.getUsers()//callback a index para recargar la tabla
                this.onCloseModal()

            } else {
                //alert(data.error)
                //showMessageError(<strong>{data.error}</strong>, 2)
                //this.setState({ error: data.error[0].message, alerta: "error" })

            }*/
          }).then(() => this.setState({ bloquear: false }))//desbloquea el boton editar)
        .catch(
          (reason) => {
            console.log(reason)
          });
    }

  }

  render() {
    //console.log("OpenMOdalEmpleados")
    return (

      <Modal
        open={this.props.valueAddModal}
        onClose={this.onCloseModal}
        center={true}
        closeOnEsc={false}
        showCloseIcon={false}
        closeOnOverlayClick={false}
      >
        <Card className="addObra">
          <CardBody>

            <Row>

              <Col sm={12} md={5}>
                <h3>Crear obra</h3>
              </Col>

              <Col sm={12} md={7}>
                <ButtonToolbar className="ml-1 float-right">
                  <Button size="sm"
                    onClick={() => this.onCloseModal()}>
                    <p>Salir</p>
                  </Button>
                  <Button color="success" size="sm"
                    onClick={() => this.registerEmployee()}
                    disabled={this.state.bloquear === true ? "disabled" : ""}
                  >
                    <p>Guardar</p>
                  </Button>
                </ButtonToolbar>
              </Col>
            </Row>

            <div className="form__form-group">
              <span className="form__form-group-label">Nombre</span>
              <div className="form__form-group-field">

                <input type="text"
                  className="form-control"
                  id="inpuName" autoComplete="new-password"
                  defaultValue={this.state.name}
                  onChange={(event) => {
                    this.setState({ name: event.target.value })
                    //this.state.name = event.target.value
                  }} />
              </div>
            </div>

            <div className="form__form-group">
              <span className="form__form-group-label">Dirección</span>
              <div className="form__form-group-field">

                <input type="text" className="form-control" id="inpuAdress" name="adress"
                  defaultValue={this.state.adress}
                  onChange={(event) => {
                    this.setState({ adress: event.target.value })
                  }} />
              </div>
            </div>


          </CardBody>
        </Card>
      </Modal>

    );
  }
}
export default AddObra;


