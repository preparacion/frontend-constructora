//import React from 'react';
import React, { PureComponent, Component } from 'react';
import { Col, Container, Row, Card, CardBody, Button } from 'reactstrap';
import Collapse from './Collapse';
import Can from '../../../helpers/Secure/Can';
import TableObrasContratistas from './tableObrasContratistas';
//import ModalContratistas from './modalContratista';

class CollapseObras extends PureComponent {


  //metodo que obtiene los grupos del curso
  getContratistaByObra = () => {
    //let gruposByCurso = this.props.dataGrupos.filter((dato) => {console.log(dato._id)})
    if (this.props.dataContratistas !== undefined) {

      let contratistaByCurso = this.props.dataContratistas.filter((dato) => {

        if (dato.obras.length > 0) {
          for (let a = 0; a < dato.obras.length; a++) {
            if (dato.obras[a]._id == this.props.id) {
              return dato
            }
          }
        }
      })
      return contratistaByCurso
    } else {
      let dummy = []
      return dummy
    }
  }

  muestra = () => {


    //descometar si se quieren mostrar todos los cursos aun sino tiene nungun grupo creado
    return (
      <Collapse title={this.props.nombre} className="with-shadow">

        <TableObrasContratistas
          address={this.props.address}
          nombreObra={this.props.nombre}
          dataTable={this.getContratistaByObra()}
          idObra={this.props.id}
          getAllContratistasObras={this.props.getAllContratistasObras}
          getActualContratista={this.props.getActualContratista}
          getAllObras={this.props.getAllObras}
        />
      </Collapse>
    )
  }

  render() {

    return (

      <Col md={12} lg={12} >

        {
          this.muestra()
        }

      </Col>

    )
  }

}

export default CollapseObras;
