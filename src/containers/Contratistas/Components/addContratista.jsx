import React, { Component } from 'react';
import { Col, Row, Card, CardBody, Button, ButtonToolbar, } from 'reactstrap';
import { fetchAsync, urlContratistas, fetchAsyncConst } from '../../../helpers/Globals/globals';
import {
  showMessageLoading, showMessageWarning,
} from '../../../helpers/MessageAndNotificationUtils';
import Modal from 'react-responsive-modal';
import { Select } from 'antd';
import 'antd/lib/date-picker/style/index.css';

const selectOption = ["tipo1", "tipo2", "tipo3", "tipo4"]

let closeLoading = ""
const { Option } = Select;

class AddContratista extends Component {

  constructor(props) {
    super(props);
    this.state = {

      name: "",
      lastName: "",
      secondLastName: "",
      curp: "",
      rfc: "",
      bloquear: false,

    };

  }

  onCloseModal = () => {
    if (closeLoading != "") {
      closeLoading()
    }

    this.props.close(false);

    this.setState({
      name: "",
      lastName: "",
      secondLastName: "",
      //curp: "",
      rfc: "",
      bloquear: false,
    })
  };

  //crea uan funcion para lamzar el mensaje de loading
  LoadingMessage = () => {
    return showMessageLoading('Registrando..', 0);
  };

  //valida y genera las advertencias necesarias para el formulario
  registerContratista = () => {

    let letterNumber = /[0-9]/g;
    let ifNum = this.state.name + this.state.lastName + this.state.secondLastName

    if (this.state.name == null || this.state.name == "") {
      showMessageWarning("Atención! Debes ingresar un nombre", 2)

    } else if (this.state.lastName == null || this.state.lastName == "" ||
      this.state.secondLastName == null || this.state.secondLastName == "") {
      showMessageWarning("Atención! Debes ingresar un apellido paterno y materno", 2)

    } else if (ifNum.match(letterNumber)) {
      showMessageWarning("Atención! Debes escribir solo letras en nombre y apellidos", 2)

    } else if (this.state.rfc == null || this.state.rfc == "") {
      showMessageWarning("Atención! Debes ingresar el RFC", 2)

    } else {
      closeLoading = this.LoadingMessage()
      this.setState({ bloquear: true })//bloquea el boton editar

      // eslint-disable-next-line no-new-object
      let body = new Object()


      body = {

        nombre: this.state.name,
        apellidoP: this.state.lastName,
        apellidoM: this.state.secondLastName,
        rfc: this.state.rfc,
        //curp: this.state.curp,

      }

      console.log(body)


      fetchAsync(urlContratistas, JSON.stringify(body), "POST", "")
        .then(
          (data) => {
            closeLoading();
            //this.props.getUsers()//callback a index para recargar la tabla
            this.onCloseModal()

            /*if (data.success == true) {
                //alert('Usuario registrado con éxito!')
                this.props.getUsers()//callback a index para recargar la tabla
                this.onCloseModal()

            } else {
                //alert(data.error)
                //showMessageError(<strong>{data.error}</strong>, 2)
                //this.setState({ error: data.error[0].message, alerta: "error" })

            }*/
          }).then(() => this.setState({ bloquear: false }))//desbloquea el boton editar)
        .catch(
          (reason) => {
            console.log(reason)
          });
    }

  }

  //muestra la lista de Obras
  showTipos = () => {

    return selectOption.map((valor, index) => {
      return <Option value={valor}>{valor}</Option>

    })
  }

  render() {
    //console.log("OpenMOdalEmpleados")
    return (


      <Modal
        open={this.props.valueAddModal}
        onClose={this.onCloseModal}
        center={true}
        closeOnEsc={false}
        showCloseIcon={false}
        closeOnOverlayClick={false}
      >
        <Card className="addContratistaCont">
          <CardBody>

            <Row>

              <Col sm={12} md={6}>
                <h3>Crear contratista</h3>
              </Col>

              <Col sm={12} md={6}>
                <ButtonToolbar className="ml-1 float-right">
                  <Button size="sm"
                    onClick={() => this.onCloseModal()}>
                    <p>Salir</p>
                  </Button>
                  <Button color="success" size="sm"
                    onClick={this.registerContratista}
                    disabled={this.state.bloquear === true ? "disabled" : ""}
                  >
                    <p>Crear</p>
                  </Button>
                </ButtonToolbar>
              </Col>
            </Row>

            <div className="form__form-group">
              <span className="form__form-group-label">Nombre</span>
              <div className="form__form-group-field">

                <input type="text"
                  className="form-control"
                  id="inpuName" autoComplete="new-password"
                  defaultValue={this.state.name}
                  onChange={(event) => {
                    this.setState({ name: event.target.value })
                    //this.state.name = event.target.value
                  }} />
              </div>
            </div>

            <div className="form__form-group">
              <span className="form__form-group-label">Apellido paterno</span>
              <div className="form__form-group-field">

                <input type="text"
                  className="form-control"
                  id="inputLastname" autoComplete="new-password"
                  defaultValue={this.state.lastName}
                  onChange={(event) => {
                    this.setState({ lastName: event.target.value })
                    //this.state.lastName = event.target.value
                  }} />
              </div>
            </div>

            <div className="form__form-group">
              <span className="form__form-group-label">Apellido materno</span>
              <div className="form__form-group-field">

                <input type="text"
                  className="form-control"
                  id="inputSecondLastName" autoComplete="new-password"
                  defaultValue={this.state.secondLastName}
                  onChange={(event) => {
                    this.setState({ secondLastName: event.target.value })
                    //this.state.secondLastName = event.target.value
                  }} />
              </div>
            </div>

            <div className="form__form-group">
              <span className="form__form-group-label">RFC</span>
              <div className="form__form-group-field">

                <input className="form-control" type="text"

                  id="inputPassword"
                  defaultValue={this.state.rfc}
                  onChange={(event) => {
                    this.setState({ rfc: event.target.value })

                  }} />
              </div>

            </div>

            <span className="form__form-group-label">Asignar tipo</span>
            <Col sm={12} md={12} className="selectAMargin">
              <Select
                showSearch
                style={{ width: 220 }}
                placeholder="Seleciona un tipo"
              //onChange={this.handleChangeObras}
              >
                {
                  this.showTipos()
                }

              </Select>
            </Col>

          </CardBody>
        </Card>
      </Modal>

    );
  }
}
export default AddContratista;


