import React, { PureComponent } from 'react';
import { Col, Row, Card, CardBody } from 'reactstrap';

class CardCreator extends PureComponent {

    createTable = (elements) => {
        let table = []
        console.log(elements.length)
        for (let i = 0; i < elements.length; i++) {
            table.push(
                <Col xs={12} sm={12}  onClick={()=>this.handleClick(elements[i])}>
                    <Card>
                        <CardBody className="dashboard__card-widget-contratista">
                            <div className="card__title">
                                <h5 className="bold-text">{elements[i].name}</h5>
                            </div>
                            <div className="dashboard__total dashboard__total--area">
                                <p>{elements[i].description}</p>
                            </div>
                        </CardBody>
                    </Card>
                </Col>
            )
        }
        return table
    }

    handleClick(value){
        this.props.ClickOnElement(value)
    }

    render() {
        return (
            <Row>
                {this.createTable(this.props.data)}
            </Row>
        )
    }
}

export default CardCreator;