import React from 'react';
import PropTypes from 'prop-types';
import { Col, Row, Button, ButtonToolbar, } from 'reactstrap';

export const Header = ({ titulo, onCloseModal, hanldleSubmit, bloquear }) => (
  <Row>

    <Col xs="12" sm="12" md="6">
      <h3>{titulo}</h3>
    </Col>
    <Col xs="12" sm="12" md="6" className="">

      <ButtonToolbar className="float-right">
        <Button color="success" size="sm"
          onClick={() => hanldleSubmit()}
          disabled={bloquear === true ? "disabled" : ""}
        >
          <p>Guardar</p>
        </Button>
        <Button size="sm"
          onClick={() => onCloseModal()}>
          <p>Salir</p>
        </Button>
      </ButtonToolbar>

    </Col>
  </Row>
)

Header.propTypes = {
  titulo: PropTypes.string.isRequired
}

