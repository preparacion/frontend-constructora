import React, { useState, useEffect } from 'react';
import { Col, Row, Card, CardBody, ButtonToolbar, Button } from 'reactstrap';
import { Header } from './Header';
import Modal from 'react-responsive-modal';
import { Select } from 'antd';
import 'antd/lib/date-picker/style/index.css';
import { showMessageWarning, } from '../../../../helpers/MessageAndNotificationUtils';
import Can from '../../../../helpers/Secure/Can';
import ModalConfirmation from '../../../../containers/Modal/ModalConfirmation';

const { Option } = Select;
const selectOption = ["tipo1", "tipo2", "tipo3", "tipo4"]

//formulario para empleados y contratistas
function FormContratista({ titulo, openAddModal, valueAddModal,
  agregarContratista, editarInfo = undefined, editContratista,
  borrarContratista, categories, idObra }) {

  const inicialState = {
    name: "",
    secondName: "",
    aPaterno: "",
    aMaterno: "",
    rfc: "",
    noSocial: "",
    type: "",
    //category: "",
    status: true,
  }

  //estados
  const [contratista, setContratista] = useState(inicialState)
  const [bloquear, setBloquear] = useState(false)
  const [eliminar, setEliminar] = useState(false)

  useEffect(() => {
    //console.log("useEffectMOdalll")
    if (editarInfo != undefined) {

      setContratista({
        name: editarInfo.name,
        secondName: editarInfo.secondName,
        aPaterno: editarInfo.aPaterno,
        aMaterno: editarInfo.aMaterno,
        rfc: editarInfo.rfc,
        noSocial: editarInfo.noSocial,
        type: editarInfo.type,
        status: true,
      })
    } else {
      setContratista(inicialState)//reiniciar losvalores de los estaods
      setBloquear(false)
    }
  }, [editarInfo])

  //cerrar el modal
  const onCloseModal = () => {
    openAddModal(false)
    setContratista(inicialState)//reiniciar losvalores de los estaods
    setBloquear(false)
  }

  //INPUTS//
  const handleChangeInputs = e => {
    setContratista({
      ...contratista, //recordar copiar el objeto actual para no eliminar las otras propiedades
      [e.target.name]: e.target.value
    })
  }

  ///SELECTS////////
  //maneja el evento del selector de Obras
  const handleChangeTipo = (value) => {
    setContratista({
      ...contratista,
      type: value
    })
  }

  //muestra la lista de tipos
  const showTipos = () => {

    return selectOption.map((valor, index) => {
      return <Option value={valor}>{valor}</Option>

    })
  }

  //maneja el evento del selector de categoria
  /*const handleChangeCategorie = (value) => {
    setContratista({
      ...contratista,
      category: value.split("_")[1]
    })
  }

  //muestra la lista de categorias
  const showOptions = () => {
    return categories.map((value) => {
      if (value.name.includes("Contratista") || value.name.includes("contratista")) {
        return <Option value={value.name + "_" + value._id}>{value.name}</Option>
      }

    })
  }*/

  //metodo que valida y envia el empleado a crear o editar
  const hanldleSubmit = () => {

    if (contratista.name == null || contratista.name == "") {
      showMessageWarning("Atención! Debes ingresar un nombre ", 2)
    } else if (contratista.aPaterno == null || contratista.aPaterno == "" ||
      contratista.aMaterno == null || contratista.aMaterno == "") {
      showMessageWarning("Atención! Debes ingresar un apellido materno y paterno", 2)
    } else if (contratista.rfc == null || contratista.rfc == "") {
      showMessageWarning("Atención! Debes ingresar el rfc", 2)
    } else if (contratista.noSocial == null || contratista.noSocial == "") {
      showMessageWarning("Atención! Debes ingresar un numero del seguro social", 2)
    } else if (contratista.type == null || contratista.type == "") {
      showMessageWarning("Atención! Debes seleccionar un tipo", 2)
    } else {
      //validamos i se editara o se creare un nuevo registro
      if (editarInfo != undefined) {
        editContratista(contratista, editarInfo._id)
        setTimeout(() => onCloseModal(), 500)
        //onCloseModal();//cerrar el modal
        setBloquear(true)
      } else {
        agregarContratista(contratista);
        //onCloseModal();//cerrar el modal
        setBloquear(true)
      }
    }
  }

  return (
    <Modal
      open={valueAddModal}
      onClose={onCloseModal}
      center={true}
      closeOnEsc={false}
      showCloseIcon={false}
      closeOnOverlayClick={false}
    >
      <Card className="addEmployeeCont">
        <CardBody>

          <Header
            titulo={titulo}
            onCloseModal={onCloseModal}
            hanldleSubmit={hanldleSubmit}
            bloquear={bloquear}
          />

          <div className="form__form-group">
            <span className="form__form-group-label">Nombre</span>
            <div className="form__form-group-field">
              <input type="text"
                className="form-control"
                name="name" autoComplete="new-password"
                value={contratista.name}
                onChange={handleChangeInputs}
              />
            </div>
          </div>

          <div className="form__form-group">
            <span className="form__form-group-label">Segundo nombre</span>
            <div className="form__form-group-field">
              <input type="text"
                className="form-control"
                name="secondName" autoComplete="new-password"
                value={contratista.secondName}
                onChange={handleChangeInputs}
              />
            </div>
          </div>

          <div className="form__form-group">
            <span className="form__form-group-label">Apellido paterno</span>
            <div className="form__form-group-field">
              <input type="text"
                className="form-control"
                name="aPaterno" autoComplete="new-password"
                value={contratista.aPaterno}
                onChange={handleChangeInputs}
              />
            </div>
          </div>

          <div className="form__form-group">
            <span className="form__form-group-label">Apellido materno</span>
            <div className="form__form-group-field">
              <input type="text"
                className="form-control"
                name="aMaterno" autoComplete="new-password"
                value={contratista.aMaterno}
                onChange={handleChangeInputs}
              />
            </div>
          </div>

          <div className="form__form-group">
            <span className="form__form-group-label">RFC</span>
            <div className="form__form-group-field">
              <input type="text" className="form-control"
                name="rfc"
                autoComplete="new-password"
                value={contratista.rfc}
                onChange={handleChangeInputs}
              />
            </div>
          </div>

          <div className="form__form-group">
            <span className="form__form-group-label">Número Seguro Social</span>
            <div className="form__form-group-field">
              <input className="form-control" type="number"
                autoComplete="new-password"
                name="noSocial"
                defaultValue={contratista.noSocial}
                onChange={handleChangeInputs}
              />
            </div>
          </div>

          <span className="form__form-group-label">Asignar un tipo</span>
          <Col sm={12} md={12} className="selectAMargin">
            <Select
              value={contratista.type}
              showSearch
              style={{ width: 220 }}
              placeholder="Selecionar un tipo"
              onChange={handleChangeTipo}
            >
              {
                showTipos()
              }
            </Select>
          </Col>

          { /*<span className="form__form-group-label mt-3">Asignar una categoría</span>
            <Col sm={12} md={12} className="mb-3 selectAMargin">
              <Select
                //value={empleado.category}Categoria
                //value={Categoria(categories, empleado.category)}
                showSearch
                style={{ width: 220 }}
                placeholder="Selecciona una categoría"
                onChange={handleChangeCategorie}
              >
                {showOptions()}
              </Select>
            </Col>
          */}
          {editarInfo != undefined ?
            <Can do="delete" on="/users/">
              <Row className="d-flex flex-row-reverse">
                <ButtonToolbar>
                  <Button color="danger"
                    onClick={() => setEliminar(true)}>
                    <p>
                      Eliminar</p>
                  </Button>
                </ButtonToolbar>
              </Row>
            </Can> : null}
        </CardBody>
      </Card>

      {(eliminar) ?
        <ModalConfirmation
          closeModal={setEliminar}
          valueModal={eliminar}
          callback={borrarContratista}
          value={editarInfo._id}
          titulo="Confirmación : ¿estas seguro que deseas eliminar este registro?"
          closeParent={onCloseModal}
        /> : null}
    </Modal>
  )
}
export default FormContratista
