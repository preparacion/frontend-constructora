import React, { Component } from 'react';
import { Col, Row, Card, CardBody, Button, ButtonToolbar, } from 'reactstrap';
import { fetchAsync, urlContratistas, fetchAsyncConst } from '../../../helpers/Globals/globals';
import {
  showMessageLoading, showMessageWarning,
} from '../../../helpers/MessageAndNotificationUtils';
import Can from '../../../helpers/Secure/Can';
import Modal from 'react-responsive-modal';
import ModalConfirmation from '../../Modal/ModalConfirmation';

let closeLoading = ""

class EditContratista extends Component {

  constructor(props) {
    super(props);
    this.state = {

      name: "",
      lastName: "",
      secondLastName: "",
      curp: "",
      rfc: "",
      bloquear: false,
      eliminar: false,

    };

  }

  componentWillReceiveProps = (nextProps) => {

    this.setState({

      name: nextProps.contratistaData.nombre,
      lastName: nextProps.contratistaData.apellidoP,
      secondLastName: nextProps.contratistaData.apellidoM,
      //curp: this.props.contratistaData.curp,
      rfc: nextProps.contratistaData.rfc,

    })

  }

  //abre el modal para eliminar un contratista
  openEliminar = (value) => {
    this.setState({
      eliminar: value
    })
  }

  onCloseModal = () => {
    if (closeLoading != "") {
      closeLoading()
    }

    this.props.close(false);

    this.setState({
      name: "",
      lastName: "",
      secondLastName: "",
      curp: "",
      rfc: "",
      bloquear: false,
      eliminar: false,
    })
  };

  //crea uan funcion para lamzar el mensaje de loading
  LoadingMessage = () => {
    return showMessageLoading('Actualizando..', 0);
  };

  //valida y genera las advertencias necesarias para el formulario
  registerContratista = () => {

    let letterNumber = /[0-9]/g;
    let ifNum = this.state.name + this.state.lastName + this.state.secondLastName

    if (this.state.name == null || this.state.name == "") {
      showMessageWarning("Atención! Debes ingresar un nombre", 2)

    } else if (this.state.lastName == null || this.state.lastName == "" ||
      this.state.secondLastName == null || this.state.secondLastName == "") {
      showMessageWarning("Atención! Debes ingresar un apellido paterno y materno", 2)

    } else if (ifNum.match(letterNumber)) {
      showMessageWarning("Atención! Debes escribir solo letras en nombre y apellidos", 2)

    } else if (this.state.rfc == null || this.state.rfc == "") {
      showMessageWarning("Atención! Debes ingresar el RFC", 2)

    } else {
      closeLoading = this.LoadingMessage()
      this.setState({ bloquear: true })//bloquea el boton editar

      // eslint-disable-next-line no-new-object
      let body = new Object()


      body = {

        nombre: this.state.name,
        apellidoP: this.state.lastName,
        apellidoM: this.state.secondLastName,
        rfc: this.state.rfc,
        //curp: this.state.curp,

      }

      console.log(body)


      fetchAsync(urlContratistas + this.props.contratistaData._id, JSON.stringify(body), "PUT", "")
        .then(
          (data) => {
            closeLoading();
            //this.props.getUsers()//callback a index para recargar la tabla
            this.onCloseModal()
            this.props.getActualContratista(data)//recargamos la ficha con los datos actualizados
            this.props.getAllContratistasObras()//recargamos la tabla 
          }).then(() => this.setState({ bloquear: false }))//desbloquea el boton editar)
        .catch(
          (reason) => {
            console.log(reason)
          });
    }

  }

  render() {
    console.log("OpenMOdalEmpleados")
    return (


      <Modal
        open={this.props.valueModal}
        onClose={this.onCloseModal}
        center={true}
        closeOnEsc={false}
        showCloseIcon={false}
        closeOnOverlayClick={false}
      >
        <Card className="addContratistaCont">
          <CardBody>

            <Row>

              <Col sm={12} md={6}>
                <h3>Editar contratista</h3>
              </Col>

              <Col sm={12} md={6}>
                <ButtonToolbar className="ml-1 float-right">
                  <Button size="sm"
                    onClick={() => this.onCloseModal()}>
                    <p>Salir</p>
                  </Button>
                  <Button color="success" size="sm"
                    onClick={this.registerContratista}
                    disabled={this.state.bloquear === true ? "disabled" : ""}
                  >
                    <p>Crear</p>
                  </Button>
                </ButtonToolbar>
              </Col>
            </Row>

            <div className="form__form-group">
              <span className="form__form-group-label">Nombre</span>
              <div className="form__form-group-field">

                <input type="text"
                  className="form-control"
                  id="inpuName" autoComplete="new-password"
                  defaultValue={this.state.name}
                  onChange={(event) => {
                    this.setState({ name: event.target.value })
                    //this.state.name = event.target.value
                  }} />
              </div>
            </div>

            <div className="form__form-group">
              <span className="form__form-group-label">Apellido paterno</span>
              <div className="form__form-group-field">

                <input type="text"
                  className="form-control"
                  id="inputLastname" autoComplete="new-password"
                  defaultValue={this.state.lastName}
                  onChange={(event) => {
                    this.setState({ lastName: event.target.value })
                    //this.state.lastName = event.target.value
                  }} />
              </div>
            </div>

            <div className="form__form-group">
              <span className="form__form-group-label">Apellido materno</span>
              <div className="form__form-group-field">

                <input type="text"
                  className="form-control"
                  id="inputSecondLastName" autoComplete="new-password"
                  defaultValue={this.state.secondLastName}
                  onChange={(event) => {
                    this.setState({ secondLastName: event.target.value })
                    //this.state.secondLastName = event.target.value
                  }} />
              </div>
            </div>

            {/* <div className="form__form-group">
              <span className="form__form-group-label">CURP</span>
              <div className="form__form-group-field">

                <input type="text" className="form-control" id="inputCURP" name="curp"
                  defaultValue={this.state.curp}
                  onChange={(event) => {
                    this.setState({ curp: event.target.value })
                  }} />
              </div>
            </div>
            */}
            <div className="form__form-group">
              <span className="form__form-group-label">RFC</span>
              <div className="form__form-group-field">

                <input className="form-control" type="text"

                  id="inputPassword"
                  defaultValue={this.state.rfc}
                  onChange={(event) => {
                    this.setState({ rfc: event.target.value })

                  }} />
              </div>

            </div>

            <Can do="delete" on="/users/">
              <Row className="d-flex flex-row-reverse deleteButtonEd">
                <ButtonToolbar>
                  <Button color="danger"
                    onClick={() => this.openEliminar(true)}>
                    <p>
                      Eliminar</p>
                  </Button>
                </ButtonToolbar>
              </Row>
            </Can>

          </CardBody>
          <ModalConfirmation
            closeModal={this.openEliminar}
            valueModal={this.state.eliminar}
            callback={this.props.eliminarContratista}
            value={this.props.contratistaData._id}
            titulo="Confirmación : ¿estas seguro que deseas eliminar este registro?"
            closeParent={this.onCloseModal}
          />

        </Card>
      </Modal>

    );
  }
}
export default EditContratista;


