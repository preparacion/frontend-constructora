import React, { Component } from 'react';
import { Col, Row, Card, CardBody, Button, ButtonToolbar, } from 'reactstrap';
import { fetchAsync, urlObras, urlContratistas } from '../../../helpers/Globals/globals';
import {
  showMessageLoading, showMessageWarning,
} from '../../../helpers/MessageAndNotificationUtils';
import Modal from 'react-responsive-modal';
import 'rc-checkbox/assets/index.css';
import 'antd/lib/date-picker/style/index.css';
import TableAsignarContratista from './tableAsignarContratistas';
//import TableContratistas from './tableContratistas';
let closeLoading = ""

class AsignarContratistas extends Component {

  constructor(props) {
    super(props);
    this.state = {
      allContratistas: [],
      actualCon: [],
    };

  }

  componentWillReceiveProps(nextProps) {

    if (nextProps.valueModal === true) {
      console.log("1) AsignarCONConentWillReceiveProps")
      console.log(nextProps)
      return this.getAllContratistas()
    }
  }

  onCloseModal = () => {

    this.props.close(false);
    this.setState({
      allContratistas: [],
      actualCon: [],
    })

  };

  getAllContratistas = () => {

    return fetchAsync(urlContratistas, "", "GET", "")
      .then(
        (result) => {
          this.setState({
            allContratistas: result
          })
        })
      .catch(
        (reason) => {
          console.log(reason.message)
        });
  }

  getInfoContratista = (data) => {
    console.log(data)
    this.setState({
      actualCon: data
    })

  }





  agregarContratista = () => {

    if (this.state.actualCon.length < 1) {
      showMessageWarning("Atención! Debes seleccionar un contratista", 2)
    } else {
      let body = new Object()

      body = {
        contratista: this.state.actualCon._id,
      }

      fetchAsync(urlObras + "addContratistaToObra/" + this.props.idObra, JSON.stringify(body), "PUT", "")
        .then(
          (data) => {
            //closeLoading();
            //this.props.getUsers()//callback a index para recargar la tabla
            this.onCloseModal()
            this.props.getAllContratistasObras()
            //this.props.getRowInfo(data)
          }).catch((reason) => {
            console.log(reason)
          });
    }
  }



  render() {

    return (

      <Modal
        open={this.props.valueModal}
        onClose={this.onCloseModal}
        center={true}
        closeOnEsc={false}
        showCloseIcon={false}
        closeOnOverlayClick={false}
      >
        <Card className="asigarModal">

          <Row className="centerAddContratista">

            <Col sm={12} md={6}>
              <h3>Asignar contratista</h3>
            </Col>

            <Col sm={12} md={6}>
              <ButtonToolbar className="ml-1 float-right">
                <Button size="sm"
                  onClick={() => this.onCloseModal()}>
                  <p>Salir</p>
                </Button>

                <Button color="success" size="sm"
                  onClick={this.agregarContratista}
                  disabled={this.state.bloquear === true ? "disabled" : ""}
                >
                  <p>Agregar</p>
                </Button>
              </ButtonToolbar>
            </Col>

          </Row>

          <div className="tableAsingMar scrollAsignarContratista">
            <TableAsignarContratista
              dataTable={this.state.allContratistas}
              getInfoContratista={this.getInfoContratista}
            />
          </div>
        </Card>
      </Modal>

    );
  }
}
export default AsignarContratistas;


