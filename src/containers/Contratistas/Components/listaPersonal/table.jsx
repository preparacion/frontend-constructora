import React, { PureComponent, Component } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';

import { Col, Button, Row } from 'reactstrap';

import SortTable from '../../../../helpers/Tables/sortTable';
import { month } from '../../../../helpers/Fichas/constants';
import LoadingComponent from "../../../../helpers/LoadingComponent";

import Can from '../../../../helpers/Secure/Can';

class TableListaPersonal extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {

        }
    }

    onCloseModal = () => {
        this.props.openModal();
    };

    //Funcion que enumera las filas de la lista
    generaNum = (cell, row, rowIndex) => {
        if (row) {
            return (rowIndex + 1)
        } else {
            return ""
        }
    }

    //funcion que compara los ids categories de los empleados y les asigna el nombre en la tabla
    showCategorie = (cell, row) => {
        return cell.name;
    }

    render() {
        //console.log("PersonalTable")
        //console.log(this.props.dataTable)
        //console.log("PersonalTable")

        let headerSortingStyle = { backgroundColor: 'white' };
        let { SearchBar } = Search;
        const columns = [

            {
                dataField: '',
                text: '#',
                formatter: this.generaNum,
                csvType: Number,
                csvFormatter: (cell, row, rowIndex) => `${rowIndex + 1}`,
                //csvExport: false,
                headerStyle: (colum, colIndex) => {
                    return { width: '30px', textAlign: 'center', color: "white" };
                },
                headerSortingStyle,
                align: 'center',
            },
            {
                dataField: 'name',
                text: 'Nombre',
                align: 'center',
                sort: true,
                sortCaret: (order, column) => {
                    return <SortTable order={order} colum={column} />
                },
                headerSortingStyle,
                headerAlign: 'center',
            },
            {
                dataField: 'aPaterno',
                text: 'Apellido Paterno',
                align: 'center',
                sort: true,
                sortCaret: (order, column) => {
                    return <SortTable order={order} colum={column} />
                },
                headerSortingStyle,
                headerAlign: 'center',
            },
            {
                dataField: 'aMaterno',
                text: 'Apellido Paterno',
                align: 'center',
                sort: true,
                sortCaret: (order, column) => {
                    return <SortTable order={order} colum={column} />
                },
                headerSortingStyle,
                headerAlign: 'center',
            },
            {
                dataField: 'noSocial',
                text: 'IMSS',
                align: 'center',
                sort: true,
                sortCaret: (order, column) => {
                    return <SortTable order={order} colum={column} />
                },
                headerSortingStyle,
                headerAlign: 'center',
            },
            {
                dataField: 'category',
                text: 'Categoria',
                align: 'center',
                sort: true,
                headerAlign: 'center',
                formatter: this.showCategorie,
                headerSortingStyle,
                sortCaret: (order, column) => {
                    return <SortTable order={order} colum={column} />
                },
                //headerStyle: "tableTitlesColor",

            },

        ];


        let defaultSorted = [{
            dataField: 'name',
            order: 'desc'
        }];


        const rowStyle = {
            color: '#4a4a4a',
            padding: '8px',
        };

        return (
            <Col xs={12} sm={12} md={12} >
                {this.props.dataTable !== undefined ?

                    <div className="modal-body table-responsive">
                        <ToolkitProvider
                            keyField="_id"
                            key="_id"
                            data={this.props.dataTable}
                            columns={columns}
                            //search={{ defaultSearch: '' }}
                            //defaultSorted={defaultSorted}
                            exportCSV={{
                                fileName: `Lista de personal.csv`,
                                exportAll: false,
                                noAutoBOM: false,
                            }}
                            striped
                        >
                            {
                                props => (
                                    <div >
                                        <Button className="btn float-right icon" size="sm" color="pdf" 
                                            href={"http://heg.bluedotmx.com/api/listaDePersonal/" + this.props.idObra}
                                        >
                                            <p>Descargar PDF</p>
                                        </Button>
                                        {/* <SearchBar {...props.searchProps}
                                            className="custome-search-field busqueda barraMarginTop"
                                            placeholder="Buscar"
                                        /> */}

                                        <BootstrapTable
                                            className="tableTitlesColor"
                                            keyField={"_id" + "mail"}
                                            key="_id"
                                            striped
                                            hover
                                            bordered={false}
                                            rowStyle={rowStyle}

                                            {...props.baseProps}
                                            noDataIndication={<LoadingComponent />}
                                            expandRow={this.expandRow}
                                            headerClasses="header-classTable"
                                        />
                                    </div>
                                )}
                        </ToolkitProvider>
                    </div>
                    : " "}


            </Col>
        );
    }
}

export default TableListaPersonal;