import React, { Component } from 'react';
import { Col, Row, Card, CardBody, Button, ButtonToolbar, } from 'reactstrap';
import { fetchAsync, urlObras, urlContratistas, urlCategories } from '../../../../helpers/Globals/globals';
import EmptyComponent from "../../../../helpers/Empty/EmptyComponent";
import Modal from 'react-responsive-modal';
import TableListaPersonal from './table';
import 'rc-checkbox/assets/index.css';
import 'antd/lib/date-picker/style/index.css';

class ListaPersonal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      allEmployess: [],
      categories: [],
    };

  }

  /*componentDidMount() {
    this.getCategories()
    this.getEmployeeByObraContratista(this.props.idObra)
  }*/

  componentWillReceiveProps(nextProps) {

    if (nextProps.valueModal) {
      this.getCategories()
      this.getEmployeeByObraContratista(this.props.idObra)
    }

  }

  //fetch para obtener las categorias
  getCategories = () => {

    return fetchAsync(urlCategories, "", "GET", "")
      .then(
        (result) => {

          this.setState({
            categories: result
          });
        })
      .catch(
        (reason) => {
          console.log(reason.message)
        });
  }



  onCloseModal = () => {
    this.props.close(false);
    this.setState({
      allEmployess: [],
      categories: [],
    })
  };

  getEmployeeByObraContratista = (idObra) => {

    return fetchAsync(urlContratistas + "lista/" + idObra + "/" + this.props.contratistaData._id, "", "GET", "")
      .then(
        (result) => {
          if (result.length > 0) {
            this.setState({
              allEmployess: result
            })
            //console.log("VerPersonal")
            //console.log(result)
            //console.log("VerPersonal")
          } else {
            this.setState({
              allEmployess: []
            })
          }
        })
      .catch(
        (reason) => {
          console.log(reason.message)
        });
  }

  render() {

    return (

      <Modal
        open={this.props.valueModal}
        onClose={this.onCloseModal}
        center={true}
        closeOnEsc={false}
        showCloseIcon={false}
        closeOnOverlayClick={false}
      >

        <CardBody className="listaPersonalSize">

          <Row>
            <Col sm={12} md={6}>
              <h2>Lista de personal</h2>
            </Col>

            <Col sm={12} md={6}>
              <ButtonToolbar className="ml-1 float-right">
                <Button size="sm"
                  onClick={() => this.onCloseModal()}>
                  <p>Salir</p>
                </Button>
              </ButtonToolbar>
            </Col>
          </Row>

          <div className="scrollListaPersonal">
            {(this.state.allEmployess.length < 1) ? <EmptyComponent message="No hay empleados asignados" /> : <TableListaPersonal
              idObra={this.props.idObra}
              idContratista={this.props.contratistaData._id}
              dataTable={this.state.allEmployess}
              categories={this.state.categories}
            //getInfoContratista={this.getInfoContratista}
            />}

          </div>

        </CardBody>

      </Modal>

    );
  }
}
export default ListaPersonal;


