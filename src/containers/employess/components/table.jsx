import React, { PureComponent } from 'react';
import { Col, Container, Row, Card, CardBody } from 'reactstrap';
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import SortTable from '../../../helpers/Tables/sortTable';
import { COLOR_TITLE_TABLES } from '../../../helpers/Fichas/constants';
import LoadingComponent from "../../../helpers/LoadingComponent";
import { COLOR_SORT_TABLES } from '../../../helpers/Tables/constants';
//import paginationFactory from 'react-bootstrap-table2-paginator';
import { Switch } from 'antd';
import paginationFactory, { PaginationProvider, PaginationListStandalone, SizePerPageDropdownStandalone } from 'react-bootstrap-table2-paginator';
import 'antd/dist/antd.css';


class TableEmployee extends PureComponent {

    //metodoque maneja el swicth button
    onChange = (checked, event) => {
        //console.log(event.target.value);
        //console.log(checked);
        this.props.setStatusEmployee(event.target.value)
    }

    //funcion que general el switch button en la tabla
    switchSatus = (cell, row) => {
        console.log(cell)
        if (cell) {
            return (
                <Switch defaultChecked value={row._id} key={row._id} onChange={this.onChange} />
            )
        } else {
            return (
                <Switch value={row._id} key={row._id} onChange={this.onChange} />
            )
        }
    }

    //funcion que compara los ids categories de los empleados y les asigna el nombre en la tabla
    showCategorie = (cell, row) => {

        let obj = this.props.categories.find((obj) => {
            return obj._id === cell;
        });
        return obj !== undefined ? obj.name : ""
    }

    showName = (cell, row) => {
        if (row.secondName) {
            let completo = row.name +" "+ row.secondName
            return completo
        } else {
            return row.name
        }
    }

    render() {

        console.log("TableEmplotyees")
        console.log(this.props.empleados)
        console.log("TableEmplotyees")

        const headerSortingStyle = { backgroundColor: COLOR_SORT_TABLES };
        const { SearchBar } = Search;
        const columns = [

            {
                dataField: 'name',
                text: 'Nombre',
                sort: true,
                headerAlign: 'center',
                align: 'center',
                formatter: this.showName,
                sortCaret: (order, column) => {
                    return <SortTable order={order} colum={column} />
                },
                headerSortingStyle,
                events: {
                    onClick: (e, column, columnIndex, row, rowIndex) => {
                        this.props.getRowInfo(row)
                    },
                },

            },
            {
                dataField: 'aPaterno',
                text: 'A. Paterno',
                sort: true,
                headerAlign: 'center',
                align: 'center',
                sortCaret: (order, column) => {
                    return <SortTable order={order} colum={column} />
                },
                headerSortingStyle,
                events: {
                    onClick: (e, column, columnIndex, row, rowIndex) => {
                        this.props.getRowInfo(row)
                    },
                },

            },
            {
                dataField: 'aMaterno',
                text: 'A. Materno',
                sort: true,
                headerAlign: 'center',
                align: 'center',

                sortCaret: (order, column) => {
                    return <SortTable order={order} colum={column} />
                },
                headerSortingStyle,
                events: {
                    onClick: (e, column, columnIndex, row, rowIndex) => {
                        this.props.getRowInfo(row)
                    },
                },

            },
            {
                dataField: 'noSocial',
                text: 'IMSS',
                headerAlign: 'center',
                align: 'center',
                sort: true,
                sortCaret: (order, column) => {
                    return <SortTable order={order} colum={column} />
                },
                headerSortingStyle,
                events: {
                    onClick: (e, column, columnIndex, row, rowIndex) => {
                        this.props.getRowInfo(row)
                    },
                },

            },
            {
                dataField: 'category',
                text: 'Categoria',
                align: 'center',
                sort: true,
                headerAlign: 'center',
                formatter: this.showCategorie,
                events: {
                    onClick: (e, column, columnIndex, row, rowIndex) => {
                        this.props.getRowInfo(row)
                    },
                },
                headerSortingStyle,
                sortCaret: (order, column) => {
                    return <SortTable order={order} colum={column} />
                },

            },
            {
                dataField: 'status',
                text: 'Estatus',
                align: 'center',
                headerAlign: 'center',
                formatter: this.switchSatus,
            },
        ];

        const rowEvents = {
            onClick: (e, row, rowIndex) => {
                console.log(`clicked on row with index: ${rowIndex}`);
                console.log(row);
                this.props.getRowInfo(row)
            },
        };


        const options = {
            custom: true,
            //paginationSize: 4,
            //pageStartIndex: 1,
            paginationSize: 4,
            pageStartIndex: 0,
            firstPageText: 'Primera',
            prePageText: 'Anterior',
            nextPageText: 'Siguiente',
            lastPageText: 'Ultima',
            nextPageTitle: 'First page',
            prePageTitle: 'Pre page',
            firstPageTitle: 'Next page',
            lastPageTitle: 'Last page',
            showTotal: true,
            //totalSize: this.props.empleados.length
            sizePerPageList: [{
                text: '15', value: 15
            },
            {
                text: '30', value: 30
            },
            {
                text: '50', value: 50
            },
            {
                text: 'Todos', value: this.props.empleados.length
            }]
        };


        const contentTable = ({ paginationProps, paginationTableProps }) => (
            <Col sm={12} md={12}>
                <Row>

                    <Col xs={12} sm={12} md={6} >
                        <div className="float-left">

                            <SizePerPageDropdownStandalone
                                {...paginationProps}
                            />

                        </div>

                    </Col>
                    <Col xs={12} sm={12} md={6} >
                        <div className="float-right">
                            <PaginationListStandalone {...paginationProps} />
                        </div>

                    </Col>

                </Row>


                <ToolkitProvider
                    keyField="_id"
                    columns={columns}
                    data={this.props.empleados}
                    search
                >
                    {
                        toolkitprops => (
                            <div className="scrollB">

                                <Row>
                                    <Col xs={12} sm={12} md={12} className="mt-1 mb-4">
                                        <SearchBar {...toolkitprops.searchProps}
                                            className="custome-search-field busqueda serachBarStyle"
                                            style={{
                                                //color: '#2980B9',
                                                backgroundColor: "#ecf0f1",
                                                //width: "450px"
                                            }}
                                            placeholder="Busca un empleado"
                                            autoComplete="off"
                                        />

                                    </Col>
                                </Row>


                                <BootstrapTable
                                    keyField="_id"
                                    striped
                                    hover
                                    bordered={false}
                                    //rowEvents={rowEvents}
                                    {...toolkitprops.baseProps}
                                    {...paginationTableProps}
                                    noDataIndication={<LoadingComponent />}
                                    headerClasses="header-classTable"
                                />
                            </div>
                        )
                    }
                </ToolkitProvider>
            </Col>
        );

        return (
            <Container >
                <Row>
                    <Col sm={12} md={12}>
                        <Card>



                            <PaginationProvider
                                pagination={
                                    paginationFactory(options)
                                }
                            >
                                {contentTable}
                            </PaginationProvider>



                        </Card>
                    </Col>
                </Row>
            </Container>
        );
    }
}
export default TableEmployee;
