import React, { PureComponent, Component } from 'react';
import {
    Col, Row, Button, ButtonToolbar,
    Card, ButtonGroup, Container, CardBody, CardTitle, CardHeader, CardText
} from 'reactstrap';
import FormEC from '../../../helpers/Forms/FormEC';
import AddComentarioModal from '../../../helpers/Comentarios/addComments';
import SetFechaTable from '../../../helpers/Tables/fechaTables';
import EmptyComponent from "../../../helpers/Empty/EmptyComponent";
import UploadFileEmployee from '../../uploadFiles/index';
import AvatarName from '../../../helpers/Fichas/AvatarName';

import listDocuments from '../../../helpers/Fichas/showDocuments';
import Can from '../../../helpers/Secure/Can';

//const Ava = `${process.env.PUBLIC_URL}/img/generics/face.png`;

class Ficha extends PureComponent {

    constructor(props) {
        console.log("0) constructor")
        super(props)
        this.state = {
            editEmployeModal: false,
            grupos: [],
            modalComent: false,
            uploadFile: false,
        }
    }

    openModalEdit = (value) => {
        this.setState({
            editEmployeModal: value,
        })
    }

    openModalComent = (value) => {
        this.setState({
            modalComent: value
        })
    }

    openModalUploadFile = (value) => {
        this.setState({
            uploadFile: value
        })
    }

    disponible = (valor) => {
        if (valor === true) {
            return (
                <Row className="textEnable">
                    <i className="material-icons statusEneable" >fiber_manual_record</i>
                    <p  >Activo</p>
                </Row>
            )
        } else {
            return (
                <Row className="textDisable">
                    <i className="material-icons statusDisable" >fiber_manual_record</i>
                    <p>Inactivo</p>
                </Row>
            )
        }
    }

    //funcion que compara los ids categories de los empleados y les asigna el nombre en la tabla
    showCategorie = () => {

        let obj = this.props.categories.find((obj) => {
            return obj._id === this.props.actualEmployee.category;
        });
        return obj !== undefined ? obj.name : ""
    }

    //funcion que compara los ids categories de los empleados y les asigna el nombre en la tabla
    showObra = () => {

        let obj = this.props.obras.find((obj) => {
            return obj._id === this.props.actualEmployee.obra;
        });
        return obj !== undefined ? obj.name : ""
    }

    //funcion que compara los ids categories de los empleados y les asigna el nombre en la tabla
    showContratista = () => {

        let obj = this.props.contratistas.find((obj) => {
            return obj._id === this.props.actualEmployee.contratista;
        });
        return obj !== undefined ? obj.name + " " + obj.aPaterno + " " + obj.aMaterno : ""
    }


    listComments = () => {
        if (this.props.actualEmployee.comments.length > 0) {
            return this.props.actualEmployee.comments.map((data, index) => {
                return (
                    <div key={index}>
                        <Card className="commentCenterText">
                            <CardHeader>Publicado el: {SetFechaTable(data.date)}</CardHeader>
                            <CardBody>
                                {data.comment}
                            </CardBody>
                        </Card>
                    </div>
                )
            })
        } else {
            return (
                <Card className="cardDownload">
                    <CardBody>
                        <EmptyComponent message="Aún no hay comentarios agregados" />
                    </CardBody>
                </Card>)
        }

    }

    showDate = (fecha) => {
        let date = new Date(fecha.split("T")[0])
        return date.toLocaleString().split(" ")[0]
    }

    render() {
        console.log("RenderFichaEmployee")
        console.log(this.props.actualEmployee)
        console.log("RenderFichaEmployee")

        let { name, secondName, aMaterno, aPaterno, curp,
            noSocial, status, createdAt, files, contratista, obra } = this.props.actualEmployee

        return (
            <Container >
                <Card className="fichaEmployeeSize">

                    <Row className={"no-gutters"}>
                        <Col sm={12} md={12}>
                            <Button className="icon square btn-ficha float-right" type="button" color="success" outline
                                onClick={() => this.openModalUploadFile(true)}
                            >
                                <div className="textButonFicha" >
                                    <p ><i className="material-icons">cloud_upload</i>Subir archivo</p>
                                </div>
                            </Button>
                        </Col>

                        <Row>
                            <Col xs="12" lassName="ficha_name_container">
                                <AvatarName
                                    name={name}
                                    secondName={secondName}
                                    aMaterno={aMaterno}
                                    aPaterno={aPaterno}
                                />
                                <p className="fichaData ml-1">{this.disponible(status)}</p>

                            </Col>
                            <Col sm={12} md={12} className="ficha_name_container">

                            </Col>

                        </Row>
                    </Row>
                    <Can do="put" on="/users/">
                        <Row>
                            <Col >
                                <ButtonToolbar className="float-right btn-ficha-margin">

                                    <Button className="icon square btn-ficha" type="button" color="success" outline
                                        onClick={() => this.openModalComent(true)}>
                                        <div className="textButonFicha" >
                                            <p ><i className="material-icons">add_circle_outline</i>Comentario</p>
                                        </div>
                                    </Button>
                                    <Button className="icon square btn-ficha" type="button" color="success" outline
                                        onClick={() => this.openModalEdit(true)}
                                    >
                                        <div className="textButonFicha" >
                                            <p ><i className="material-icons">edit</i>Editar</p>
                                        </div>
                                    </Button>

                                </ButtonToolbar>
                            </Col>
                        </Row>
                    </Can>
                    <Row>
                        <Col xs={12} sm={12} md={12} className="ficha_name_container">


                            <div className="mt-2 textTitle">
                                {curp !== undefined && curp !== "" ? <p className="subTitulosFicha ">CURP</p> : ""}
                            </div>
                            <p className="fichaData">{curp}</p>

                            <div className="mt-2 textTitle">
                                {noSocial !== undefined && noSocial !== "" ? <p className="subTitulosFicha textTitle">IMSS</p> : ""}
                            </div>
                            <p className="fichaData">{noSocial}</p>

                            <div className="mt-2 textTitle">
                                <p className="subTitulosFicha">Categoria</p>
                            </div>
                            <p className="fichaData">{this.showCategorie()}</p>

                            <div className="mt-2 textTitle">
                                {contratista !== undefined ? <p className="subTitulosFicha textTitle">Contratista</p> : ""}
                            </div>
                            <p className="fichaData">{this.showContratista()}</p>

                            <div className="mt-2 textTitle">
                                {obra !== undefined ? <p className="subTitulosFicha textTitle">Obra</p> : ""}
                            </div>
                            <p className="fichaData">{this.props.obras.length > 0 ? this.showObra() : ""}</p>

                            <div className="mt-2 textTitle">
                                <p className="subTitulosFicha textTitle">Fecha alta</p>
                            </div>
                            <p className="fichaData">{this.showDate(createdAt)}</p>

                            <div className="mt-2 textTitle">
                                <p className="subTitulosFicha">Neto HEG</p>
                            </div>
                            <p className="fichaData">{"2500"}</p>

                            <div className="mt-2 textTitle">
                                <p className="subTitulosFicha">ISR</p>
                            </div>
                            <p className="fichaData">{"$221.64"}</p>
                        </Col>
                    </Row>

                    <Row>
                        <Col className="my-3">
                            <h4>Documentos</h4>
                            {listDocuments(files)}
                        </Col>
                    </Row>

                    <Row>
                        <Col>
                            <h4>Comentarios</h4>
                            {this.listComments()}
                        </Col>
                    </Row>


                </Card>

                <AddComentarioModal
                    openModal={this.openModalComent}
                    valueModal={this.state.modalComent}
                    _idEmployee={this.props.actualEmployee._id}
                    getRowInfo={this.props.getRowInfo}
                    getUsers={this.props.getUsers}
                //cargarComents={this.commentsSearchByGroupAndStudenId.bind(this)}
                />

                {(this.state.editEmployeModal) ? <FormEC
                    titulo="Editar"
                    openAddModal={this.openModalEdit}
                    valueAddModal={this.state.editEmployeModal}
                    categories={this.props.categories}
                    contratistas={this.props.contratistas}
                    obras={this.props.obras}
                    editarInfo={this.props.actualEmployee}
                    editEmployee={this.props.editEmployee}
                    deleteEmployIndex={this.props.deleteEmployIndex}
                /> : null}

                {(this.state.uploadFile) ? <UploadFileEmployee
                    valueModal={this.state.uploadFile}
                    close={this.openModalUploadFile}
                    idEmployee={this.props.actualEmployee._id}
                    getRowInfo={this.props.getRowInfo}

                /> : null}

            </Container>
        )
    }

}
export default Ficha