import React, { Component } from 'react';
import { Col, Container, Row, Button, } from 'reactstrap';
import {
    fetchAsync, urlContratistas,
    urlEmployees, urlCategories, urlObras
} from '../../helpers/Globals/globals';
import TableEmployee from './components/table';
import Ficha from './components/Ficha';
import AddIcon from 'mdi-react/AddIcon';
import FormEC from '../../helpers/Forms/FormEC';
import Can from "../../helpers/Secure/Can";
import { showNotification, showMessageLoading } from '../../helpers/MessageAndNotificationUtils/';

let closeLoading = "";

class Employees extends Component {

    constructor(props) {
        super(props)
        this.state = {
            addModal: false,
            empleados: [],
            categories: [],
            allContratistas: [],
            allObras: [],
            actualEmployee: '',
        }
    }

    componentDidMount() {
        this.getUsers()
        this.getCategories()
        this.getAllContratistas()
        this.getAllObras()
    }

    //metodo para abrir o cerrar el modal agregar
    openAddModal = (value) => {

        this.setState({
            addModal: value,
        })
        console.log("entro al modal")
        console.log(this.state.addModal)
    }

    //funcion que trae los datos del row al darle click a la table es un callback
    getRowInfo = (datos) => {
        console.log(datos)
        this.props.openSide(
            <Ficha
                actualEmployee={datos}
                deleteEmployIndex={this.eliminarEmploye.bind(this)}
                getUsers={this.getUsers.bind(this)}
                getRowInfo={this.getRowInfo.bind(this)}
                categories={this.state.categories}
                contratistas={this.state.allContratistas}
                obras={this.state.allObras}
                editEmployee={this.editEmployee}
            />
        )
    }

    //OPERACIONES DE RED//

    //fetch para obtener todos los empleados
    getUsers = () => {
        return fetchAsync(urlEmployees, "", "GET", "")
            .then((result) => {
            
                this.setState({
                    empleados: result.map(a => ({ ...a }))
                });
            }).catch((reason) => {
                console.log(reason.message)
            });
    }

    //fetch para obtener las categorias
    getCategories = () => {

        return fetchAsync(urlCategories, "", "GET", "")
            .then((result) => {
                this.setState({
                    categories: result
                });
            }).catch((reason) => {
                console.log(reason.message)
            });
    }

    //funcion para traerse todos los contratistas
    getAllContratistas = () => {

        return fetchAsync(urlContratistas, "", "GET", "")
            .then((result) => {
                this.setState({
                    allContratistas: result
                })
            }).catch((reason) => {
                console.log(reason.message)
            });
    }

    getAllObras = () => {
        return fetchAsync(urlObras, "", "GET", "")
            .then((result) => {
                this.setState({
                    allObras: result
                })
            }).catch((reason) => {
                console.log(reason.message)
            });
    }

    //funcion para eliminar un employee
    eliminarEmploye = (id) => {
        fetchAsync(urlEmployees + id, "", "DELETE", "")
            .then((data) => {
                this.getUsers()
            }).catch(
                (reason) => {
                    console.log(reason.message)
                    //store.showModalDelete
                }
            );
    }

    addEmployee = (body) => {
        closeLoading = this.LoadingMessage()
        fetchAsync(urlEmployees, JSON.stringify(body), "POST", "")
            .then((response) => {
                console.log("verEstatus")
                console.log(response)
                console.log(response.status)
                console.log("verEstatus")
                this.getUsers()
                closeLoading();
                this.openAddModal(false)

            }).catch((e) => {
                closeLoading();
                console.log(e.status)
                console.log(e)
                alert(e)
            });
    }

    editEmployee = (body, id) => {
        closeLoading = this.LoadingMessageEdit()
        fetchAsync(urlEmployees + id, JSON.stringify(body), "PUT", "")
            .then((data) => {
                closeLoading();
                this.getUsers()//callback a index para recargar la tabla
                this.getRowInfo(data)
            }).catch((reason) => {
                closeLoading();
                showNotification("error", "No se pudo guardar el cambio: ", "", 0)
            });
    }

    setStatusEmployee = (idEmployee) => {

        fetchAsync(urlEmployees + "status/" + idEmployee, "", "PUT", "")
            .then(
                (data) => {
                    console.log(data)
                    this.getUsers()
                    this.getRowInfo(data)
                }).catch(
                    (reason) => {
                        console.log(reason)
                    }
                );
    }

    //MENSAJES///
    //crea uan funcion para lamzar el mensaje de loading
    LoadingMessage = () => {
        return showMessageLoading('Registrando..', 0);
    };

    LoadingMessageEdit = () => {
        return showMessageLoading('Editando..', 0);
    };

    LoadingMessageBorrar = () => {
        return showMessageLoading('Borrando..', 0);
    };

    render() {
        /*console.log("RenderEmployes")
        console.log(this.state.empleados)
        console.log(this.state.categories)
        console.log("RenderEmployes")*/
        return (
            <Container className="dashboard">
                <Row>
                    <Col sm={12} md={12} className="mt-2">

                        <Row>
                            <Col xs={12} sm={12} md={7} className="mt-2 titlesComponents">
                                <div className="typography--inline">
                                    <p className="subhead">Gestión de usuarios |</p>
                                    <p style={{ color: "#3057af", marginLeft: "5px" }}>Empleados</p>
                                </div>
                            </Col>
                            <Can do="post" on="/users/">
                                <Col xs={12} sm={12} md={5} className="mt-2">
                                    <Button className="btn float-right icon" size="sm" color="success"
                                        style={{ marginRight: "15px" }}
                                        onClick={() => this.openAddModal(true)}
                                    >
                                        <p><AddIcon />Añadir empleado</p>
                                    </Button>
                                </Col>
                            </Can>
                        </Row>

                        <Row>
                            <Col sm={12} >
                                <TableEmployee
                                    empleados={this.state.empleados}
                                    getRowInfo={this.getRowInfo}
                                    categories={this.state.categories}
                                    setStatusEmployee={this.setStatusEmployee}
                                />
                            </Col>
                            <Col sm={4}>
                            </Col>
                        </Row>
                    </Col>
                </Row>

                {(this.state.addModal) ? <FormEC
                    titulo="Agregar"
                    openAddModal={this.openAddModal}
                    valueAddModal={this.state.addModal}
                    categories={this.state.categories}
                    contratistas={this.state.allContratistas}
                    obras={this.state.allObras}
                    addEmployee={this.addEmployee}
                /> : null}

            </Container>
        )
    }
}
export default Employees