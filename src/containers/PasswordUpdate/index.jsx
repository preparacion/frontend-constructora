import React from 'react';
import { Col, Container, Row } from 'reactstrap';
import PropTypes from 'prop-types';
import VerticalForm from './components/VerticalForm';

const PasswordUpdate = (props="") => {

  let urlId = props.infoUrl.split("/")[3]

  return (<Container>
    <Row>
      <Col md={12}>
        <h3 className="page-title">Editar contraseña</h3>
        <h3 className="page-subhead subhead">
          Aqui podrás editar tu contraseña por una nueva, la contraseña debe tener entre 5 a 10 digitos.
        </h3>
      </Col>
    </Row>
    <Row>
      <VerticalForm id={urlId} />
    </Row>
  </Container>)
};

PasswordUpdate.propTypes = {
  t: PropTypes.func.isRequired,
};

export default PasswordUpdate;
