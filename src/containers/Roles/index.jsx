import React, { PureComponent } from 'react';
import { Col, Container, Row, Button, } from 'reactstrap';
import Ficha from "./components/Ficha"
import TableRoles from "./components/tableRoles"
import ModalRol from "./components/ModalRol"
import AddIcon from 'mdi-react/AddIcon';
import Can from "../../helpers/Secure/Can"

class Roles extends PureComponent {

    constructor(props) {
        super(props)
        this.state = {
            valueModal: false,
            forceUpdateProp: false
        }
    }

    openModal = () => {
        this.setState({
            valueModal: true,
        })
    }

    closeModal = () => {
        this.setState({
            valueModal: false,
            forceUpdateProp: !this.state.forceUpdateProp
        })
    }

    forceUpdate = () => {
        this.setState({
            forceUpdateProp: !this.state.forceUpdateProp
        })
    }

    handleClick = (element) => {
        this.props.openSide(
            <Ficha
                rolElement={element}
                update={this.forceUpdate}
                closeTab={this.props.closeTab}
            />
        )
    }

    render() {
        return (
            <Container className="dashboard">
                <Row>
                    <Col xs={12} sm={12} md={7} className="mt-2 titlesComponents">
                        <div className="typography--inline">
                            <p className="subhead">Gestión de usuarios  |</p>
                            <p style={{ color: "#3057af", marginLeft: "5px" }}>Roles</p>
                        </div>
                    </Col>
                    <Can do="post" on="/roles/*">
                        <Col xs={12} sm={12} md={5} className="mt-2">
                            <Button className="btn float-right icon" size="sm" color="success"
                                style={{ marginRight: "15px" }}
                                onClick={() => this.openModal()}>
                                <p><AddIcon />Agregar Rol</p>
                            </Button>
                        </Col>
                    </Can>
                </Row>
                <Row>
                    <TableRoles
                        ClickOnElement={this.handleClick}
                        update={this.state.forceUpdateProp}
                    />
                </Row>
                <ModalRol
                    value={this.state.valueModal}
                    close={this.closeModal}
                />
            </Container>
        )
    }

}
export default Roles