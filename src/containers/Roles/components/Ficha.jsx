import React, { PureComponent } from 'react';
import { Col, Container, Row, Button, ButtonToolbar, ButtonGroup, Card, CardBody, } from 'reactstrap';
import { Tabs, Popconfirm } from 'antd';
import PermissionList from "./PermissionList"
import UsersList from "./UsersList"
import { reverseServiceToTree } from "../../../helpers/Secure/utils"
import 'antd/lib/tabs/style/index.css';
import DeleteIcon from "mdi-react/DeleteIcon"
import 'antd/lib/popover/style/index.css'
import '../../../scss/component/motion.scss'
import { structPermissionTree } from "../../../helpers/Secure/utils"
import { fetchAsync, urlRoles } from '../../../helpers/Globals/globals';
import { showMessageLoading, showMessageSuccess, showMessageWarning, showNotification } from '../../../helpers/MessageAndNotificationUtils';

import Can from '../../../helpers/Secure/Can';
let loadingMessage
const { TabPane } = Tabs

class Ficha extends PureComponent {


    /*****************************************
     * ***************************************
     * *************LifeCycle*****************
     * ***************************************
     * ***************************************
    */

    constructor(props) {
        super(props)
        let allowa = reverseServiceToTree(this.props.rolElement ? this.props.rolElement.allows : "")
        this.state = {
            name: this.props.rolElement.name,
            description: this.props.rolElement.description,
            nameEdit: this.props.rolElement.name,
            descriptionEdit: this.props.rolElement.description,
            checkedKeys: allowa,
            isUpdate: false,
            loading: false,
            Rol: this.props.rolElement
        }
    }

    componentDidMount() {

    }
    componentDidUpdate(prepProps, prepState) {
        if (prepProps.rolElement._id !== this.props.rolElement._id) {
            let allowa = reverseServiceToTree(this.props.rolElement ? this.props.rolElement.allows : "")
            this.setState({
                name: this.props.rolElement.name,
                description: this.props.rolElement.description,
                nameEdit: this.props.rolElement.name,
                descriptionEdit: this.props.rolElement.description,
                checkedKeys: allowa,
                isUpdate: false,
                loading: false,
                Rol: this.props.rolElement
            })
        }
    }

    handlerCheck = (checkedKeys) => {
        this.setState({
            checkedKeys: checkedKeys
        })

    }

    clickEdit = () => {
        this.setState({
            isUpdate: true
        })
    }

    clickCancel = () => {
        this.setState({
            isUpdate: false
        })
    }


    clickSave = () => {
        this.setState({
            loading: true
        })
        loadingMessage = showMessageLoading("Actualizando Rol", 0)
        let allows = structPermissionTree(this.state.checkedKeys)
        let body = {
            name: this.state.nameEdit,
            description: this.state.descriptionEdit,
            allows: allows
        }
        fetchAsync(urlRoles + this.props.rolElement._id, JSON.stringify(body), "PUT", "")
            .then(
                result => {
                    if (result.success) {
                        //This will hide loadiing message
                        loadingMessage()
                        showMessageSuccess("Rol Actualizado exitosamente", 2.5)
                        this.props.update()
                        this.setState({
                            isUpdate: false,
                            name: this.state.nameEdit,
                            description: this.state.descriptionEdit,
                            loading: false,
                            Rol: result.role
                        })
                    } else {
                        let message = "Ocurrio un error al crear el rol"
                        showMessageWarning(message, 2.5)
                        showNotification("error", message, "", 0)
                    }
                }
            )
            .catch(

            )
    }

    clickDelete = () => {
        this.setState({
            loading: true
        })
        loadingMessage = showMessageLoading("Eliminando el Rol", 0)
        fetchAsync(urlRoles + this.props.rolElement._id, "", "DELETE", "")
            .then(
                result => {
                    if (result.success) {
                        loadingMessage()
                        showMessageSuccess("Rol Eliminado exitosamente", 2.5)
                        this.props.update()
                        this.props.closeTab()
                    } else {

                    }
                })
            .catch()
    }


    setIsUpdate = (value) => {
        this.setState({
            isUpdate: value
        })
    }

    /*****************************************
     * ***************************************
     * *************HelperFunctions***********
     * ***************************************
     * ***************************************
    */

    openModalTabs = (value) => {
        this.setState({
            openTabsModal: value,
        })
    }

    render() {
        return (
            <Container >
                <Card >
                    <Row>
                        <Col >
                            <ButtonToolbar className="float-right btn-ficha-margin">
                                {this.state.isUpdate ?
                                    <ButtonGroup >
                                        <Button className="icon square btn-ficha" type="button" color="success" outline
                                            onClick={() => this.clickCancel()} disabled={this.state.loading}>
                                            <div className="textButonFicha" >
                                                <p>Cancelar</p>
                                            </div>
                                        </Button>
                                        <Button className="icon square btn-ficha" type="button" color="success" outline
                                            onClick={() => this.clickSave()} disabled={this.state.loading}>
                                            <div className="textButonFicha" >
                                                <p ><i className="material-icons">done</i>Guardar</p>
                                            </div>
                                        </Button>
                                    </ButtonGroup>
                                    :
                                    <Can do="put" on="/roles/*">
                                        <Button className="icon square btn-ficha" type="button" color="success" outline
                                            onClick={() => this.clickEdit()} >
                                            <div className="textButonFicha" >
                                                <p ><i className="material-icons">edit</i>Editar</p>
                                            </div>
                                        </Button>
                                    </Can>
                                }
                            </ButtonToolbar>
                        </Col>
                    </Row>
                    <Row>
                        <Col className>
                            {this.state.isUpdate ?
                                <div>
                                    <div className="form__form-group">
                                        <span className="form__form-group-label">Nombre</span>
                                        <div className="form__form-group-field">
                                            <input type="text"
                                                className="form-control"
                                                id="inpuName" autocomplete="off"
                                                defaultValue={this.state.nameEdit}
                                                onChange={(event) => {
                                                    this.setState({ nameEdit: event.target.value })
                                                }} disabled={this.state.loading} />
                                        </div>
                                    </div>
                                    <div className="form__form-group">
                                        <span className="form__form-group-label">Descripción</span>
                                        <div className="form__form-group-field">
                                            <input type="text"
                                                className="form-control"
                                                id="inpuName" autocomplete="off"
                                                defaultValue={this.state.descriptionEdit}
                                                onChange={(event) => {
                                                    this.setState({ descriptionEdit: event.target.value })
                                                }} disabled={this.state.loading} />
                                        </div>
                                    </div>
                                </div>
                                :
                                <div>
                                    <p className="fichaNameTitle">{this.state.name}</p>
                                    <p>{this.state.description}</p>
                                </div>
                            }
                            <Tabs size="small">
                                <TabPane tab="Permisos" key="1">
                                    <PermissionList
                                        Rol={this.state.Rol}
                                        handlerCheked={this.handlerCheck}
                                        isUpdate={this.state.isUpdate}
                                        disabled={this.state.loading}
                                    />
                                </TabPane>
                                {this.state.isUpdate ? "" :
                                    <TabPane tab="Usuarios" key="2">
                                        <UsersList
                                            rol={this.state.Rol}
                                            update={this.props.update}
                                        />
                                    </TabPane>
                                }
                            </Tabs>
                            {!this.state.isUpdate ? "" :
                                <Can do="delete" on="/roles/*">
                                    <div>
                                        <Popconfirm title="¿Estás seguro de eliminar este Rol？Esta acción no puede deshacerse"
                                            okText="Si"
                                            cancelText="No"
                                            icon={<i className="material-icons red_icon">error</i>}
                                            placement="topLeft"
                                            onConfirm={() => { this.clickDelete() }}>
                                            <Button size="sm" className="btn float-right" color="danger">
                                                <p><DeleteIcon />Eliminar</p>
                                            </Button>
                                        </Popconfirm>
                                    </div>
                                </Can>
                            }
                        </Col>
                    </Row>

                </Card>
            </Container >
        )
    }

}
export default Ficha