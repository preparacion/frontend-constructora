import React, { PureComponent } from 'react';
import { Avatar, Popconfirm } from 'antd';
import { fetchAsync, urlRoles } from '../../../helpers/Globals/globals';
import DeleteIcon from 'mdi-react/DeleteIcon';
import LoadingComponent from "../../../helpers/LoadingComponent"
import 'antd/lib/avatar/style/index.css';
//Styles for Popconfirm
import 'antd/lib/popover/style/index.css'
import '../../../scss/component/motion.scss'

class UsersList extends PureComponent {

    constructor(props) {
        super(props)
        this.state = {
            notifications: [],
            loading: true,
        }
    }

    componentDidMount() {
        this.getRolesUsers()
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.rol._id !== this.props.rol._id) {
            this.getRolesUsers()
        }
    }


    onDeleteUser = () => {
        // This will update parent
        // this.props.update()
    }

    getRolesUsers = () => {
        if (!this.state.loading)
            this.setState({
                loading: true,
            })
        fetchAsync(urlRoles + this.props.rol._id + "/users", "", "GET", "")
            .then(
                (result) => {
                    if (result.success === true) {
                        this.setState({
                            notifications: result.users,
                            loading: false
                        })
                    } else {
                        console.log('success false');
                    }
                }
            )
            .catch(

            )
    }

    render() {
        return (
            <div>
                {this.state.loading ? <LoadingComponent /> :
                    this.state.notifications.map((notification, index) => (
                        <div className="topbar__collapse-item" key={index}>
                            <div className="topbar__collapse-img-wrap">
                                <Avatar icon="user" />
                            </div>
                            <span>{notification.name}</span>
                            <div className="erase_button_card">
                                <Popconfirm title="¿Deseas quitar el rol a este usuario？"
                                    okText="Si"
                                    cancelText="No"
                                    icon={<i className="material-icons red_icon">error</i>}
                                    placement="topLeft"
                                    onConfirm={() => { alert("Borrar " + notification.name) }}>
                                    <DeleteIcon />
                                </Popconfirm>
                            </div>
                        </div>
                    ))}
            </div>
        );
    }
}
export default UsersList;
