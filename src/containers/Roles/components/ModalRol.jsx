import React, { PureComponent } from 'react';
import { Col, Row, Button, ButtonToolbar, Card, CardBody } from 'reactstrap';
import Modal from 'react-responsive-modal';
import PermissionList from "./PermissionList"
import {structPermissionTree,reverseServiceToTree} from "../../../helpers/Secure/utils"
import { fetchAsync , urlRoles} from '../../../helpers/Globals/globals';
import {showMessageLoading,showMessageSuccess,showMessageWarning,showNotification} from '../../../helpers/MessageAndNotificationUtils';

let loadingMessage

class ModalRol extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            name: "",
            description: "",
            checkedKeys:[],
        };
    }

    onCloseModal = () => {
        this.props.close();

    };

    handlerCheck=(checkedKeys)=>{
        this.setState({
            checkedKeys:checkedKeys
        })

    }

    saveRol = () =>{
        loadingMessage=showMessageLoading("Creando Rol",0)
        let allows= structPermissionTree(this.state.checkedKeys.length>0 ? this.state.checkedKeys:reverseServiceToTree(undefined))
        let body = {
            name:this.state.name,
            description:this.state.description,
            allows:allows
        }
        fetchAsync(urlRoles,JSON.stringify(body),"POST","")
        .then(
            result =>{
                if (result.success){
                    //This will hide loadiing message
                    loadingMessage()
                    showMessageSuccess("Rol creado exitosamente",2.5)
                    this.onCloseModal()
                }else{
                    let message="Ocurrio un error al crear el rol"
                    showMessageWarning(message,2.5)
                    showNotification("error",message,"",0)
                }
            }
        )
        .catch(

        )

    }



    render() {
        return (
            <Modal
                open={this.props.value}
                onClose={this.onCloseModal}
                center={true}
                closeOnEsc={false}
                showCloseIcon={false}
                closeOnOverlayClick={false}>

                <Col sm={12} md={12}>
                    <Card>

                        <Row>
                            <Col xs={12} sm={4} md={4}>
                                <h4 className="bold-text mt-3">Crear nuevo Rol</h4>
                            </Col>
                            <Col xs={12} sm={8} md={8} className="mt-2">
                                <Button size="sm" className="btn float-right"
                                    //color="success"
                                    style={{ marginRight: "10px" }}
                                    onClick={() => this.onCloseModal()}>
                                    <p>Salir</p>
                                </Button>
                                <Button size="sm" className="btn float-right"
                                    color="success"
                                    style={{ marginRight: "10px" }}
                                    onClick={() => this.saveRol()}>
                                    <p>Guardar</p>
                                </Button>

                            </Col>
                        </Row>
                        <CardBody>
                            <div className="form__form-group">
                                <span className="form__form-group-label">Nombre</span>
                                <div className="form__form-group-field">
                                    <input type="text"
                                        className="form-control"
                                        id="inpuName" autocomplete="off"
                                        defaultValue={this.state.name}
                                        onChange={(event) => {
                                            this.setState({ name: event.target.value })
                                            //this.state.name = event.target.value
                                        }} />
                                </div>
                            </div>
                            <div className="form__form-group">
                                <span className="form__form-group-label">Descripción</span>
                                <div className="form__form-group-field">
                                    <input type="text"
                                        className="form-control"
                                        id="inpuName" autocomplete="off"
                                        defaultValue={this.state.description}
                                        onChange={(event) => {
                                            this.setState({ description: event.target.value })
                                            //this.state.name = event.target.value
                                        }} />
                                </div>
                            </div>
                            <Row className="d-flex flex-row">
                                <Col sm={12} md={12}>
                                    <PermissionList 
                                    handlerCheked={this.handlerCheck}
                                    isUpdate={true}
                                    />
                                </Col>
                            </Row>
                        </CardBody>
                    </Card>
                </Col>
            </Modal>
        );
    }
}
export default ModalRol;
