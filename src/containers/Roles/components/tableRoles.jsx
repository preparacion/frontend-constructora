//import React from 'react';
import React, { PureComponent } from 'react';
import { Container} from 'reactstrap';
import { fetchAsync, urlRoles } from '../../../helpers/Globals/globals';
import LoadingComponent from "../../../helpers/LoadingComponent"
import CardRolesDraw from "./cardRolesDraw"


class TableRoles extends PureComponent {

    constructor(props) {
        super(props)
        this.state = {
            roles: [],
            loading: true,
        }
    }

    componentDidMount() {
        this.getRoles()
    }
    componentDidUpdate(prevProps,prepState){
        if(prevProps.update!==this.props.update){
            this.getRoles()
        }
    }

    handleClick=(element) =>{
        this.props.ClickOnElement(element)
    }

    getRoles = () => {
        fetchAsync(urlRoles, "", "GET", "")
            .then(
                (result) => {
                    if (result.success === true) {
                        this.setState({
                            roles: result.roles,
                            loading: false
                        })
                    } else {
                        console.log('success false');
                    }
                }
            )
            .catch(

            )
    }

    render() {
        return (
            <Container >
                {this.state.loading ? <LoadingComponent /> :
                    <CardRolesDraw
                        data={this.state.roles}
                        ClickOnElement={this.handleClick} />
                }
            </Container>
        );
    }
}
export default TableRoles;
