import React, { PureComponent } from 'react';
import { Tree } from 'antd';
import { reverseServiceToTree, } from "../../../helpers/Secure/utils"
import { removeItemFromArray } from "../../../helpers/Globals/funciones"
import 'antd/lib/tree/style/index.css';

const { TreeNode } = Tree;

const treeData = [
    {
        title: 'Gestión de usuarios',
        key: 'GU',
        children: [
            {
                title: 'Alumnos',
                key: 'students',
                children: [
                    { title: 'Leer', key: '/students/-get', dependency: ["/search-get", "/payments/student/*-get", "/groups/*-get"] },
                    { title: 'Crear', key: '/students/-post' },
                    { title: 'Editar', key: '/students/-put' },
                    { title: 'Borrar', key: '/students/-delete' },
                    {
                        title: 'Pagos',
                        key: 'payments',
                        children: [
                            { title: 'Leer', key: '/payments/*-get' },
                            { title: 'Crear', key: '/payments/*-post' },
                            { title: 'Editar', key: '/payments/*-put' },
                            { title: 'Borrar', key: '/payments/*-delete' },
                        ]
                    },
                    {
                        title: 'Comentarios',
                        key: 'comments',
                        children: [
                            { title: 'Leer', key: '/comments/*-get' },
                            { title: 'Crear', key: '/comments/*-post' },
                            { title: 'Editar', key: '/comments/*-put' },
                            { title: 'Borrar', key: '/comments/*-delete' },
                        ]
                    },
                    { title: 'Envíar notificaciones', key: '/notifications/student/-post', dependency: ["/templates/*-get"] },
                    { title: 'Incribir Usuario Existente', key: '/groups/?/enroll/-put', dependency: ["/groups/?-get", "/course/levels/"] },
                    { title: 'Cambio de grupo', key: '/students/?/groups/?-put', dependency: ["/course/levels/*-get", "/courses/?/groups-get"] }
                ],
            },
            {
                title: 'Empleados',
                key: 'users',
                children: [
                    { title: 'Leer', key: '/users/-get' },
                    { title: 'Crear', key: '/users/-post', dependency: ["/roles/-get"] },
                    { title: 'Editar', key: '/users/-put', dependency: ["/roles/-get"] },
                    { title: 'Borrar', key: '/users/-delete' },
                ],
            },
            {
                title: 'Roles',
                key: 'roles',
                children: [
                    { title: 'Leer', key: '/roles/*-get' },
                    { title: 'Crear', key: '/roles/*-post' },
                    { title: 'Editar', key: '/roles/*-put' },
                    { title: 'Borrar', key: '/roles/*-delete' },
                ],
            },
        ],
    },
    {
        title: 'Administración escolar',
        key: 'AE',
        children: [
            {
                title: 'Inscripción Usuario Nuevo',
                key: '/students/register-post',
                dependency: ["/students/email/-get", "/students/roles/-get"]
            },
            {
                title: 'Sedes',
                key: 'locations',
                children: [
                    { title: 'Leer', key: '/locations/*-get', dependency: ["/classrooms/location/*-get"] },
                    { title: 'Crear', key: '/locations/*-post' },
                    { title: 'Editar', key: '/locations/*-put' },
                    { title: 'Borrar', key: '/locations/*-delete' }
                ],
            },
            {
                title: 'Cursos',
                key: 'courses',
                children: [
                    { title: 'Leer', key: '/courses/-get', dependency: ["/course/*-get", "/groups/*-get"] },
                    { title: 'Crear', key: '/courses/-post', dependency: ["/course/*-get"] },
                    { title: 'Editar', key: '/courses/-put', dependency: ["/course/*-get"] },
                    { title: 'Borrar', key: '/courses/-delete' }
                ],
            },
            {
                title: 'Grupos',
                key: 'groups',
                children: [
                    { title: 'Leer', key: '/groups/-get', dependency: ["/course/*-get", "/templates/*-get", "/attendance/*-get", "/locations/-get", "/course/levels/"] },
                    { title: 'Crear', key: '/groups/-post', dependency: ["/course/*-get", "/classrooms/*-get", "/locations/-get", "schedules/is/available-post"] },
                    { title: 'Editar', key: '/groups/-put', dependency: ["/course/*-get", "/templates/*-get", "/attendance/*-get", "/locations/-get", "schedules/is/available-post"] },
                    { title: 'Borrar', key: '/groups/-delete' },
                    {
                        title: 'Asistencia',
                        key: 'attendance',
                        children: [
                            { title: 'Ver Asistencia por grupo', key: '/attendance/-get', dependency: ["/course/*-get", "/groups/*-get"] },
                            { title: 'Pasar Asistencia por grupo', key: '/attendance/-post', dependency: ["/course/*-get"] }
                        ]
                    },
                    {
                        title: 'Alumnos por grupo ',
                        key: 'studentsGroup',
                        children: [
                            { title: 'Generar Excel', key: '/groups/?/students/download-get' },
                            { title: 'Cambio Masivo de alumnos', key: '/students/?/groups/*-put', dependency: ["/course/levels/*-get", "/courses/?/groups-get"] },
                            { title: 'Enviár notificación a grupo', key: '/notifications/group/?-post', dependency: ["/templates/?/versions/?-get", "/course/*-get"] }
                        ]
                    },
                ],
            }
        ],
    },
];


// let key= treeData[0].children[0].clildren[2].key

class PermissionList extends PureComponent {
    constructor(props) {
        super(props)
        let allowa = reverseServiceToTree(this.props.Rol ? this.props.Rol.allows : "")
        this.state = {
            autoExpandParent: true,
            expandedKeys: allowa,
            checkedKeys: allowa,
            checkedKeysDefault: allowa,
            selectedKeys: [],
            isUpdate: this.props.isUpdate
        };
    }

    componentDidUpdate(prevProps, prepState) {
        if (this.props.Rol) {
            if (prevProps.Rol._id !== this.props.Rol._id) {
                let allowa = reverseServiceToTree(this.props.Rol ? this.props.Rol.allows : "")
                this.setState({
                    checkedKeys: allowa,
                    expandedKeys: allowa,
                    checkedKeysDefault: allowa,
                    isUpdate: this.props.isUpdate,
                    autoExpandParent: true,
                })
                return true
            } else if (prevProps.isUpdate !== this.props.isUpdate) {
                let allowa = reverseServiceToTree(this.props.Rol ? this.props.Rol.allows : "")
                this.setState({
                    isUpdate: this.props.isUpdate,
                    checkedKeys: allowa,
                    checkedKeysDefault: allowa
                })
                return true
            }
        }
        return true
    }
    buscarChecked = (value1, value2) => {
        let resultado = value1.find(function (element) {
            return element == value2;
        })
        if (resultado)
            return true
        return false
    }
    onExpand = expandedKeys => {
        console.log('onExpand', expandedKeys);
        // if not set autoExpandParent to false, if children expanded, parent can not collapse.
        // or, you can remove all expanded children keys.
        this.setState({
            expandedKeys,
            autoExpandParent: false,
        });
    };

    /*
    * Dependency add will allow to defined when you need to use more service for a screen
    *
    */

    handleArrayAdd = (array) => {
        let temp = []
        for (let i = 0; i < array.length; i++) {
            console.log("array in " + i, array[i])
            Array.prototype.push.apply(temp, this.handleObjectAdd(array[i]));
            console.log(temp)
        }
        return temp
    }

    handleObjectAdd = (object) => {
        let result = []
        if (object.children) {
            let results = this.handleArrayAdd(object.children)
            return results
        } else {
            //Here is were the dependency is manage and add
            result.push(object.key)
            if (object.dependency) {
                // if (!isInArray(this.state.checkedKeys,object.key))
                //hasta aqui agrega las dependency , no revisa si ya fueron agregadas
                Array.prototype.push.apply(result, object.dependency);
            }
            return result
        }
    }



    handleArrayRemove = (array, childs) => {
        let temp = array
        for (let i = 0; i < childs.length; i++) {
            temp = this.handleObjectRemove(array, childs[i]);
        }
        return temp
    }

    handleObjectRemove = (array, object) => {
        let result = []
        if (object.children) {
            let results = this.handleArrayRemove(array, object.children)
            return results
        } else {
            //Here is were the dependency is manage and remove 
            result = removeItemFromArray(array, object.key)
            if (object.dependency) {
                //if has dependency iterate for deleting from original array
                object.dependency.forEach(element => {
                    removeItemFromArray(array, element)
                });
            }
            return result
        }
    }


    onCheck = (checkedKeys, e) => {
        console.log(e)
        let checkedKeysDependency = this.state.checkedKeys
        if (e.checked) {
            let result = this.handleObjectAdd(e.node.props.dataRef)
            Array.prototype.push.apply(checkedKeysDependency, result);
        } else {
            let result = this.handleObjectRemove(checkedKeysDependency, e.node.props.dataRef)
            checkedKeysDependency = result
        }
        if (this.props.handlerCheked)
            this.props.handlerCheked(checkedKeysDependency)
        this.setState((state) => {
            return { checkedKeys: checkedKeysDependency };
        }, this.forceUpdate);
    };

    onSelect = (selectedKeys, info) => {
        console.log('onSelect', info);
        this.setState({ selectedKeys });
    };

    renderTreeNodes = (data) =>
        data.map(item => {
            if (item.children) {
                return (
                    <TreeNode icon={this.buscarChecked(this.state.checkedKeys, item.key) ? <i className="material-icons green_icon icons_card_roles">done</i> : ""} title={item.title} key={item.key} dataRef={item} disabled={item.disabled}>
                        {this.renderTreeNodes(item.children)}
                    </TreeNode>
                );
            }
            return <TreeNode icon={this.buscarChecked(this.state.checkedKeys, item.key) ? <i className="material-icons green_icon icons_card_roles">done</i> : ""} title={item.title} key={item.key} dataRef={item} disabled={item.disabled} />;
        });

    render() {
        return (
            <Tree
                showIcon={!this.state.isUpdate}
                checkable={this.state.isUpdate}
                selectable={false}
                onExpand={this.onExpand}
                expandedKeys={this.state.expandedKeys}
                autoExpandParent={this.state.autoExpandParent}
                onCheck={this.onCheck}
                checkedKeys={this.state.checkedKeys}
                onSelect={this.onSelect}
                selectedKeys={this.state.selectedKeys}
                disabled={this.props.disabled}
            >
                {this.renderTreeNodes(treeData)}
            </Tree>
        );
    }
}
export default PermissionList;
