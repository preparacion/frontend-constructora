import React from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import PropTypes from 'prop-types';
import range from 'lodash.range';

export default class CustPag extends React.PureComponent {
  static propTypes = {
    onChangePage: PropTypes.func.isRequired,
    itemsCount: PropTypes.number.isRequired,
    itemsToShow: PropTypes.number.isRequired,
    pageOfItems: PropTypes.number.isRequired,
  };
  
  static defaultProps = {
    pageSize: 1,
  }

  constructor(props) {
    super(props);
    this.state = { pager: {}, size: this.props.pageSize };
  }

  componentDidMount(){
    this.setPage(this.props.pageOfItems,this.props.itemsCount,this.props.itemsToShow)
  }

  componentDidUpdate(prevProps){
    console.log("ENTER")
    console.log(this.props.itemsCount)
    if (prevProps.itemsCount !== this.props.itemsCount || prevProps.itemsToShow !== this.props.itemsToShow ){
      this.setPage(this.props.pageOfItems,this.props.itemsCount,this.props.itemsToShow)
    }
  }

  setPage = (page,totalSize,size) => {
    console.log("NO RETURN")
    // get new pager object for specified page
    let pag = this.getPager(totalSize, page, size);
    this.setState({
      pager:pag
    })
  };

  setPageOnClick = (page,totalSize,size) => {
    console.log("NO RETURN")
    // get new pager object for specified page
    let pag = this.getPager(totalSize, page, size);
    this.setState({
      pager:pag
    })
    this.props.onChangePage(pag.startIndex,pag.endIndex)
  };

  getPager = (totalItems, curPage, pSize) => {
    console.log("total items:"+totalItems)
    console.log("actual page is:"+curPage)
    console.log("size of page is:"+pSize)
    // default to first page
    const currentPage = curPage || 1;

    // default page size is 10
    const pageSize = pSize || 10;

    // calculate total pages
    const totalPages = Math.ceil(totalItems / pageSize);

    let startPage;
    let endPage;
    if (totalPages <= 10) {
      // less than 10 total pages so show all
      startPage = 1;
      endPage = totalPages;
    } else if (currentPage <= 6) {
      // more than 10 total pages so calculate start and end pages
      startPage = 1;
      endPage = 10;
    } else if (currentPage + 4 >= totalPages) {
      startPage = totalPages - 9;
      endPage = totalPages;
    } else {
      startPage = currentPage - 5;
      endPage = currentPage + 4;
    }

    // calculate start and end item indexes
    const startIndex = (currentPage - 1) * pageSize;
    const endIndex = Math.min((startIndex + pageSize) - 1, totalItems - 1);

    // create an array of pages to ng-repeat in the pager control
    const pages = range(startPage, endPage + 1);

    // return object with all pager properties required by the view
    return {
      totalItems,
      currentPage,
      pageSize,
      totalPages,
      startPage,
      endPage,
      startIndex,
      endIndex,
      pages,
    };
  };

  render() {
    const { itemsCount, itemsToShow, pageOfItems } = this.props;

    return this.props.itemsCount ? (
      <div className="pagination__wrap">
        {(itemsCount <= 1) ? '' :
        <Pagination className="pagination">
          <PaginationItem className="pagination__item" disabled={this.state.pager.currentPage<=1}>
            <PaginationLink
              className="pagination__link pagination__link--arrow"
              type="button"
              onClick={() => this.setPageOnClick(this.state.pager.currentPage - 1,this.props.itemsCount,this.props.itemsToShow)}
            >
               <i className="material-icons pagination__link-icon">keyboard_arrow_left</i>

            </PaginationLink>
          </PaginationItem>
          {this.state.pager.pages.map(page =>
              (
                <PaginationItem
                  className="pagination__item"
                  key={page}
                  active={this.state.pager.currentPage === page}
                >
                  <PaginationLink
                    className="pagination__link"
                    type="button"
                    onClick={() => this.setPageOnClick(page,this.props.itemsCount,this.props.itemsToShow)}
                  >
                    {page}
                  </PaginationLink>
                </PaginationItem>
              ))
          }
          <PaginationItem className="pagination__item" disabled={this.state.pager.currentPage>=this.state.pager.endPage}>
            <PaginationLink
              className="pagination__link pagination__link--arrow"
              type="button"
              onClick={() => this.setPageOnClick(this.state.pager.currentPage + 1,this.props.itemsCount,this.props.itemsToShow)}
            >
              <i className="material-icons pagination__link-icon">keyboard_arrow_right</i>
            </PaginationLink>
          </PaginationItem>
        </Pagination>
          }
        <div className="pagination-info">
          <span>Mostrando resultados {`${this.state.pager.startIndex+1} `}
              al {this.state.pager.endIndex+1} de {itemsCount} encontrados
          </span>
        </div>
      </div>
    ) : <div />;
  }
}

/* eslint-enable */
