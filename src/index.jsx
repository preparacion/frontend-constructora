import React from 'react';
import { render } from '@hot-loader/react-dom';
import App from './containers/App/App';

render(
  <App />,
  document.getElementById('root'),
);
