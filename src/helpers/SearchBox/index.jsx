import React, { PureComponent } from 'react';
import {Row} from "reactstrap";
class SearchBox extends PureComponent {

    constructor(props) {
        super(props)
        this.state = {
            input: ""
        }
    }

    //Auxiliar Function
    handleClick = () => {
        this.props.busqueda(this.state.input.value)
    };

    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.props.busqueda(this.state.input.value)
        }

    }
    render() {
        return (
            <div className="FlexSearchBox">
                <input
                    placeholder="Buscar un alumno"
                    className="form-control searchBoxTable"
                    ref={n => this.state.input = n}
                    type="text"
                    //onClick={handleClick}
                    onKeyPress={this._handleKeyPress}
                />
                <i className="material-icons icon-searchBar">search</i>
            </div >
        )
    }
}
export default SearchBox