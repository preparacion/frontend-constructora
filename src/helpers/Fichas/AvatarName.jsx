import React from 'react';
import { Col, Row, Button, } from 'reactstrap';
import 'antd/dist/antd.css';
import { Avatar } from 'antd';


const AvatarName = ({ name, secondName = "", aMaterno, aPaterno }) => {
  return (
    <Row>
      <Col xs="4">
        <Avatar style={{ backgroundColor: "black" }}
          size={65} >{name[0].toUpperCase()}
        </Avatar>
      </Col>
      <Col xs="8">
        <p className="fichaNameTitle">{`${name}`} {secondName !== "" ? secondName : null}</p>
        <p className="apellidosFont">{`${aPaterno} ${aMaterno}`}</p>
      </Col>
    </Row>
  )
}
export default AvatarName