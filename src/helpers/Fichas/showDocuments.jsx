import React from 'react';
import {
  Card, ButtonGroup, Container, CardBody, CardTitle, CardHeader, CardText
} from 'reactstrap';
import SetFechaTable from '../Tables/fechaTables';
import EmptyComponent from "../Empty/EmptyComponent"

const listDocuments = (lista) => {
  console.log("listDocuments")
  console.log(lista)
  console.log("listDocuments")
  if (lista.length > 0) {
    return lista.map((data, index) => {
      return (
        <div key={index}>
          <Card>
            <CardHeader>Subido el: {SetFechaTable(data.date)}</CardHeader>
            <CardBody className=" cardDownload">
              <a href={data.url}>Descargar {data.name}</a>
            </CardBody>
          </Card>
        </div>
      )
    })
  } else {
    return (
      <Card className="cardDownload">
        <CardBody>
          <EmptyComponent message="Aún no hay docuemntos agregados" />
        </CardBody>
      </Card>)
  }

}

export default listDocuments