export const month = new Array();
month[0] = "Enero";
month[1] = "Febrero";
month[2] = "Marzo";
month[3] = "Abril";
month[4] = "Mayo";
month[5] = "Junio";
month[6] = "Julio";
month[7] = "Agosto";
month[8] = "Septiembre";
month[9] = "Octubre";
month[10] = "Noviembre";
month[11] = "Diciembre";

export const semana = new Array();
semana[0] = "Lunes";
semana[1] = "Martes";
semana[2] = "Miercoles";
semana[3] = "Jueves";
semana[4] = "Viernes";
semana[5] = "Sabado";
semana[6] = "Domingo";

export const semanaLetra = new Array();
semanaLetra[0] = "l";
semanaLetra[1] = "m";
semanaLetra[2] = "w";
semanaLetra[3] = "j";
semanaLetra[4] = "v";
semanaLetra[5] = "s";
semanaLetra[6] = "d";

export const semanaDias = new Array();
semanaDias[0] = "Lu";
semanaDias[1] = "Ma";
semanaDias[2] = "Mi";
semanaDias[3] = "Ju";
semanaDias[4] = "Vi";
semanaDias[5] = "Sa";
semanaDias[6] = "Do";

export const COLOR_TITLE_TABLES = "#123878"

export const FAMILIAS = [
  {
    clave: 'MOA',
    descripcion: "mano de obra aluminio"
  },
  {
    clave: 'MOC',
    descripcion: "mano de obra cristal"
  },
  {
    clave: 'MOP',
    descripcion: "mano de obra panel"
  },
  {
    clave: 'MOH',
    descripcion: "mano de obra herrería"
  },
  {
    clave: 'MOV',
    descripcion: "mano de obra varios"
  },
  {
    clave: 'MOS',
    descripcion: "mano de obra sellos y empaques"
  },
]

export const UNIDADES = ["M2", "ML", "PZA", "LOTE", "KG", "SEM"]