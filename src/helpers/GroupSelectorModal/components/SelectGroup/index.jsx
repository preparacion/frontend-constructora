import React, { PureComponent } from 'react';
import { fetchAsync, urlCourses } from "../../../Globals/globals"
import { newSchedules } from '../../../../helpers/Globals/funciones';
import {
    Col, Row, Nav, NavItem, NavLink,
    TabContent, TabPane, Spinner,
} from 'reactstrap';
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import SortTable from '../../../Tables/sortTable';

const headerSortingStyle = { backgroundColor: '#D6DBDF' };

class SelectGroup extends PureComponent {
    static propTypes = {
        // handleSubmit: PropTypes.func.isRequired,
    };
    constructor(props) {
        super(props);
        this.state = {
            idGroup: "",
            dataSelect: [],
            dataSelectGroup: [],
        };
        this.loadCursosInfo(this.props.idCourse);
    }

    stringHour = (_hour) => {
        var hour = _hour.toString();
        var lengthHour = hour.length;
        var substringminuts = hour.substring((Number(lengthHour) - 2), Number(lengthHour));
        var substringHour = hour.substring(0, (Number(lengthHour) - 2));
        if (substringHour.length == 1) {
            substringHour = '0' + substringHour.toString();
        }
        return substringHour.toString() + ':' + substringminuts.toString();
    }



    dateToIsoString(_date) {
        _date = new Date(_date.substring(0, 10))
        let nextDay = new Date(_date)
        nextDay.setDate(_date.getDate() + 1)
        return (nextDay.getDate()) + '/' + (nextDay.getMonth() + 1) + '/' + nextDay.getFullYear()
    }

    rangeDays = (row) => {
        let result = this.stringHour(row.startHour) + " - " + this.stringHour(row.endHour)
        return result
    }

    selectGroup(data) {
        this.props.setSelected(data);
    };

    rowEvents = {
        onClick: (e, row, rowIndex) => {
            this.props.setGroupId(row._id)
            this.setState({
                idGroup: row._id
            })
        }
    };

    rowColor = (cell, row) => {
        let rowStyle = { backgroundColor: '#f6f6ff' };
        if (this.state.idGroup == cell._id) {
            return rowStyle
        }
        if (this.props.actualId == cell._id) {

        }
    }

    rowClassColor = (cell, row) => {
        let rowClass = "rowTableSelected"
        if (this.state.idGroup == cell._id) {
            return rowClass
        }
        if (this.props.actualId == cell._id) {

        }
    }

    diasHoras = (cell, row) => {
        const newArray = cell.map(a => ({ ...a }));
        return newSchedules(newArray)
    }

    sortIncrementarDate(dataArray) {
        let sortedArray = dataArray.sort(function (a, b) {
            return new Date(a.startDate) - new Date(b.startDate)
        });
        return sortedArray
    }

    async loadCursosInfo(_id) {
        await this.setState({
            visibleLoading: 'initial',
            visibleTable: 'none',
        });
        await fetchAsync(urlCourses + _id + "/groups", "", "GET", "")
            .then(
                (result) => {
                    if (result.success) {
                        let arrayOrder = [];
                        for (var i = 0; i < result.groups.length; i++) {
                            if (result.groups[i].available > 0) {
                                arrayOrder.push(result.groups[i]);
                            }
                        }
                        this.setState({
                            dataSelect: this.sortIncrementarDate(arrayOrder)
                        });
                    }
                }
            )
            .catch(
                (reason) => {
                    console.log(reason.message)
                }
            );
        // [DEBUG]
        await this.setState({
            visibleLoading: 'none',
            visibleTable: 'initial',
        });
    }

    render() {
        let hiddenRows = []
        if (this.props.currentGroup){
            hiddenRows.push(this.props.currentGroup)
        }
        const columns = [
            {
                dataField: 'name',
                text: 'Nombre del grupo',
                sort: true,
                headerAlign: 'center',
                align: 'center',
                sortCaret: (order, column) => {
                    return <SortTable order={order} colum={column} />
                },
                headerSortingStyle,
            },
            {
                dataField: 'startDate',
                text: 'Fecha de inicio',
                headerAlign: 'center',
                align: 'center',
                sortCaret: (order, column) => {
                    return <SortTable order={order} colum={column} />
                },
                headerSortingStyle,
                formatter: this.dateToIsoString,
            },
            {
                dataField: 'schedules',
                text: 'Dia(s) y Horarios',
                sort: true,
                headerAlign: 'center',
                align: 'center',
                sortCaret: (order, column) => {
                    return <SortTable order={order} colum={column} />
                },
                headerSortingStyle,
                formatter: this.diasHoras,
            },
            {
                dataField: 'locationInfo.name',
                text: 'Sede',
                sort: true,
                headerAlign: 'center',
                align: 'center',
                sortCaret: (order, column) => {
                    return <SortTable order={order} colum={column} />
                },
                headerSortingStyle,
            },
        ];
        return (
            <Col sm={12} md={12} >
                <ToolkitProvider
                    keyField="_id"
                    data={this.state.dataSelect}
                    columns={columns}
                >
                    {
                        props => (
                            <div>
                                <BootstrapTable
                                    keyField="_id"
                                    bordered={false}
                                    rowEvents={this.rowEvents}
                                    rowStyle={this.rowColor}
                                    rowClasses={this.rowClassColor}
                                    hiddenRows={ hiddenRows }
                                    {...props.baseProps}
                                    noDataIndication={<div style={{ textAlign: "center", color: "#2471A3" }}><Spinner type="grow" style={{ width: '4rem', height: '4rem' }} /></div>}
                                />
                            </div>
                        )
                    }
                </ToolkitProvider>
            </Col>
        );
    }
}

export default SelectGroup;
