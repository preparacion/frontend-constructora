import React, { PureComponent } from 'react';
import { fetchAsync, urlCourseLevels } from "../../../Globals/globals"
import {
    Col, Row, Nav, NavItem, NavLink,
    TabContent, TabPane, Spinner,
} from 'reactstrap';
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import classnames from 'classnames';
import SortTable from '../../../Tables/sortTable';

const idMedia = "5ce2b9b2e277d4309b5ed0f6"
const idSuperior = "5ce2b9b2e277d4309b5ed0f5"
const headerSortingStyle = { backgroundColor: '#D6DBDF' };

class SelectCourse extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            activeTab: '1',
            activePhase: "1",
            idCourse: "",
            dataCursosSuperior: [],
            dataCursosMedia: [],
        };
        this.getCursosLevelMedia(idMedia)
        this.getCursosLevelSuperior(idSuperior)
    }

    getCursosLevelMedia(id) {
        fetchAsync(urlCourseLevels + id + "/courses", "", "GET", "")
            .then(
                (data) => {
                    if (data.success === true) {
                        this.setState({
                            dataCursosMedia: data.courses,
                        });
                    }
                }
            ).catch((reason) => { console.log(reason) });
    }

    //metodo que trae todos los cursos de superior
    getCursosLevelSuperior(id) {
        fetchAsync(urlCourseLevels + id + "/courses", "", "GET", "")
            .then(
                (data) => {
                    if (data.success === true) {
                        this.setState({
                            dataCursosSuperior: data.courses,
                        });
                    }
                }
            ).catch((reason) => { console.log(reason) });
    }

    //funcion para el cambio entre tabs
    toggle = (tab) => {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab,
            });
        }
    };

    //Funcion que enumera las filas de la lista (DEPRECATED EN EL NUEVO DISEÑO)
    generaNum = (cell, row, rowIndex) => {
        if (row) {
            return (rowIndex + 1)
        } else {
            return ""
        }
    }

    //genera el dot para activo o inactivo
    disponible = (cell, row) => {
        if (cell == true) {
            return (<Row className={"justify-content-center"}><i className="material-icons ficha_icons green_icon">check_circle_outline</i></Row>)
        } else {
            return (<Row className={"justify-content-center"}><i className="material-icons ficha_icons red_icon">cancel</i></Row>)
        }
    }

    rowEvents = {
        onClick: (e, row, rowIndex) => {
            this.props.setCourseId(row._id)
            this.setState({
                idCourse: row._id
            })
        }
    };
    //TODO: Cambiar el color a una variable en los estilos
    rowColor = (cell, row) => {
        let rowStyle = { backgroundColor: '#D0D3D4' };
        if (this.state.idCourse == cell._id) {
            return rowStyle
        }
    }

    render() {
        const columns = [
            {
                dataField: 'name',
                text: 'Cursos',
                sort: true,
                headerAlign: 'center',
                align: 'center',
                sortCaret: (order, column) => {
                    return <SortTable order={order} colum={column} />
                },
                headerSortingStyle,
            },
            {
                dataField: 'active',
                text: 'Estado',
                sort: true,
                headerAlign: 'center',
                align: 'center',
                formatter: this.disponible,
            },
        ];

        const defaultSorted = [{
            dataField: 'active', 
            order: 'desc'
          }];
        return (
            <Col xs={12} sm={12} md={12} lg={12}>
                <div className="tabs tabs--vertical tabs--vertical-colored ">
                    <div className="tabs__wrap">
                        <Nav tabs>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === '1' })}
                                    onClick={() => {
                                        this.toggle('1');
                                    }}
                                >
                                    Media Superior
                                        </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === '2' })}
                                    onClick={() => {
                                        this.toggle('2');
                                    }}
                                >
                                    Superior
                                        </NavLink>
                            </NavItem>

                        </Nav>

                        <TabContent activeTab={this.state.activeTab} className="mt-1">
                            <TabPane tabId="1">
                                <Col sm={12} md={12}>
                                    <ToolkitProvider
                                        keyField="_id"
                                        data={this.state.dataCursosMedia}
                                        columns={columns}
                                    >
                                        {
                                            props => (
                                                <div className="scrollCambioMasivoComponente1">
                                                    <BootstrapTable
                                                        keyField="_id"
                                                        bordered={false}
                                                        rowEvents={this.rowEvents}
                                                        rowStyle={this.rowColor}
                                                        defaultSorted={defaultSorted}
                                                        {...props.baseProps}
                                                        noDataIndication={<div style={{ textAlign: "center", color: "#2471A3" }}><Spinner type="grow" style={{ width: '4rem', height: '4rem' }} /></div>}
                                                    />
                                                </div>
                                            )
                                        }
                                    </ToolkitProvider>
                                </Col>
                            </TabPane>
                            <TabPane tabId="2">
                                <Col sm={12} md={12}>
                                    <ToolkitProvider
                                        keyField="_id"
                                        data={this.state.dataCursosSuperior}
                                        columns={columns}
                                    >
                                        {
                                            props => (
                                                <div className="scrollCambioMasivoComponente1">
                                                    <BootstrapTable
                                                        keyField="_id"
                                                        bordered={false}
                                                        rowEvents={this.rowEvents}
                                                        rowStyle={this.rowColor}
                                                        defaultSorted={defaultSorted}
                                                        {...props.baseProps}
                                                        noDataIndication={<div style={{ textAlign: "center", color: "#2471A3" }}><Spinner type="grow" style={{ width: '4rem', height: '4rem' }} /></div>}
                                                    />
                                                </div>
                                            )
                                        }
                                    </ToolkitProvider>
                                </Col>
                            </TabPane>
                        </TabContent>
                    </div>
                </div>
            </Col>
        );
    }
}

export default SelectCourse
