import React, { PureComponent } from 'react';
import { Col, ButtonToolbar, Button, Row } from 'reactstrap';
import Modal from 'react-responsive-modal';
import SelectCourse from "./components/SelectCourse"
import SelectGroup from "./components/SelectGroup"
// import LoadingComponent from '../../../../helpers/LoadingComponent'
const defaulState = {
    loading: true,
    targetId: "",
    courseId: "",
    currentStep: "1"
}
class GroupSelectorModal extends PureComponent {

    constructor(props) {
        super(props);
        this.state = defaulState;
    }

    componentDidMount() {
        // this.studentsByGroup(this.props.grupo._id)
    }

    onCloseModal = () => {
        this.props.closeModal()
        this.setState(defaulState)
    }

    onClickButton = () => {
        if (this.state.currentStep === "1") {
            this.setState({
                currentStep: "2"
            })
        } else {
            this.props.selectTarget(this.state.targetId)
            this.setState(defaulState)
        }
    }

    setTargetId(id) {
        this.setState({
            targetId: id
        })
    }

    setCourseId(id) {
        this.setState({
            courseId: id
        })
    }

    selectTargetId() {
        this.props.selectTarget(this.state.targetId)
    }

    render() {
        
        return (
            <Modal
                open={this.props.valueModal}
                onClose={this.onCloseModal}
                center={true}
                closeOnEsc={false}
                showCloseIcon={false}
                closeOnOverlayClick={false}
                
            >
                <div>
                    <Row className="marginTableInfoG ">
                        <Col sm={12} md={5}>
                            <p className="titleInfoTable">Selección de Curso</p>
                        </Col>
                        <Col sm={12} md={7} className="marginButtonsChange">
                            <Button size="sm" className="btn btn-secondary" style={{ marginRight: "15px" }}
                                onClick={() => this.onCloseModal()}>
                                <p>Cerrar</p>
                            </Button>
                            <Button size="sm" className="btn btn-secondary disable" 
                                onClick={() => this.onClickButton()} disabled={this.state.currentStep === "1" ? !this.state.courseId : !this.state.targetId}>
                                <p>{this.state.currentStep === "1" ? "Siguiente" : "Seleccionar"}</p>
                            </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} sm={12} md={12} className="scrollCambioMasivoComponente">
                            {this.state.currentStep === "1" ?
                                <SelectCourse
                                    setCourseId={this.setCourseId.bind(this)}
                                />
                                :
                                <SelectGroup
                                    idCourse={this.state.courseId}
                                    setGroupId={this.setTargetId.bind(this)}
                                    currentGroup={this.props.currentGroup}
                                />
                            }
                        </Col>
                    </Row>
                </div>
            </Modal >
        )
    }
}
export default GroupSelectorModal
