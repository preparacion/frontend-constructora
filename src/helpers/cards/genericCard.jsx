//import React from 'react';
import React, { PureComponent } from 'react';
import { Col, Row, Card, CardBody } from 'reactstrap';

class CardDraw extends PureComponent {

  createTable = (elements, type) => {
    let table = []
    console.log(elements.length)

    if (type === "salones") {
      for (let i = 0; i < elements.length; i++) {
        table.push(
          <Col xs={12} sm={6} md={6} lg={4} xl={3} onClick={() => this.handleClick(elements[i])}>
            
            <Card>
              <CardBody className="dashboard__card-widget">
                <div className="card__title">
                  <h5 className="bold-text">Nombre salón: {elements[i].name}</h5>
                  <h5 className="subhead">Capacidad:{elements[i].capacity}</h5>
                </div>
              </CardBody>
            </Card>
          </Col>
        )
      }
    }else{

      /*for (let i = 0; i < elements.length; i++) {
        table.push(
          <Col xs={12} sm={6} md={6} lg={4} xl={3} onClick={() => this.handleClick(elements[i])}>
            <Card>
              <CardBody className="dashboard__card-widget">
                <div className="card__title">
                  <h5 className="bold-text">{elements[i].name}</h5>
                  <h5 className="subhead">Usuarios:{elements[i].totalUsers}</h5>
                </div>
                <div className="dashboard__total dashboard__total--area">
                  <p>{elements[i].description}</p>
                </div>
              </CardBody>
            </Card>
          </Col>
        )
      }*/

    }
    
    return table
  }

  handleClick(value) {
    this.props.ClickOnElement(value)
  }

  render() {
    return (
      <Row>
        {this.createTable(this.props.data,this.props.type)}
      </Row>
    )
  }
}

export default CardDraw;