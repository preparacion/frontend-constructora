import React from 'react';
import { Col, Spinner } from 'reactstrap';

const LoadingComponent = () => (
    <Col className="text-center">
        <Spinner type="grow" style={{ width: '4rem', height: '4rem' }} />
    </Col>
)


export default LoadingComponent