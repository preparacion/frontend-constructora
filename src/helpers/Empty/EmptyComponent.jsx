import React from "react"
import { Empty } from 'antd';
import 'antd/lib/empty/style/index.css';

const EmptyComponent = (props) => {
    let { message } = props
    return (
        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={message} />
    )
}

export default EmptyComponent



