import { createCanBoundTo } from '@casl/react';
import {ability} from './abilities';

export default createCanBoundTo(ability);