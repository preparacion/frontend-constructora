import { AbilityBuilder, Ability } from '@casl/ability'
// TODO
// https://github.com/stalniy/casl/tree/master/packages/casl-react

// let users
// let template=[]
// if( users =  JSON.parse(localStorage.getItem("dataLoginStudent"))){
//    template = users.allows
// }

//  function dynamicCan (canFunction){
//    for (let i = 0 ; i<template.length;i++){
//     canFunction(template[i].actions,template[i].subject)
//    }
// }

// export default AbilityBuilder.define(can => {
//   Ability.addAlias('post', 'create')
//   Ability.addAlias('put', 'update')
//   dynamicCan(can)
// })
let users
let template = []
if (users = JSON.parse(localStorage.getItem("dataLoginStudent"))) {
  template = users.allows
}
Ability.addAlias('post', 'create')
Ability.addAlias('put', 'update')

function dynamicCan(canFunction) {
  if (template != undefined) {
    for (let i = 0; i < template.length; i++) {
      canFunction(template[i].actions, template[i].subject)
    }
  }
}

let ability = AbilityBuilder.define(can => {
  Ability.addAlias('post', 'create')
  Ability.addAlias('put', 'update')
  dynamicCan(can)
})

function forceUpdate(data) {
  ability.update(data)
}

export {
  ability,
  forceUpdate
};