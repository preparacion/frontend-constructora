
//agrega dos puntos a la hora 0930 =  09:30
const formatoHora = (hora) => {
  let uno = hora.slice(0, 2);
  let dos = hora.slice(2, 4);
  return uno + ":" + dos
}

export const removeItemFromArray = (array, item) => {
  var index = array.indexOf(item);
  if (index > -1) {
    array.splice(index, 1);
    return array
  }
  return array
}


export const isInArray = (array, item) => {
  var index = array.indexOf(item);
  if (index > -1) {
    return true
  }
  return false
}

//regresa una fecha en formato día mes año
export const showDate = (fecha) => {
  let date = new Date(fecha.split("T")[0])
  return date.toLocaleString().split(" ")[0]
}

export const getActualDate = () => {
  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth() + 1;
  var yyyy = hoy.getFullYear();

  return dd + '-' + mm + '-' + yyyy;
}