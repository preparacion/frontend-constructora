// =================================================
// === dev
// =================================================
const urlBase = 'https://dev.api.preparacionipn.mx';

// =================================================
// === Endpoints
// =================================================
const urlPeople = urlBase + '';
const urlLogin = urlBase + '/api/login';
//const urlLoginStudent = urlBase+'/api/st/login';
const urlLoginStudent = urlBase + '/api/login';
const urlLoginStudentRecover = urlBase + '/api/st/pass/recover';
const urlLoginUserRecover = urlBase + '/api/unpr/users/pass/recover';
const urlPermissions = urlBase + '/api/permissions/';
const urlUsers = urlBase + '/api/users/';
const urlMe = urlBase + '/api/me';
const urlRoles = urlBase + '/api/roles/';


// =================================================
// =================================================
const urlBaseConstructora = 'http://heg.bluedotmx.com/api/';

// const urlBaseConstructora = 'http://api.heg.com.mx/api/';

//empleados urls
const urlEmployees = urlBaseConstructora + 'employees/';
const urlEmployeesComment = urlBaseConstructora + 'employees/addComment/';
const urlCategories = urlBaseConstructora + 'categories/';
const urlConcepts = urlBaseConstructora + 'concepts/';
const urlObras = urlBaseConstructora + 'obras/';
const urlContratistas = urlBaseConstructora + 'contratistas/';
const urlCatalog = urlBaseConstructora + 'catalog/';
const urlEstimation = urlBaseConstructora + 'estimaciones/';
const urlFiles = urlBaseConstructora + 'file/';



async function fetchAsync(_url, _body, _method, _header, _token) {

  let auth = '';
  if (localStorage.getItem('authStudent') === 'true') {
    let dataUser = JSON.parse(localStorage.getItem('dataLoginStudent'));
    if (dataUser != null) {
      auth = 'Bearer ' + dataUser.token
    }
  }

  if (_token !== undefined) {
    auth = _token;
  }

  // console.log('===== token ====: ', auth);

  let headerC = new Headers({
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Headers': 'Origin, X-Requested-With',
    // "Content-Type": "Accept",
    'Authorization': auth,
  });
  var data;

  if (_body === '') {
    let options = {
      method: _method,
      headers: headerC
    }
    data = await fetch(_url, options)
      .then(response => response.json())
      .then(data => data)
  } else {
    let options = {
      method: _method,
      headers: headerC,
      body: _body,
    }
    data = await fetch(_url, options)
      .then(response => response.json())
      .then(data => data)
  }

  return data;
}



async function fetchAsyncPublic(_url, _body, _method, _header, _token) {

  let headerC = new Headers({
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Headers': 'Origin, X-Requested-With',
  });
  var data;
  if (_body === '') {
    data = await (await fetch(_url, {
      method: _method,
      headers: headerC
    })).json();
  } else {
    data = await (await fetch(_url, {
      method: _method,
      headers: headerC,
      body: _body,
    })).json();
  }

  return data;
}


async function fetchAsyncDownload(_url, _body, _method, _header, _token) {

  let auth = '';
  if (localStorage.getItem('auth') === 'true') {

    let dataUser = JSON.parse(localStorage.getItem('dataLogin'));
    if (dataUser != null) {
      auth = 'Bearer ' + dataUser.token
    }

  }

  if (_token !== undefined) {
    auth = _token
  }

  let headerC = new Headers({
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'text',
    "Access-Control-Allow-Headers": "Origin, X-Requested-With",
    // "Content-Type": "Accept",
    'Authorization': auth,
  });

  let data;
  if (_body === '') {
    data = await (await fetch(_url, {
      method: _method,
      headers: headerC,
    }));
  }
  return data;
}

async function uploadData(_url, _body, _method, _header, _token) {
  let auth = '';
  if (localStorage.getItem('authStudent') === 'true') {
    let dataUser = JSON.parse(localStorage.getItem('dataLoginStudent'));
    if (dataUser != null) {
      auth = 'Bearer ' + dataUser.token
    }
  }

  if (_token !== undefined) {
    auth = _token;
  }

  let data = await (await fetch(_url, {
    method: _method,
    headers: {
      'Access-Control-Allow-Origin': '*',
      "Access-Control-Allow-Headers": "Origin, X-Requested-With",
      "processData": false,
      "contentType": false,
      "mimeType": "multipart/form-data",
      "Authorization": auth
    },
    body: _body,
    progress: (e) => { console.log(`Progress: ${e.loaded / e.total}%`) }
  }))
  return data;
}


function uploadData2(_url, _body, _method, onProgress, index) {
  let auth = '';
  if (localStorage.getItem('authStudent') === 'true') {
    let dataUser = JSON.parse(localStorage.getItem('dataLoginStudent'));
    if (dataUser != null) {
      auth = 'Bearer ' + dataUser.token
    }
  }
  let xhr = new XMLHttpRequest()
  let returnPromise = new Promise((res, rej) => {
    xhr.open(_method, _url)
    xhr.setRequestHeader('Access-Control-Allow-Origin', "*")
    xhr.setRequestHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, content-type")
    xhr.setRequestHeader("processData", false)
    // xhr.setRequestHeader("contentType", "multipart/form-data")
    xhr.setRequestHeader("mimeType", "multipart/form-data")
    xhr.setRequestHeader("Access-Control-Allow-Credentials",true)
    xhr.setRequestHeader("Authorization", auth)


    xhr.onload = e => res(e.target.responseText);

    xhr.onerror = rej

    xhr.upload.onprogress = function (event) {
      if (event.lengthComputable) {
        var percent = Math.round((event.loaded / event.total) * 100)
        onProgress(percent, index)
        // console.log(percent)
      }
    }
    xhr.send(_body)
  })
  returnPromise.abort = () => {
    xhr.abort()
  }

  return returnPromise

}


export {


  fetchAsync,
  fetchAsyncPublic,
  uploadData,
  uploadData2,

  urlPeople,
  urlLogin,
  urlLoginStudent,
  urlLoginStudentRecover,
  urlLoginUserRecover,
  urlPermissions,
  urlUsers,
  urlMe,
  urlRoles,
  urlBase,
  fetchAsyncDownload,
  
  //contructora
  urlConcepts,
  urlEmployees,
  urlEmployeesComment,
  urlCategories,
  urlObras,
  urlContratistas,
  urlCatalog,
  urlEstimation,
  urlFiles,

};
