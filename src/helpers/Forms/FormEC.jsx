import React, { useState, useEffect } from 'react';
import { Col, Row, Card, CardBody, ButtonToolbar, Button } from 'reactstrap';
import { Header } from './Header';
import Modal from 'react-responsive-modal';
import { Select } from 'antd';
import 'antd/lib/date-picker/style/index.css';
import { showMessageWarning, } from '../MessageAndNotificationUtils/';
import { Categoria, Contratista, Obra } from './SelectsValues';
import Can from '../Secure/Can';
import ModalConfirmation from '../../containers/Modal/ModalConfirmation';
import { fetchAsync, urlContratistas, } from '../../helpers/Globals/globals';

const { Option } = Select;

//formulario para empleados y Contratistas
function FormEC({ titulo, openAddModal, valueAddModal, categories,
  contratistas, obras, addEmployee,
  editarInfo = undefined, editEmployee, deleteEmployIndex }) {

  const inicialState = {
    name: "",
    secondName: "",
    aPaterno: "",
    aMaterno: "",
    curp: "",
    noSocial: "",
    category: "",
    contratista: "",
    obra: "",
    categoryName: "",
    status: true,
  }

  //estados
  const [empleado, setEmpleado] = useState(inicialState)
  const [bloquear, setBloquear] = useState(false)
  const [eliminar, setEliminar] = useState(false)
  const [esContratista, setEsContratista] = useState(false)
  const [esContratistaEditar, setEsContratistaEditar] = useState(false)
  const [allContratistas, setAllContratistas] = useState([])

  useEffect(() => {

    // console.log("useEffectMOdalll")
    // console.log(editarInfo)

    if (editarInfo != undefined) {

      setEmpleado({
        name: editarInfo.name,
        secondName: editarInfo.secondName,
        aPaterno: editarInfo.aPaterno,
        aMaterno: editarInfo.aMaterno,
        curp: editarInfo.curp,
        noSocial: editarInfo.noSocial,
        category: editarInfo.category,
        contratista: editarInfo.contratista != undefined ? editarInfo.contratista : "",
        obra: editarInfo.obra != undefined ? editarInfo.obra : "",
        status: true,
      })

      //validacion para seber si es un contratista o no
      if (editarInfo.category) {
        if (Categoria(categories, editarInfo.category).toLowerCase().includes("contratista") === true) {
          setEsContratistaEditar(true)
          setEsContratista(true)
        }
        return
      } else {
        setEsContratistaEditar(true)
        setEsContratista(true)
        return
      }

    } else {
      setEmpleado(inicialState)//reiniciar losvalores de los estaods
      setBloquear(false)
    }
  }, [editarInfo])

  useEffect(() => {
    getAllContratistas()
  }, [])

  //funcion para traerse todos los Contratistas
  const getAllContratistas = () => {

    return fetchAsync(urlContratistas, "", "GET", "")
      .then((result) => {
        setAllContratistas(result)
      }).catch((reason) => {
        console.log(reason.message)
      });
  }

  //cerrar el modal
  const onCloseModal = () => {
    setEmpleado(inicialState)//reiniciar losvalores de los estaods
    openAddModal(false)
    setBloquear(false)
  }

  //INPUTS//
  const handleChangeInputs = e => {
    setEmpleado({
      ...empleado, //recordar copiar el objeto actual para no eliminar las otras propiedades
      [e.target.name]: e.target.value
    })
  }

  //Selects////////
  //maneja el evento del selector de categoria
  //validamos si es contratista para bloquera campos
  const handleChangeCategorie = (value) => {
    //console.log(`selected ${value}`);

    if (value.toLowerCase().includes("contratista") === true) {
      setEmpleado({
        ...empleado,
        category: value.split("_")[1],
        categoryName: value.split("_")[0],
        curp: '',
        contratista: '',
        obra: ''
      })

      setEsContratista(true)
    } else {
      setEmpleado({
        ...empleado,
        category: value.split("_")[1],
        categoryName: value.split("_")[0]
      })
      setEsContratista(false)
    }

  }

  //muestra la lista de categorias
  const showOptions = () => {
    return categories.map((value) => {
      return <Option key={value._id} value={value.name + "_" + value._id}>{value.name}</Option>
    })
  }

  //maneja el evento del selector de Contratista
  const handleChangeContratista = (value) => {
    setEmpleado({
      ...empleado,
      contratista: value.split("_")[1],
    })
  }

  //muestra la lista de Contratistas
  const showContratista = () => {
    return allContratistas.map((value) => {
      return <Option key={value._id} value={value.name + "_" + value._id}>{value.name} {value.aPaterno} {value.aMaterno}</Option>
    })
  }

  //maneja el evento del selector de Obras
  const handleChangeObras = (value) => {
    //console.log(value.split("_")[1]);
    setEmpleado({
      ...empleado,
      obra: value.split("_")[1]
    })
  }

  //muestra la lista de Obras
  const showObras = () => {
    return obras.map((value) => {
      return value.contratistas.map((data) => {
        if (data._id === empleado.contratista) {
          return <Option key={value._id} value={value.name + "_" + value._id}>{value.name}</Option>
        }
      })
    })
  }

  //metodo que valida y envia el empleado a crear o editar
  const hanldleSubmit = () => {

    if (empleado.name == null || empleado.name == "") {
      showMessageWarning("Atención! Debes ingresar un nombre ", 2)
    } else if (empleado.aPaterno == null || empleado.aPaterno == "" ||
      empleado.aMaterno == null || empleado.aMaterno == "") {
      showMessageWarning("Atención! Debes ingresar un apellido materno y paterno", 2)
    } else if (empleado.noSocial == null || empleado.noSocial == "") {
      showMessageWarning("Atención! Debes ingresar un numero del seguro social", 2)
    } else if (empleado.category == null || empleado.category == "") {
      showMessageWarning("Atención! Debes seleccionar una categoria", 2)
    } else if (Categoria(categories, empleado.category).toLowerCase().includes("contratista") !== true && (empleado.obra === "" || empleado.contratista === "")) {
      showMessageWarning("Atención! Debes seleccionar un contratista y una obra", 2)
    }
    else {
      //validamos si se editara o se creare un nuevo registro
      if (editarInfo != undefined) {

        if (editarInfo.name == null || editarInfo.name == "") {
          showMessageWarning("Atención! Debes ingresar un nombre ", 2)
        } else if (editarInfo.aPaterno == null || editarInfo.aPaterno == "" ||
          editarInfo.aMaterno == null || editarInfo.aMaterno == "") {
          showMessageWarning("Atención! Debes ingresar un apellido materno y paterno", 2)
        } else if (editarInfo.noSocial == null || editarInfo.noSocial == "") {
          showMessageWarning("Atención! Debes ingresar un numero del seguro social", 2)
        } else if (editarInfo.category == null || editarInfo.category == "") {
          showMessageWarning("Atención! Debes seleccionar una categoria", 2)
        } else if (Categoria(categories, editarInfo.category).toLowerCase().includes("contratista") !== true && (editarInfo.obra === "" || editarInfo.contratista === "")) {

          showMessageWarning("Atención! Debes seleccionar un contratista y una obra", 2)
        }

        else if (esContratista) {
          let bodyContratista = {
            name: empleado.name,
            secondName: empleado.secondName,
            aPaterno: empleado.aPaterno,
            aMaterno: empleado.aMaterno,
            noSocial: empleado.noSocial,
            status: true,
          }
          editEmployee(bodyContratista, editarInfo._id)
          onCloseModal();//cerrar el modal
        } else {
          editEmployee(empleado, editarInfo._id)
          onCloseModal();//cerrar el modal
        }

      } else {//creamos un empleado o contratista 

        if (esContratista) {
          let bodyContratista = {
            name: empleado.name,
            secondName: empleado.secondName,
            aPaterno: empleado.aPaterno,
            aMaterno: empleado.aMaterno,
            noSocial: empleado.noSocial,
            category: empleado.category,
            categoryName: empleado.categoryName,
            status: true,
          }
          addEmployee(bodyContratista)
        } else {
          addEmployee(empleado)
          setBloquear(true)
        }
      }
    }
  }

  return (
    <Modal
      open={valueAddModal}
      onClose={onCloseModal}
      center={true}
      closeOnEsc={false}
      showCloseIcon={false}
      closeOnOverlayClick={false}
    >
      <Card className="addEmployeeCont">
        <CardBody>

          <Header
            titulo={titulo}
            onCloseModal={onCloseModal}
            hanldleSubmit={hanldleSubmit}
            bloquear={bloquear}
          />

          <div className="form__form-group">
            <span className="form__form-group-label">Nombre</span>
            <div className="form__form-group-field">
              <input type="text"
                className="form-control"
                name="name" autoComplete="new-password"
                value={empleado.name}
                onChange={handleChangeInputs}
              />
            </div>
          </div>

          <div className="form__form-group">
            <span className="form__form-group-label">Segundo nombre</span>
            <div className="form__form-group-field">
              <input type="text"
                className="form-control"
                name="secondName" autoComplete="new-password"
                value={empleado.secondName}
                onChange={handleChangeInputs}
              />
            </div>
          </div>

          <div className="form__form-group">
            <span className="form__form-group-label">Apellido paterno</span>
            <div className="form__form-group-field">
              <input type="text"
                className="form-control"
                name="aPaterno" autoComplete="new-password"
                value={empleado.aPaterno}
                onChange={handleChangeInputs}
              />
            </div>
          </div>

          <div className="form__form-group">
            <span className="form__form-group-label">Apellido materno</span>
            <div className="form__form-group-field">
              <input type="text"
                className="form-control"
                name="aMaterno" autoComplete="new-password"
                value={empleado.aMaterno}
                onChange={handleChangeInputs}
              />
            </div>
          </div>

          <div className="form__form-group">
            <span className="form__form-group-label">CURP</span>
            <div className="form__form-group-field">
              <input type="text" className="form-control"
                name="curp"
                autoComplete="new-password"
                value={empleado.curp}
                onChange={handleChangeInputs}
                disabled={(esContratista) ? "disabled" : ""}
              />
            </div>
          </div>

          <div className="form__form-group">
            <span className="form__form-group-label">Número Seguro Social</span>
            <div className="form__form-group-field">
              <input className="form-control" type="number"
                autoComplete="new-password"
                name="noSocial"
                defaultValue={empleado.noSocial}
                onChange={handleChangeInputs}
              />
            </div>
          </div>

          <hr className="new4" />
          <h5>Asignaciones</h5>

          <span className="form__form-group-label">Asignar una categoría</span>
          <Col sm={12} md={12} className="mb-3 selectAMargin">
            <Select
              //value={empleado.category}Categoria
              disabled={(esContratistaEditar) ? "disabled" : ""}
              value={Categoria(categories, empleado.category)}
              showSearch
              style={{ width: "100%" }}
              placeholder="Selecciona una categoría"
              onChange={handleChangeCategorie}
            >
              {showOptions()}
            </Select>
          </Col>

          <span className="form__form-group-label">Asignar un contratista</span>
          <Col sm={12} md={12} className="mb-2 selectAMargin">
            <Select
              value={Contratista(allContratistas, empleado.contratista)}
              showSearch
              style={{ width: "100%" }}
              placeholder="Selecciona un contratista"
              onChange={handleChangeContratista}
              disabled={(esContratista) ? "disabled" : ""}
            >
              {showContratista()}
            </Select>
          </Col>

          <span className="form__form-group-label">Asignar a una obra</span>
          <Col sm={12} md={12} className="selectAMargin">
            <Select
              notFoundContent="El contratista no tiene obras asignadas"
              value={Obra(obras, empleado.obra)}
              showSearch
              style={{ width: "100%" }}
              placeholder="Asignar a una obra"
              onChange={handleChangeObras}
              disabled={(empleado.contratista === "") ? "disabled" : (esContratista) ? "disabled" : ""}
            >
              {showObras()}
            </Select>
          </Col>

          {editarInfo != undefined ?
            <Can do="delete" on="/users/">
              <Row className="d-flex flex-row-reverse">
                <ButtonToolbar>
                  <Button color="danger"
                    onClick={() => setEliminar(true)}>
                    <p>
                      Eliminar</p>
                  </Button>
                </ButtonToolbar>
              </Row>
            </Can> : null}
        </CardBody>
      </Card>

      {(eliminar) ?
        <ModalConfirmation
          closeModal={setEliminar}
          valueModal={eliminar}
          callback={deleteEmployIndex}
          value={editarInfo._id}
          titulo="Confirmación : ¿estas seguro que deseas eliminar este registro?"
          closeParent={onCloseModal}
        /> : null}
    </Modal>
  )
}
export default FormEC
