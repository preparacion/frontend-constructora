import React from 'react';

//regresa el nombre la la categoria
export const Categoria = (categorias = [], id) => {
  if (categorias.length <= 0) return
  let categoriaActual = categorias.find(value => value._id === id)

  return categoriaActual != undefined ? categoriaActual.name : null
}

//regresa el nombre del contratista dado su id
export const Contratista = (contratista = [], id) => {
  if (contratista.length <= 0) return
  let contratistaA = contratista.find(value => value._id === id)

  return (contratistaA != undefined ?
    `${contratistaA.name} ${contratistaA.aPaterno} ${contratistaA.aMaterno}` : null)
}

//regresa el nombre de una obra dado su id
export const Obra = (obras = [], id) => {
  if (obras.length <= 0) return
  let obraA = obras.find(value => value._id === id)

  return (obraA != undefined ? obraA.name : null)
}