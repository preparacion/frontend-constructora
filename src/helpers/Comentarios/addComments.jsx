import React, { Component } from 'react';
import { Card, CardBody, Col, ButtonToolbar, Button, Row } from 'reactstrap';

import Modal from 'react-responsive-modal';
import { fetchAsync, urlComments, urlEmployeesComment } from '../Globals/globals';
import {
    showMessageLoading,
} from '../MessageAndNotificationUtils';
import 'react-inputs-validation/lib/react-inputs-validation.min.css';

import Can from "../Secure/Can";

let closeLoading = "";

class AddComentarioModal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            comentario: '',
            grupos: [],
            bloquear: false,
        };
        this.handleChange = this.handleChange.bind(this);
    }

    agregarComentarios = () => {

        closeLoading = this.LoadingMessage()
        this.setState({ bloquear: true, })

        let utc = new Date().toJSON();

        let bodyRequest = {
            // "student": this.props._idEstudiente,
            //"group": this.state.grupos[this.state.grupos.length-1],
            // "date": utc,
            "comment": this.state.comentario
        }

        if (bodyRequest.comment.length <= 500) {

            fetchAsync(urlEmployeesComment+this.props._idEmployee, JSON.stringify(bodyRequest), "PUT", "")
                .then(
                    (result) => {
                        this.props.getUsers()
                        this.props.getRowInfo(result)
                        closeLoading()
                       // if (result.success === true) {
                            //this.props.cargarComents(this.props._idEstudiente)
                            this.onCloseModal()
                            this.setState({
                                comentario: ''
                            })

                       // }
                        this.setState({
                            bloquear: false,
                        })
                    })
                .catch(
                    console.log('err')
                );
        } else {
            alert("El comentario excede el limite de 500 caracteres.")
        }
    }

    onCloseModal = () => {
        this.setState({
            comentario: '',
            bloquear: false,
        })
        this.props.openModal(false);
        if (closeLoading != "") {
            closeLoading()
        }
    };

    numCaracteres = () => {

        if (this.state.comentario.length >= 500) {
            return (<p className="pull-right contadorCaracteres">Limite de 500 caracteres alcanzado.</p>)
        } else {
            return (<p className="pull-right contadorCaracteres">Caracteres restantes: {500 - this.state.comentario.length} de 500</p>)
        }
    }

    handleChange(event) {
        this.setState({ comentario: event.target.value });
    }

    //crea uan funcion para lamzar el mensaje de loading
    LoadingMessage = () => {
        return showMessageLoading('creando..', 0);
    };

    render() {

        //console.log("grupo")
        //console.log(this.state.grupos)
        //console.log("grupo")
        return (
            <div class="container">
                <div className="row justify-content-center">
                    <div class="col-12">
                        <Modal
                            open={this.props.valueModal}
                            onClose={this.onCloseModal}
                            closeOnEsc={false}
                            showCloseIcon={false}
                            closeOnOverlayClick={false}
                        >
                            <Col xs={12} sm={12} md={12} className="modal-header mb-2">
                                <p className="titleComments">Agregar comentario</p>
                            </Col>
                            <Row className="mt-3">

                                <Col xs={12} sm={12} md={12} >
                                    <div className="cajaText1">
                                        <form action="" className="form_comentarios d-flex justify-content-end flex-wrap" >
                                            <textarea name="" id=""
                                                placeholder="Comentario"
                                                value={this.state.comentario}
                                                onChange={this.handleChange}
                                                autofocus="true"
                                                maxlength="500" />
                                            {
                                                this.numCaracteres()
                                            }
                                        </form>

                                    </div>
                                </Col>
                                <Col xs={12} md={12} className="mt-2">

                                    <Button size="sm" className="btn float-right"
                                        color="success"
                                        style={{ marginRight: "-5px" }}
                                        onClick={() => { this.agregarComentarios() }}
                                        disabled={this.state.bloquear === true ? "disabled" : ""}
                                    >
                                        <p>Enviar</p>
                                    </Button>

                                    <Button size="sm" className="btn btn-secondary float-right"
                                        style={{ marginRight: "15px" }}
                                        onClick={() => this.onCloseModal()}>
                                        <p>Salir</p>
                                    </Button>
                                </Col>
                            </Row>
                        </Modal>
                    </div>
                </div>
            </div>
        );
    }
}
export default AddComentarioModal;